// YouAreTheFatherMauryPovichStyle
export interface Person {
  name: string
  chromosomes: string[][]
}

function parse(line: string): Person {
  const semiColumnPosition = line.indexOf(':')
  const name = line.substring(0, semiColumnPosition)
  const chromosomes = line
    .substring(semiColumnPosition + 1)
    .trim()
    .split(' ')
    .map((p) => p.split('').sort())

  return { name, chromosomes }
}

export const init = () => {
  const mother = parse(readline())
  const child = parse(readline())

  ;[...Array(+readline())]
    .map(() => readline())
    .map(parse)
    .filter((father) =>
      father.chromosomes.every((fatherChromosome, i) => {
        const [f1, f2] = fatherChromosome
        const [m1, m2] = mother.chromosomes[i]
        const childChromosome = child.chromosomes[i]

        return (
          (childChromosome.includes(f1) || childChromosome.includes(f2)) &&
          (childChromosome.includes(m1) || childChromosome.includes(m2))
        )
      }),
    )
    .forEach((father) => print(`${father.name}, you are the father!`))
}
