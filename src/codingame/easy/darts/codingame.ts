import { Point } from '@lib/geometry/point'
import { isInCircle } from '@lib/geometry/distance'

export const init = () => {
  const SIZE = parseInt(readline())
  if (SIZE === 0) return
  const N = parseInt(readline())

  const nameIndex: any = {}
  for (let i = 0; i < N; i++) {
    nameIndex[readline()] = i
  }
  const T = parseInt(readline())
  const r = SIZE / 2

  const inCircle = (p: Point) => isInCircle(p, r)
  const inSquare = (p: Point) => p[0] >= -r && p[0] <= r && p[1] >= -r && p[1] <= r
  const inDiamond = (p: Point) => {
    const x = p[0]
    const y = p[1]
    return y <= -x + r && y <= x + r && y >= -x - r && y >= x - r
  }

  const scoreTree: any = {}
  for (let i = 0; i < T; i++) {
    const t = readline().split(' ')
    const name = t[0]
    const p: Point = [+t[1], +t[2]]

    const score = inDiamond(p) ? 15 : inCircle(p) ? 10 : inSquare(p) ? 5 : 0
    scoreTree[name] = (scoreTree[name] == undefined ? 0 : scoreTree[name]) + score
  }

  const score = new Array<Score>()
  for (let n in scoreTree) {
    score.push({ name: n, score: scoreTree[n] })
  }

  const compareFn = (a: Score, b: Score) => {
    const result = b.score - a.score
    if (result !== 0) return result
    return +nameIndex[a.name] - +nameIndex[b.name]
  }

  score.sort(compareFn).forEach((s) => print(s.name + ' ' + s.score))
}

interface Score {
  name: string
  score: number
}
