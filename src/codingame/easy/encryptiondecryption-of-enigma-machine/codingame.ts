export const init = () => {
  const operation = readline()
  const pseudoRandomNumber = parseInt(readline())

  const rotors = ['ABCDEFGHIJKLMNOPQRSTUVWXYZ']
  for (let i = 0; i < 3; i++) {
    rotors.push(readline())
  }

  const message = readline()

  const subs = (r1: string, r2: string, char: string): string => {
    const index = r1.indexOf(char)
    return r2.charAt(index)
  }

  const encode = (rotors: string[], message: string): string => {
    let output = ''
    for (let i = 0; i < message.length; i++) {
      const codes = ((message.charAt(i).charCodeAt(0) + pseudoRandomNumber + i - 65) % 26) + 65
      let currentChar = String.fromCharCode(codes)
      for (let j = 0; j < rotors.length; j++) {
        currentChar = subs(rotors[0], rotors[j], currentChar)
      }
      output += currentChar
    }
    return output
  }

  const decode = (rotors: string[], message: string): string => {
    let output = ''

    for (let j = 1; j <= message.length; j++) {
      let subs = message.substr(0, j)
      for (let i = 0; i < rotors[0].length; i++) {
        const char = rotors[0].charAt(i)
        const encoded = encode(rotors, output + char)
        if (encoded === subs) {
          output += char
          break
        }
      }
    }
    return output
  }

  print((operation === 'ENCODE' ? encode : decode)(rotors, message))
}
