export const init = () => {
  const intext = readline().toLowerCase()

  const upperCase = (value: string) => value.charAt(0).toUpperCase() + value.slice(1)

  const rem = (value: string) =>
    value
      .replace(/,+/g, ',')
      .replace(/\s+/g, ' ')
      .replace(/\.+/g, '.')
      .replace(/\s+,/g, ',')
      .replace(/\s+\./g, '.')

  print(
    rem(
      rem(
        rem(intext)
          .split(',')
          .join(', ')
          .split('.')
          .map((a) => upperCase(a.trim()))
          .join('. ')
          .trim(),
      ),
    ),
  )
}
