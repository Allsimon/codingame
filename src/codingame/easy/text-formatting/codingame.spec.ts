import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/text-formatting/codingame'

describe('TextFormatting', () => {
  it(`One sentence without spaces`, () => runTest(init, `one,two,three.`, `One, two, three.`))

  it(`Two sentences`, () =>
    runTest(init, `one,two,three.four,five, six.`, `One, two, three. Four, five, six.`))

  it(`Extra spaces`, () =>
    runTest(init, `one , two , three . four , five , six .`, `One, two, three. Four, five, six.`))

  it(`More errors`, () =>
    runTest(
      init,
      `one , TWO  ,,  three  ..  four,fivE , six .`,
      `One, two, three. Four, five, six.`,
    ))

  it(`Shakespeare`, () =>
    runTest(
      init,
      `when a father gives to his son,,, Both laugh; When a son gives to his father, , , Both cry...shakespeare`,
      `When a father gives to his son, both laugh; when a son gives to his father, both cry. Shakespeare`,
    ))
})
