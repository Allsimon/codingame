export const init = () => {
  const N = +readline()
  if (!N) return
  const numbers = readline().split(' ').map(Number).sort()

  const sum = (arr: number[], score: number): number => {
    if (arr.length === 1) {
      return score
    } else {
      let subArr = arr.sort((a, b) => a - b)
      const subSum = arr[0] + arr[1]
      subArr = subArr.slice(2)
      subArr.push(subSum)
      return sum(subArr, score + subSum)
    }
  }
  print(sum(numbers, 0))
}
