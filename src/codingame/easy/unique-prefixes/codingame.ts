// UniquePrefixes
export const init = () => {
  const N = +readline()
  const lines = [...Array(N)].map(() => readline())

  lines.forEach((line) => {
    const chars = line.split('')

    let i = 1
    for (; i < chars.length; i++) {
      let found = false
      for (let nextLine of lines) {
        if (nextLine !== line) {
          if (nextLine.startsWith(line.substring(0, i))) {
            found = true
          }
        }
      }
      if (!found) {
        break
      }
    }
    print(line.substring(0, i))
  })
}
