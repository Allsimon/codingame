// Lumen
import {
  cloneAndReplaceInMatrix,
  doOnAdjacentComplete,
  findAllInMatrix,
  MatrixNavigationParameter,
  Position,
} from '@lib/matrix/matrix'

const param: MatrixNavigationParameter = {
  considerDiagonal: true,
}

function reduceLuminosity(distances: number[][], currentLuminosity: number, p: Position) {
  if (currentLuminosity === 0) {
    return
  }
  const adjacentLuminosity = distances[p.x][p.y]
  const reducedLuminosity = currentLuminosity - 1
  if (adjacentLuminosity <= reducedLuminosity) {
    distances[p.x][p.y] = reducedLuminosity
    doOnAdjacentComplete(distances, p, param, (position) =>
      reduceLuminosity(distances, reducedLuminosity, position),
    )
  }
}

export const init = () => {
  const length = +readline()
  const baseLight = +readline()

  const originalMatrix = [...Array(length)].map(() => readline().split(' '))
  const distances = cloneAndReplaceInMatrix(originalMatrix, (value) => {
    if (value === 'X') {
      return 0
    } else if (value === 'C') {
      return baseLight
    }
    throw new Error("Can't parse: " + value)
  })

  findAllInMatrix(distances, (value) => value === baseLight).forEach((position) =>
    reduceLuminosity(distances, baseLight + 1, position),
  )

  print('' + distances.flatMap((p) => p).filter((value) => value === 0).length)
}
