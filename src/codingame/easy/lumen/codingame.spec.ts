import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/lumen/codingame'

describe('Lumen', () => {
  it(`THEY only have one candle`, () =>
    runTest(
      init,
      `5
3
X X X X X
X C X X X
X X X X X
X X X X X
X X X X X`,
      `9`,
    ))

  it(`THEY are doing a ritual`, () =>
    runTest(
      init,
      `5
3
C X X X C
X X X X X
X X X X X
X X X X X
C X X X C`,
      `0`,
    ))

  it(`THEY have a large pit`, () =>
    runTest(
      init,
      `5
3
X X X X X
X C X X X
X X X X X
X X X C X
X X X X X`,
      `2`,
    ))

  it(`THEY have a small cellar`, () =>
    runTest(
      init,
      `6
3
X X X X X X
X C X X X X
X X X X X X
X X X C X X
X X X X X X
X X X X X X`,
      `4`,
    ))

  it(`THEY have a medium cellar`, () =>
    runTest(
      init,
      `15
3
X X X X X X X X X X X X C X X
X X X X X X X X X X X X C X X
X X X X X X X X X C X X X X X
X X X X X C X X X X X X X X X
X X X X C X X X X X X X X X X
C X X X X X X X C X X C X X X
X X X X X C X X X X X X X X C
X C X X X X X X X X X X X X X
X X X X X X X C X C X X X X X
X X X X X X X X X X X X C X X
X X X X X X X X X X X X X X X
X X C X C X X X X X X X X X C
X X X X X X C X X X C X X X X
X C X X X X X X X X X X X X X
X X X X X X X X X X X X X X X`,
      `14`,
    ))

  it(`THEY have a large cellar`, () =>
    runTest(
      init,
      `20
3
X X X C X C X X X X X X X X X X X X X X
X C X X X C X X X X C X X X X C X X X X
X X X X X X X X X X X X X C C X X X X X
X X X X X X X X X X X X X X X X X X X X
X X X X X X X X X X X X X X X X X X X C
X X X X X X X X C X X X X X X X C X X X
X X X X X C X X X X X X X X C X X X X C
X X X X X X C X X C C C X X X X X X X X
X C X X X X X X X X C X X X C X X X X X
X X X X C X X C X X X X X X X X X C X X
X X X X X X X X X C X C X X X X X X X X
X X X X X X X X X X X X X X X X X X X X
X X X X X X C X X X X X X X X X X C X X
X X X X X X X X X X X X X X X X X X X X
X X X X X X X X X C C X X X X C X X X X
X X X X X X X X X X X X C C C X X X X X
X X X X X X X X C X X X X X X X C C X X
X X X C X X X X X X X X X X X X X C X X
X X X X X C X X X X X X X X X C X X X X
X C X C X X X X X X X X X X X X X X X X`,
      `34`,
    ))

  it(`THEY are not very smart`, () =>
    runTest(
      init,
      `3
3
X X X
X X X
X X X`,
      `9`,
    ))

  it(`THEY have a great hall`, () =>
    runTest(
      init,
      `25
2
X X C C C C X X X X C C X X X C X X X X X C X X X
C X X X X X X X X X X X X X X C X X X X X X X X X
X X X X X X X X X X X X C X X X X C X X X C C X X
C X X X X C X X X X X X X X X X X X X C X X X X X
X X C X C X X X X X X X X X C X X C X X X C X X X
X X X X X C X X X X X X X C X C X X X X X X X X X
C X X X X C C X X X X X X X X X C X X X X X X C X
C X C C X X C X X C X X C C X X C X X X X X X X X
X X X X X X X X X X X X X X X X X C X X X X X C X
X C X X X X X X X X X X X X X X X X X C C X X X X
C X X X X X X C X C X X X X C X X X X X C X X X X
X C X X C X X X X X X X X X C X X X X X C X X X C
X X X X X X X X X C X X C X X X X X X X X X C X X
X X C X C X X X X X C X X X X X X C C X X X C X C
C X C C X X X X X X X X X X C X X X X X C X X X X
C X X C X C X X C C X X X C C X X X X X X X C C X
C X X X X X X X X X X X X X X X X X X C X X X X X
X X X X X X X C X X X X X X C X X X X X X X C X X
X C X C X X C X X X X X C X X X X X X X C C X C X
X X X C X X C C C X X C X X X X X C X X X X X X X
X C X X X X X X X X X X X C X X X X X X X X X X X
X X X C C X X C X C X X X X X X X X X C C X X X X
X X C X X X X C X C C X X X X X C X X X X C C X X
X X X X C X X X X X C X X X X X C X X X C X X C X
X X X C X X C X X X X X X X X X X X C X X X X X X`,
      `90`,
    ))

  it(`Not Euclidean`, () =>
    runTest(
      init,
      `5
4
C X X X X
X X X X X
X X X X X
X X X X X
X X X X X`,
      `9`,
    ))
})
