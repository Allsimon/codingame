import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/carmichael-numbers/codingame'

describe('CarmichaelNumbers', () => {
  it(`Small composite number`, () => runTest(init, `12`, `NO`))

  it(`Small Carmichael number`, () => runTest(init, `561`, `YES`))

  it(`Bigger composite number`, () => runTest(init, `1728`, `NO`))

  it(`Small prime number`, () => runTest(init, `7`, `NO`))

  it(`Big prime number`, () => runTest(init, `29347`, `NO`))

  it(`Big Carmichael number`, () => runTest(init, `449065`, `YES`))
})
