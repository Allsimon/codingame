// CreateTurnHereSigns
export const init = () => {
  const sign = { left: '<', right: '>' }

  const [
    direction,
    howManyArrows,
    heightOfArrows,
    strokeThicknessOfArrows,
    spacingBetweenArrows,
    additionalIndentOfEachLine,
  ] = readline().split(' ')

  // @ts-ignore
  const arrow = sign[direction].repeat(+strokeThicknessOfArrows)

  const middle = Math.floor(+heightOfArrows / 2)

  for (let i = 0; i < +heightOfArrows; i++) {
    let numberOfSpace
    if (direction === 'left') {
      numberOfSpace = (i <= middle ? +heightOfArrows - i : i + 1) - middle - 1
    } else {
      numberOfSpace = i <= middle ? i : +heightOfArrows - i - 1
    }

    const result =
      ' '.repeat(numberOfSpace * +additionalIndentOfEachLine) +
      [...Array(+howManyArrows)].map((_) => arrow).join(' '.repeat(+spacingBetweenArrows))
    print(result)
  }
}
