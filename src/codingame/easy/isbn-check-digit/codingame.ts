export const init = () => {
  const N = parseInt(readline())
  if (!N) return

  const isbn10 = (ISBN: string) => {
    const sum = Array.from(ISBN.slice(0, -1)).reduce((a, v, i) => +v * (10 - i) + a, 0)

    let lastNumber = (sum % 11 === 0 ? 0 : 11 - (sum % 11)) + ''
    if (lastNumber === '10') lastNumber = 'X'
    return ISBN[9] === lastNumber
  }

  const isbn13 = (ISBN: string) => {
    const sum = Array.from(ISBN.slice(0, -1)).reduce((a, v, i) => +v * (i % 2 ? 3 : 1) + a, 0)

    let lastNumber = (sum % 10 === 0 ? 0 : 10 - (sum % 10)) + ''
    if (lastNumber === '10') lastNumber = 'X'
    return ISBN[12] === lastNumber
  }

  const isInvalid = (ISBN: string) => !(ISBN.length === 10 ? isbn10 : isbn13)(ISBN)

  const invalid = Array(N)
    .fill(1)
    .map(() => readline())
    .filter((i) => isInvalid(i))

  print(`${invalid.length} invalid:`)
  invalid.forEach((i) => print(i))
}
