// OffsetArrays

import { reduceToObject } from '@lib/array/arrays'

interface ArrayOp {
  name: string
  start: number
  end: number
  content: number[]
}

function parseAr(line: string): ArrayOp {
  // A[-1..1] = 1 2 3
  // @ts-ignore
  const { name, start, end, content } =
    /(?<name>\w+)\[(?<start>.*)\.\.(?<end>.*)] = (?<content>.*)/.exec(line)?.groups
  return { name, start: +start, end: +end, content: content.split(' ').map(Number) }
}

function resultOf(name: string, index: number, mapOfLines: Record<string, ArrayOp>): number {
  const array = mapOfLines[name]
  return array.content[index - array.start]
}

function parseIndex(line: string, mapOfLines: Record<string, ArrayOp>): number {
  // B[A[C[-329054771]]]
  // @ts-ignore
  const { name, index } = /(?<name>\w+)\[(?<index>.*)]/.exec(line)?.groups

  if (index.includes('[')) {
    return resultOf(name, parseIndex(index, mapOfLines), mapOfLines)
  }
  return resultOf(name, index, mapOfLines)
}

export const init = () => {
  const N = +readline()
  const lines = [...Array(N)].map(() => readline()).map(parseAr)

  const mapOfLines = reduceToObject(
    lines,
    (l) => l.name,
    (l) => l,
  )
  const output = readline()

  print(parseIndex(output, mapOfLines))
}
