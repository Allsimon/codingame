import { Mapp } from '@lib/type/type'

interface Player {
  num: number
  sign: string
  vanquished: number[]
}

function assignVictory(winner: Player, loser: Player): Player {
  winner.vanquished.push(loser.num)
  return winner
}

function match(p1: Player, p2: Player): Player {
  if (p1.sign === p2.sign) {
    return p1.num < p2.num ? p1 : p2
  }

  const winningMove: Mapp<string[]> = {
    C: ['P', 'L'],
    P: ['R', 'S'],
    R: ['L', 'C'],
    L: ['S', 'P'],
    S: ['C', 'R'],
  }
  if (winningMove[p1.sign].includes(p2.sign)) {
    return p1
  }
  return p2
}

function fightAndReturnsWinner(players: Player[]): Player {
  if (players.length === 1) {
    return players[0]
  }

  const winners = []
  for (let i = 0; i < players.length / 2; i++) {
    const index = i * 2

    const p1 = players[index]
    const p2 = players[index + 1]

    const winner = match(p1, p2)
    assignVictory(winner, winner === p1 ? p2 : p1)
    winners.push(winner)
  }
  return fightAndReturnsWinner(winners)
}

export const init = () => {
  const N = +readline()
  const matches = [...Array(N)].map(() => {
    const [num, sign] = readline().split(' ')

    return { num: +num, sign, vanquished: [] }
  })

  const winner = fightAndReturnsWinner(matches)

  print(winner.num)
  print(winner.vanquished.join(' '))
}
