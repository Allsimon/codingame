import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/guessing-n-cheating/codingame'

describe('GuessingNCheating', () => {
  it(`Sample cheating`, () =>
    runTest(
      init,
      `3
5 too high
1 too high
2 right on`,
      `Alice cheated in round 2`,
    ))

  it(`Cheat right in the face`, () =>
    runTest(
      init,
      `5
47 too low
48 too high
49 too high
50 too high
47 right on`,
      `Alice cheated in round 2`,
    ))

  it(`Bob in a mess`, () =>
    runTest(
      init,
      `8
50 too low
55 too low
45 too low
44 too low
62 too high
80 too high
51 too low
61 right on`,
      `No evidence of cheating`,
    ))

  it(`Contradiction`, () =>
    runTest(
      init,
      `6
50 too low
52 too high
51 too low
52 too low
50 too high
53 right on`,
      `Alice cheated in round 3`,
    ))

  it(`Good Luck`, () =>
    runTest(
      init,
      `1
50 right on`,
      `No evidence of cheating`,
    ))

  it(`Sequence`, () =>
    runTest(
      init,
      `14
1 too low
2 too low
3 too low
4 too low
5 too low
6 too low
7 too low
8 too low
9 too low
10 too low
11 too low
12 too low
13 too low
14 right on`,
      `No evidence of cheating`,
    ))

  it(`Impossible`, () =>
    runTest(
      init,
      `3
100 too low
1 too high
50 right on`,
      `Alice cheated in round 1`,
    ))

  it(`Repetition`, () =>
    runTest(
      init,
      `7
30 too low
50 too low
35 too low
40 too low
50 too low
60 too high
55 right on`,
      `No evidence of cheating`,
    ))

  it(`Elimination`, () =>
    runTest(
      init,
      `7
30 too low
50 too high
35 too low
40 too low
49 too low
60 too high
55 right on`,
      `Alice cheated in round 5`,
    ))

  it(`Right at the end`, () =>
    runTest(
      init,
      `7
50 too low
75 too high
62 too low
69 too low
72 too high
70 too low
17 right on`,
      `Alice cheated in round 7`,
    ))
})
