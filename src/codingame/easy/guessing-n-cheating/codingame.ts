export const init = () => {
  const N = +readline()
  if (!N) return
  const inputs = Array(N)
    .fill(0)
    .map(() => readline())
  const goal = +inputs[inputs.length - 1].split(' ')[0]

  const possible: boolean[] = []
  for (let i = 1; i <= 100; i++) {
    possible[i] = true
  }

  const count = () => possible.reduce((ar, cv) => ar + (cv === true ? 1 : 0), 0)

  for (let i = 0; i < inputs.length - 1; i++) {
    const arr = inputs[i].split(' ')
    const val = +arr[0]
    const high = arr[arr.length - 1] === 'high'

    if (high) {
      for (let j = val; j <= 100; j++) {
        possible[j] = false
      }
    } else {
      for (let j = val; j > 0; j--) {
        possible[j] = false
      }
    }
    if (count() === 0) {
      print(`Alice cheated in round ${i + 1}`)
      return
    }
  }
  if (possible[goal] === false) {
    print(`Alice cheated in round ${N}`)
  } else {
    print('No evidence of cheating')
  }
}
