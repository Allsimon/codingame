namespace BankRobbers {
  const time = (C: number, N: number): number => Math.pow(10, N) * Math.pow(5, C - N)

  const R = parseInt(readline())
  const V = parseInt(readline())
  const timePerRobbers: number[][] = Array.from({ length: R }, () => [])

  const sum = (array: number[]): number =>
    array.length === 0 ? 0 : array.reduce((total, current) => total + current)

  for (let i = 0; i < V; i++) {
    const inputs = readline().split(' ')
    const C = parseInt(inputs[0])
    const N = parseInt(inputs[1])

    let minTime = Number.MAX_VALUE
    let minRobber = []
    for (let currentRobber of timePerRobbers) {
      const currentRobberTime = sum(currentRobber)
      if (currentRobberTime < minTime) {
        minRobber = currentRobber
        minTime = currentRobberTime
      }
    }
    minRobber.push(time(C, N))
  }

  const totalTime = Math.max(...timePerRobbers.map((robber) => sum(robber)))

  print(totalTime)
}
