import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/reverse-fizzbuzz/codingame'

describe('ReverseFizzbuzz', () => {
  it(`Sample`, () =>
    runTest(
      init,
      `15
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz`,
      `3 5`,
    ))

  it(`Everything is a Fizz`, () =>
    runTest(
      init,
      `20
Fizz
Fizz
Fizz
Fizz
FizzBuzz
Fizz
Fizz
Fizz
Fizz
FizzBuzz
Fizz
Fizz
Fizz
Fizz
FizzBuzz
Fizz
Fizz
Fizz
Fizz
FizzBuzz`,
      `1 5`,
    ))

  it(`Fizz is Buzz`, () =>
    runTest(
      init,
      `20
14
FizzBuzz
16
17
18
19
FizzBuzz
21
22
23
24
FizzBuzz
26
27
28
29
FizzBuzz
31
32
33`,
      `5 5`,
    ))

  it(`One Fizz to rule them all`, () =>
    runTest(
      init,
      `30
1
2
3
4
5
6
7
8
Buzz
10
11
12
13
14
15
16
17
Buzz
19
20
21
22
23
24
25
26
Buzz
28
29
Fizz`,
      `30 9`,
    ))

  it(`Tricky`, () =>
    runTest(
      init,
      `21
Fizz
Buzz
52
53
54
Fizz
56
57
58
59
Fizz
61
62
63
64
Fizz
66
67
68
69
Fizz`,
      `5 51`,
    ))
})
