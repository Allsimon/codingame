// ReverseFizzbuzz
import { isNumeric } from '@lib/number/numbers'

function indexOf(values: string[], expected: string): number {
  return values.findIndex((value) => value.includes(expected))
}

function guess(values: string[], expected: string): number {
  const firstIndex = indexOf(values, expected)
  if (firstIndex === -1) {
    return -1
  }

  const nextIndex = indexOf(values.slice(firstIndex + 1), expected)
  if (nextIndex === -1) {
    const indexOther = values.findIndex((value) => isNumeric(value))
    const valueIndexOther = values[indexOther]

    return firstIndex - indexOther + +valueIndexOther
  }

  return nextIndex + 1
}

export const init = () => {
  const N = +readline()
  const lines = [...Array(N)].map(() => readline())

  print(guess(lines, 'Fizz') + ' ' + guess(lines, 'Buzz'))
}
