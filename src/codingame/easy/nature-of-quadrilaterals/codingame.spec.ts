import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/nature-of-quadrilaterals/codingame'

describe('NatureOfQuadrilaterals', () => {
  it('Quadrilateral', () =>
    runTest(
      init,
      `1
A -14 -3 B 5 -9 C 11 4 D -7 13`,
      `ABCD is a quadrilateral.`,
    ))

  it('Parallelogram', () =>
    runTest(
      init,
      `1
D -4 -2 E 2 0 R 4 4 P -2 2`,
      `DERP is a parallelogram.`,
    ))

  it('Rhombus', () =>
    runTest(
      init,
      `1
A -2 0 B 0 1 C 2 0 D 0 -1`,
      `ABCD is a rhombus.`,
    ))

  it('Rectangle', () =>
    runTest(
      init,
      `1
E -2 -1 F -2 3 G 1 3 H 1 -1`,
      `EFGH is a rectangle.`,
    ))

  it('Square', () =>
    runTest(
      init,
      `1
A 1 -2 B 5 0 C 3 4 D -1 2`,
      `ABCD is a square.`,
    ))

  it('Everything', () =>
    runTest(
      init,
      `3
H -4 3 A 2 5 R 4 2 D 10 4
J -2 0 A 0 1 C 2 0 K 0 -1
A 1 -2 B 5 0 C 3 4 D -1 2`,
      `HARD is a quadrilateral.
JACK is a rhombus.
ABCD is a square.`,
    ))
})
