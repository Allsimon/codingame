import { euclideanDistance } from '@lib/geometry/distance'
import { angleBetween, dotProduct, vector } from '@lib/geometry/vector'
import { isClose } from '@lib/number/numbers'

export const init = () => {
  const n = parseInt(readline())
  for (let i = 0; i < n; i++) {
    const inputs = readline().split(' ')
    const A = inputs[0]
    const xA = parseInt(inputs[1])
    const yA = parseInt(inputs[2])
    const B = inputs[3]
    const xB = parseInt(inputs[4])
    const yB = parseInt(inputs[5])
    const C = inputs[6]
    const xC = parseInt(inputs[7])
    const yC = parseInt(inputs[8])
    const D = inputs[9]
    const xD = parseInt(inputs[10])
    const yD = parseInt(inputs[11])

    const pA = [xA, yA]
    const pB = [xB, yB]
    const pC = [xC, yC]
    const pD = [xD, yD]

    const allEquals = (ar: number[]) => ar.filter((a) => a !== ar[0]).length === 0

    const isAParallelogram =
      isClose(
        dotProduct(vector(pA, pB), vector(pD, pC)),
        euclideanDistance(pA, pB) * euclideanDistance(pC, pD),
      ) &&
      isClose(
        dotProduct(vector(pB, pC), vector(pA, pD)),
        euclideanDistance(pB, pC) * euclideanDistance(pD, pA),
      )

    const isARhombus = allEquals([
      euclideanDistance(pA, pB),
      euclideanDistance(pB, pC),
      euclideanDistance(pC, pD),
      euclideanDistance(pD, pA),
    ])

    const isARectangle =
      isClose(angleBetween(pA, pB, pC), Math.PI / 2) &&
      isClose(angleBetween(pB, pC, pD), Math.PI / 2) &&
      isClose(angleBetween(pC, pD, pA), Math.PI / 2)

    let status = 'quadrilateral'

    if (isAParallelogram) {
      status = 'parallelogram'
    }

    if (isARhombus) {
      status = 'rhombus'
    }

    if (isARectangle) {
      status = 'rectangle'
    }

    if (isARectangle && isARhombus) {
      status = 'square'
    }
    print(A + B + C + D + ` is a ${status}.`)
  }
}
