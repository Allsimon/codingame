import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/street-fighter-level-i/codingame'

describe('StreetFighterLevelI', () => {
  it(`1- KEN vs RYU`, () =>
    runTest(
      init,
      `KEN RYU
4
< KICK
< PUNCH
> KICK
< PUNCH`,
      `RYU beats KEN in 3 hits`,
    ))

  it(`2-KEN vs VLAD Kick only`, () =>
    runTest(
      init,
      `KEN VLAD
10
> KICK
< KICK
> KICK
< KICK
> KICK
< KICK
> KICK
< KICK
> KICK
< KICK
`,
      `KEN beats VLAD in 5 hits`,
    ))

  it(`3-ANNA vs JUN Punch only`, () =>
    runTest(
      init,
      `ANNA JUN
10
> PUNCH
< PUNCH
> PUNCH
< PUNCH
> PUNCH
< PUNCH
> PUNCH
< PUNCH
> PUNCH
< PUNCH`,
      `JUN beats ANNA in 5 hits`,
    ))

  it(`4-TANK vs JADE punch & kick`, () =>
    runTest(
      init,
      `TANK JADE
10
> PUNCH
< KICK
> PUNCH
< KICK
> PUNCH
< KICK
> PUNCH
< KICK
> PUNCH
< KICK`,
      `TANK beats JADE in 5 hits`,
    ))

  it(`5- VLAD vs JUN Specials`, () =>
    runTest(
      init,
      `VLAD JUN
12
> PUNCH
< PUNCH
> KICK
< KICK
> PUNCH
< PUNCH
> PUNCH
< KICK
> KICK
< KICK
< SPECIAL
> SPECIAL`,
      `JUN beats VLAD in 6 hits`,
    ))

  it(`6- KEN vs TANK Specials`, () =>
    runTest(
      init,
      `KEN TANK
14
> PUNCH
< PUNCH
> KICK
< KICK
> PUNCH
< PUNCH
< SPECIAL
> SPECIAL
> PUNCH
< KICK
> KICK
< KICK
< SPECIAL
> SPECIAL`,
      `KEN beats TANK in 7 hits`,
    ))

  it(`7-VLAD vs RYU Special VLAD Kill`, () =>
    runTest(
      init,
      `VLAD RYU
14
< PUNCH
< PUNCH
< KICK
< KICK
< PUNCH
> PUNCH
> KICK
> SPECIAL
> PUNCH
< KICK
> KICK
< KICK
< SPECIAL
> SPECIAL`,
      `VLAD beats RYU in 5 hits`,
    ))

  it(`8-JUN vs KEN long story`, () =>
    runTest(
      init,
      `JUN KEN
30
< PUNCH
< PUNCH
< KICK
< KICK
< PUNCH
> PUNCH
> KICK
> SPECIAL
> PUNCH
< KICK
> KICK
< KICK
< SPECIAL
> SPECIAL
> PUNCH
< PUNCH
< KICK
> PUNCH
< KICK
> PUNCH
< KICK
< SPECIAL
> SPECIAL
> SPECIAL
< KICK
> PUNCH
< KICK
> KICK
< KICK
> SPECIAL`,
      `KEN beats JUN in 11 hits`,
    ))

  it(`9-ANNA vs JADE round 1`, () =>
    runTest(
      init,
      `ANNA JADE
30
< PUNCH
< PUNCH
< KICK
< KICK
< PUNCH
> PUNCH
> KICK
> SPECIAL
> PUNCH
< KICK
> KICK
< KICK
< SPECIAL
> SPECIAL
> PUNCH
< PUNCH
< KICK
> PUNCH
< KICK
> PUNCH
< KICK
< SPECIAL
> SPECIAL
< KICK
< KICK
> PUNCH
< KICK
> KICK
< KICK
> SPECIAL`,
      `JADE beats ANNA in 4 hits`,
    ))

  it(`10-ANNA vs JADE`, () =>
    runTest(
      init,
      `ANNA JADE
30
< PUNCH
< KICK
> SPECIAL
< KICK
> KICK
< KICK
> PUNCH
> PUNCH
< SPECIAL
> KICK
< KICK
> PUNCH
< KICK
> KICK
< PUNCH
< KICK
< KICK
< PUNCH
> PUNCH
> KICK
> SPECIAL
> PUNCH
< KICK
> KICK
< KICK
< SPECIAL
> SPECIAL
> PUNCH
< PUNCH
< KICK`,
      `JADE beats ANNA in 4 hits`,
    ))
})
