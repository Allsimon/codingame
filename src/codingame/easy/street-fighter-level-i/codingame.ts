// StreetFighterLevelI

interface Champion {
  name: string
  baseLife: number
  currentLife: number
  punch: number
  kick: number
  rage: number
  hitsMade: number
  special: Special
}

function createChamp(
  name: string,
  life: number,
  punch: number,
  kick: number,
  ...specials: Special[]
): Champion {
  return {
    name,
    baseLife: life,
    currentLife: life,
    punch,
    kick,
    rage: 0,
    hitsMade: 0,
    special: chainSpecials(...specials, reduceRage),
  }
}

type Special = (self: Champion, opponent: Champion) => void

function chainSpecials(...specials: Special[]): Special {
  return (self: Champion, opponent: Champion) => specials.forEach((sp) => sp(self, opponent))
}

const reduceRage: Special = (self) => (self.rage = 0)

function dmg(champ: Champion, dmg: number) {
  champ.currentLife -= dmg
  champ.rage++
}

export const init = () => {
  // Champ.|Life|Punch|Kick|Special Attack
  // KEN   |25  |6    |5   |3*rage
  // RYU   |25  |4    |5   |4*rage
  // TANK  |50  |2    |2   |2*rage
  // VLAD  |30  |3    |3   |2*(rage+opp.rage);opp.rage=0
  // JADE  |20  |2    |7   |number of hits made*rage
  // ANNA  |18  |9    |1   |damage received*rage
  // JUN   |60  |2    |1   |rage; and rage is added to JUN's life

  const allChamps = [
    createChamp('KEN', 25, 6, 5, (self, opponent) => dmg(opponent, 3 * self.rage)),
    createChamp('RYU', 25, 4, 5, (self, opponent) => dmg(opponent, 4 * self.rage)),
    createChamp('TANK', 50, 2, 2, (self, opponent) => dmg(opponent, 2 * self.rage)),
    createChamp(
      'VLAD',
      30,
      3,
      3,
      (self, opponent) => dmg(opponent, 2 * (self.rage + opponent.rage)),
      (self, opponent) => (opponent.rage = 0),
    ),
    createChamp('JADE', 20, 2, 7, (self, opponent) => dmg(opponent, self.hitsMade * self.rage)),
    createChamp('ANNA', 18, 9, 1, (self, opponent) =>
      dmg(opponent, (self.baseLife - self.currentLife) * self.rage),
    ),
    createChamp(
      'JUN',
      60,
      2,
      1,
      (self, opponent) => dmg(opponent, self.rage),
      (self, opponent) => (self.currentLife += self.rage),
    ),
  ]

  const champs: { [name: string]: Champion } = {}
  allChamps.forEach((champ) => (champs[champ.name] = champ))

  const [champion1, champion2] = readline()
    .split(' ')
    .map((c) => champs[c])
  const N = +readline()
  const lines = [...Array(N)].map(() => readline().split(' '))

  const attackedPicker = { '<': champion1, '>': champion2 }
  const attackerPicker = { '<': champion2, '>': champion1 }
  for (let [direction, attack] of lines) {
    // @ts-ignore
    const [attacker, attacked] = [attackerPicker, attackedPicker].map((c) => c[direction]) as [
      Champion,
      Champion,
    ]
    if (attack === 'PUNCH') {
      dmg(attacked, attacker.punch)
    } else if (attack === 'KICK') {
      dmg(attacked, attacker.kick)
    } else if (attack === 'SPECIAL') {
      attacker.special(attacker, attacked)
    }
    attacker.hitsMade++

    if (attacked.currentLife <= 0) {
      break
    }
  }
  const [winner, loser] = [champion1, champion2].sort((c1, c2) => c2.currentLife - c1.currentLife)
  print(`${winner.name} beats ${loser.name} in ${winner.hitsMade} hits`)
}
