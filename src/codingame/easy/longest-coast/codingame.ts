// LongestCoast
import { doOnAdjacentComplete, Position, positionAreEquals, valueOf } from '@lib/matrix/matrix'
import { sortByMultiCriteria } from '@lib/array/arrays'

interface Island {
  index: number
  earthBlocks: Position[]
  waterBlocks: Position[]
}

function containsEarth(island: Island, p: Position): boolean {
  return island.earthBlocks.some((block) => positionAreEquals(block, p))
}

function containsWater(island: Island, p: Position): boolean {
  return island.waterBlocks.some((block) => positionAreEquals(block, p))
}

function recursivelyFillIsland(map: string[][], island: Island, p: Position) {
  doOnAdjacentComplete(map, p, { considerDiagonal: false, periodic: false }, (nextPoint) => {
    const nextSymbol = valueOf(map, nextPoint)
    if (nextSymbol === '#' && !containsEarth(island, nextPoint)) {
      island.earthBlocks.push(nextPoint)
      recursivelyFillIsland(map, island, nextPoint)
    } else if (nextSymbol === '~' && !containsWater(island, nextPoint)) {
      island.waterBlocks.push(nextPoint)
    }
  })
}

export const init = () => {
  const N = +readline()
  const map = [...Array(N)].map(() => readline().split(''))

  const islands: Island[] = []

  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
      const position = { x: i, y: j }
      if (map[i][j] === '#') {
        if (!islands.some((island) => containsEarth(island, position))) {
          const island: Island = {
            index: islands.length + 1,
            earthBlocks: [position],
            waterBlocks: [],
          }
          recursivelyFillIsland(map, island, position)
          islands.push(island)
        }
      }
    }
  }

  // The index of the island with the longest coast followed by the amount of water it holds separated by a space.
  const sortedIsland = islands.sort(
    sortByMultiCriteria(
      (i1, i2) => i2.waterBlocks.length - i1.waterBlocks.length,
      (i1, i2) => i1.index - i2.index,
    ),
  )

  const maxIsland = sortedIsland[0]
  print(`${maxIsland.index} ${maxIsland.waterBlocks.length}`)
}
