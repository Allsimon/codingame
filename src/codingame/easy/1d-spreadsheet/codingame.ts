// 1dSpreadsheet
function parseRef(ref: string) {
  return +ref.substring(1)
}

function hasNotFinishedParsing(values: number[]): boolean {
  return values.some(isNaN)
}

export const init = () => {
  const N = +readline()

  const lines = [...Array(N)].map(() => readline().split(' '))
  const parsed: number[] = [...Array(N)].map(() => NaN)

  while (hasNotFinishedParsing(parsed)) {
    lines.forEach((splitted, i) => {
      const [oper, value1, value2] = splitted

      const ref1 = value1.startsWith('$') ? parsed[parseRef(value1)] : +value1
      const ref2 = value2.startsWith('$') ? parsed[parseRef(value2)] : +value2

      if (isNaN(ref1)) {
        return
      }
      if (oper === 'VALUE') {
        parsed[i] = ref1
      }
      if (isNaN(ref2)) {
        return
      }
      if (oper === 'ADD') {
        parsed[i] = ref1 + ref2
      } else if (oper === 'ADD') {
        parsed[i] = ref1 + ref2
      } else if (oper === 'SUB') {
        parsed[i] = ref1 - ref2
      } else if (oper === 'MULT') {
        parsed[i] = ref1 * ref2
      }
    })
  }
  parsed
    // '-0' fail on web version
    .map((value) => (value === -0 ? 0 : value))
    .forEach((value) => print(value))
}
