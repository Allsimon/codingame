// AChildsPlay

import { findInMatrix } from '@lib/matrix/matrix'

function id(x: number, y: number, dir: number) {
  return x + '-' + y + '-' + dir
}

export const init = () => {
  const [w, h] = readline().split(' ').map(Number)
  let n = +readline()
  const map = [...Array(h)].map(() => readline().split(''))

  let [y, x] = findInMatrix(map, 'O')
  const direction: Dir[] = ['UP', 'RIGHT', 'DOWN', 'LEFT']
  let currentDir = 0

  const loop = [id(x, y, currentDir)]

  while (n > 0) {
    const [nextX, nextY] = {
      UP: [x, y - 1],
      DOWN: [x, y + 1],
      LEFT: [x - 1, y],
      RIGHT: [x + 1, y],
    }[direction[currentDir]]
    const nextPos = [nextX, nextY]
    const nextCase = map[nextY][nextX]
    const canMove = nextCase !== '#'

    const nextId = id(nextX, nextY, currentDir)
    if (n > loop.length) {
      if (loop.includes(nextId)) {
        const startOfLoop = loop.indexOf(nextId)
        for (let i = 0; i < startOfLoop; i++) {
          loop.shift()
        }
        n = n % loop.length
      } else if (canMove) {
        loop.push(nextId)
      }
    }

    if (canMove) {
      ;[x, y] = nextPos
      n--
    } else {
      currentDir = (currentDir + 1) % direction.length
    }
  }

  print(`${x} ${y}`)
}

type Dir = 'UP' | 'RIGHT' | 'DOWN' | 'LEFT'
