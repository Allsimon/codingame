export const init = () => {
  const N = parseInt(readline())
  if (!N) return
  const commands = readline().split(';')

  const road = new Array<string>()
  for (let i = 0; i < N; i++) {
    const roadPattern = readline().split(';')
    for (let j = 0; j < +roadPattern[0]; j++) {
      road.push(roadPattern[1])
    }
  }

  let position = +commands[0] - 1
  let index = 0

  const replaceAt = (str: string, index: number, chr: string) =>
    str.substr(0, index) + chr + str.substr(index + 1)

  for (let i = 1; i < commands.length; i++) {
    const number = +commands[i].slice(0, -1)
    const action = commands[i].slice(-1)
    const loopToDo = index + number
    for (; index < loopToDo; index++) {
      if (action === 'L') position--
      if (action === 'R') position++
      if (index < road.length) road[index] = replaceAt(road[index], position, '#')
    }
  }
  road.forEach((r) => print(r))
}
