// SixDegreesOfKevinBacon
import { foldLeft } from '@lib/array/arrays'
import { Dijkstra } from '@lib/graph/dijkstra/dijkstra'

export const init = () => {
  const wantedActor = readline()
  const N = +readline()
  const movies = [...Array(N)].map(() => readline()).map((line) => line.split(': '))

  const moviesMap = foldLeft(movies, {} as Record<string, string[]>, (map, line) => {
    map[line[0]] = line[1].split(', ')
    return map
  })
  const graph = new Dijkstra()

  for (let title in moviesMap) {
    const actors = moviesMap[title]
    ;[...actors].forEach((actor) => {
      const otherActors = actors.filter((a) => a !== actor)
      graph.addVertexDistanceOneBiDir(actor, ...otherActors)
    })
  }
  const shortestPath = graph.shortestPath(wantedActor, 'Kevin Bacon')

  print(shortestPath.length - 1)
}
