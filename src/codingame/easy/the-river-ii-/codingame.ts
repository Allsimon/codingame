namespace TheRiverII {
  const r1 = parseInt(readline())

  const computeNextNumber = (seed: number): number => {
    const seedString = '' + seed
    let addition = 0
    for (let i = seedString.length - 1; i >= 0; i--) {
      addition += +seedString[i]
    }
    return seed + addition
  }

  const badNumbers = new Set([0])

  const riverMeet = (meetingPoint: number): boolean => {
    for (let i = 1; i < meetingPoint; i++) {
      let currentNumber = i

      const visitedNumbers = [currentNumber]
      while (currentNumber < meetingPoint && !badNumbers.has(currentNumber)) {
        currentNumber = computeNextNumber(currentNumber)
        visitedNumbers.push(currentNumber)
      }
      visitedNumbers.forEach((n) => badNumbers.add(n))

      if (currentNumber === meetingPoint) return true
    }
    return false
  }

  print(riverMeet(r1) ? 'YES' : 'NO')
}
