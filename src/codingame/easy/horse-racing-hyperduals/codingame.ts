import { manhattanDistance } from '@lib/geometry/distance'

export const init = () => {
  const number = +readline()
  if (!number) return
  const VECTORS = Array(number)
    .fill(0)
    .map((_) => readline().split(' ').map(Number))
  const curriedReduce = (curr1: number[]) => (acc2: number, curr2: number[]) =>
    Math.min(curr1 === curr2 ? Infinity : manhattanDistance(curr1, curr2), acc2)

  const minReduce = (acc1: number, curr1: number[]) =>
    Math.min(acc1, VECTORS.reduce(curriedReduce(curr1), Infinity))

  print(VECTORS.reduce(minReduce, Infinity))
}
