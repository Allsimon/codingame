// ShootEnemyAircraft

import { findAllInMatrix, findInMatrix, Position } from '@lib/matrix/matrix'
import { reduceToObject } from '@lib/array/arrays'

function findPlanes(map: string[][]): Position[] {
  return findAllInMatrix(map, (char) => char === '>' || char === '<')
}

export const init = () => {
  const N = +readline()
  const map = [...Array(N)].map(() => readline().split(''))

  // expect only one cannon
  const [x, y] = findInMatrix(map, '^')
  const cannon = { x, y }

  const shot = findPlanes(map).map(
    (plane) => Math.abs(plane.y - cannon.y) - Math.abs(plane.x - cannon.x) - 1,
  )
  const mapOfShot = reduceToObject(
    shot,
    (s) => s,
    (s) => true,
  )

  const max = Math.max(...shot)
  for (let i = 0; i <= max; i++) {
    print(mapOfShot[i] ? 'SHOOT' : 'WAIT')
  }
}
