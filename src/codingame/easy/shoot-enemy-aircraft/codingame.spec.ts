import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/shoot-enemy-aircraft/codingame'

describe('ShootEnemyAircraft', () => {
  it(`One aircraft on each side`, () =>
    runTest(
      init,
      `6
....................
.>..................
...................<
....................
....................
_________^__________`,
      `WAIT
WAIT
WAIT
SHOOT
WAIT
WAIT
SHOOT`,
    ))

  it(`Three on the same side`, () =>
    runTest(
      init,
      `6
>...................
>...................
>...................
....................
....................
_________^__________`,
      `WAIT
WAIT
WAIT
SHOOT
SHOOT
SHOOT`,
    ))

  it(`Hold the line`, () =>
    runTest(
      init,
      `2
...<<<<<<<<<<<<<<<<<<<<<<<..<<<
^______________________________`,
      `WAIT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
WAIT
WAIT
SHOOT
SHOOT
SHOOT`,
    ))

  it(`Don't panic`, () =>
    runTest(
      init,
      `10
...........................<...........
.......................................
.......................................
.......................................
............<..<..<..<..<..<..<..<..<..
.......................................
.......................................
.......................................
..>......<..<..<..<..<..<..<..<..<..<..
______^________________________________`,
      `SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
WAIT
SHOOT
SHOOT
WAIT
SHOOT
SHOOT
SHOOT
SHOOT
SHOOT
WAIT
SHOOT
SHOOT
WAIT
SHOOT
SHOOT
WAIT
SHOOT
SHOOT
WAIT
SHOOT
SHOOT
WAIT
WAIT
SHOOT`,
    ))

  // see https://www.codingame.com/forum/t/community-puzzle-shoot-enemy-aircraft/199381/13

  it(`Manual test case`, () =>
    runTest(
      init,
      `2
>.>..
____^`,
      `SHOOT
WAIT
SHOOT`,
    ))
})
