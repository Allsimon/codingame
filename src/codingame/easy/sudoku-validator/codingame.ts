// SudokuValidator

import { Position, doOnAdjacent, doOnAdjacentComplete, valueOf } from '@lib/matrix/matrix'

function checkSubgrid(lines: number[][], p: Position): boolean {
  const found: number[] = [valueOf(lines, p)]

  doOnAdjacentComplete(lines, p, { considerDiagonal: true }, (adjacent) =>
    found.push(valueOf(lines, adjacent)),
  )

  return checkNumbers(found)
}

function checkHorizontal(lines: number[][]): boolean {
  for (let i = 0; i < lines.length; i++) {
    const found: number[] = []
    for (let j = 0; j < lines[i].length; j++) {
      found.push(lines[i][j])
    }
    if (!checkNumbers(found)) {
      return false
    }
  }
  return true
}

function checkVertical(lines: number[][]) {
  const found: number[] = []
  for (let i = 0; i < lines.length; i++) {
    const found: number[] = []
    for (let j = 0; j < lines[i].length; j++) {
      found.push(lines[j][i])
    }
    if (!checkNumbers(found)) {
      return false
    }
  }
  return checkNumbers(found)
}

function checkNumbers(result: number[]): boolean {
  const sorted = result.sort()
  for (let i = 0; i < result.length; i++) {
    if (sorted[i] !== i + 1) {
      return false
    }
  }
  return true
}

export const init = () => {
  const lines = [...Array(9)].map(() => readline().split(' ').map(Number))

  const everythingOK = [
    checkHorizontal(lines),
    checkVertical(lines),
    ...[
      [1, 1],
      [4, 1],
      [7, 1],
      [1, 4],
      [4, 4],
      [7, 4],
      [1, 7],
      [4, 7],
      [7, 7],
    ].map(([x, y]) => checkSubgrid(lines, { x, y })),
  ].every((t) => t)
  print('' + everythingOK)
}
