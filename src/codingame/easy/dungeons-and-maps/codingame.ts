// DungeonsAndMaps
import { MatrixNavigationParameter, Position, positionAreEquals } from '@lib/matrix/matrix'

function navigateMap(map: string[][], p: Position, positionsChecked: Position[]): Position[] {
  const currentSymbol = map[p.x][p.y]
  if (currentSymbol === 'T') {
    return positionsChecked
  }

  if (!'^v<>'.includes(currentSymbol)) {
    return []
  }
  if (positionsChecked.some((alreadyChecked) => positionAreEquals(alreadyChecked, p))) {
    // cycle
    return []
  }
  positionsChecked.push(p)
  if (currentSymbol === '^') {
    return navigateMap(map, { x: p.x - 1, y: p.y }, positionsChecked)
  } else if (currentSymbol === 'v') {
    return navigateMap(map, { x: p.x + 1, y: p.y }, positionsChecked)
  } else if (currentSymbol === '<') {
    return navigateMap(map, { x: p.x, y: p.y - 1 }, positionsChecked)
  } else if (currentSymbol === '>') {
    return navigateMap(map, { x: p.x, y: p.y + 1 }, positionsChecked)
  }
  throw new Error("Can't parse map: " + currentSymbol)
}

export const init = () => {
  const navParam: MatrixNavigationParameter = {
    considerDiagonal: false,
    periodic: false,
  }

  const [W, H] = readline().split(' ').map(Number)
  const [startRow, startCol] = readline().split(' ').map(Number)
  const N = +readline()
  const maps = [...Array(N)].map(() => [...Array(H)].map(() => readline().split('')))

  const navigations = maps.map((map) => navigateMap(map, { x: startRow, y: startCol }, []))

  const sortedNavs = navigations
    .map((nav, i) => ({ map: i, length: nav.length }))
    .filter((map) => map.length !== 0)
    .sort((a, b) => a.length - b.length)

  if (sortedNavs.length === 0) {
    print('TRAP')
  } else {
    print(sortedNavs[0].map)
  }
}
