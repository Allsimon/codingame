// Asteroids
import { cloneAndReplaceInMatrix, findAllInMatrix, isInMatrix, valueOf } from '@lib/matrix/matrix'
import { reduceToObject } from '@lib/array/arrays'

export const init = () => {
  const [W, H, t1, t2, t3] = readline().split(' ').map(Number)
  const lines = [...Array(H)].map(() => readline().split(' '))

  const image1 = lines.map((v) => v[0].split(''))
  const image2 = lines.map((v) => v[1].split(''))

  const asteroidInImage1 = reduceToObject(
    findAllInMatrix(image1, (char) => char !== '.'),
    (p) => valueOf(image1, p),
    (p) => p,
  )
  const asteroidInImage2 = reduceToObject(
    findAllInMatrix(image2, (char) => char !== '.'),
    (p) => valueOf(image2, p),
    (p) => p,
  )

  const output = cloneAndReplaceInMatrix(image1, () => '.')

  Object.keys(asteroidInImage1)
    .sort()
    .reverse()
    .forEach((asteroid) => {
      const p1 = asteroidInImage1[asteroid]
      const p2 = asteroidInImage2[asteroid]

      const x = p1.x + Math.floor(((p2.x - p1.x) / (t2 - t1)) * (t3 - t1))
      const y = p1.y + Math.floor(((p2.y - p1.y) / (t2 - t1)) * (t3 - t1))
      if (isInMatrix(output, { x, y })) {
        output[x][y] = asteroid
      }
    })
  output.forEach((line) => print(line.join('')))
}
