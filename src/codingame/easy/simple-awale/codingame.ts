export const init = () => {
  const BOWL_COUNT = 7
  const bowls = [readline(), readline()].map((b) => b.split(' ').map((a) => +a))

  const num = parseInt(readline())
  let turnToPlay = bowls[1][num]
  bowls[1][num] = 0

  const finishInReserve = turnToPlay + num === BOWL_COUNT - 1

  for (let i = num + 1; i <= num + turnToPlay; i++) {
    const laneToPlay = (Math.floor(i / BOWL_COUNT) + 1) % 2
    // going to play in other player reserve
    if (laneToPlay === 0 && i % BOWL_COUNT === 6) {
      turnToPlay++
      continue
    }

    bowls[laneToPlay][i % BOWL_COUNT]++
  }

  const printBowl = (bowl: number[]) =>
    bowl.map((a, i) => (i !== BOWL_COUNT - 1 ? a : `[${a}]`)).join(' ')

  bowls.map((b) => printBowl(b)).forEach((b) => print(b))

  if (finishInReserve) print('REPLAY')
}
