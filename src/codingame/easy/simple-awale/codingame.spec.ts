import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/simple-awale/codingame'

describe('SimpleAwale', () => {
  it('Test 1', () =>
    runTest(
      init,
      `5 1 0 6 2 2 3
3 4 0 3 3 2 2
0`,
      `5 1 0 6 2 2 [3]
0 5 1 4 3 2 [2]`,
    ))

  it('Test 2', () =>
    runTest(
      init,
      `5 1 0 6 2 2 3
3 4 0 3 3 2 2
5`,
      `6 1 0 6 2 2 [3]
3 4 0 3 3 0 [3]`,
    ))

  it('Test 3', () =>
    runTest(
      init,
      `5 1 0 6 2 2 3
3 4 0 3 3 2 2
3`,
      `5 1 0 6 2 2 [3]
3 4 0 0 4 3 [3]
REPLAY`,
    ))

  it('Test 4', () =>
    runTest(
      init,
      `4 4 4 4 4 4 0
4 4 4 4 4 4 0
2`,
      `4 4 4 4 4 4 [0]
4 4 0 5 5 5 [1]
REPLAY`,
    ))

  it('Test 5', () =>
    runTest(
      init,
      `4 2 1 7 0 6 2
5 14 1 4 0 2 3
1`,
      `5 3 2 8 1 7 [2]
6 1 3 5 1 3 [4]`,
    ))
})
