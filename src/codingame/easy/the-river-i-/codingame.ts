namespace TheRiverI {
  const r1 = parseInt(readline())
  const r2 = parseInt(readline())

  // @see https://www.codingame.com/ide/puzzle/the-river-i-
  const computeNextNumber = (seed: number): number => {
    const seedString = '' + seed
    let addition = 0
    for (let i = seedString.length - 1; i >= 0; i--) {
      addition += +seedString[i]
    }
    return seed + addition
  }

  // apparently overflow with `Even bigger` validator
  const findMeetingRiverRecursive = (n1: number, n2: number): number => {
    const min = Math.min(n1, n2)
    const max = Math.max(n1, n2)
    return min === max ? min : findMeetingRiverRecursive(computeNextNumber(min), max)
  }

  const findMeetingRiverLoop = (n1: number, n2: number): number => {
    while (n1 !== n2) {
      const min = Math.min(n1, n2)
      const max = Math.max(n1, n2)

      n1 = computeNextNumber(min)
      n2 = max
    }
    return n1
  }

  print(findMeetingRiverLoop(r1, r2))
}
