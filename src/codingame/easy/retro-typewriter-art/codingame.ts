// RetroTypewriterArt
export const init = () => {
  const recipe = readline()

  const prettyPrinted = recipe
    .split(' ')
    // Abbreviations used:
    // sp = space
    // bS = backSlash \
    // sQ = singleQuote '
    // nl = NewLine
    .map((pattern) => pattern.replace('sp', ' ').replace('bS', '\\').replace('sQ', "'"))
    .map((pattern) => {
      if (pattern === 'nl') {
        return '\n'
      }
      const number = +pattern.substring(0, pattern.length - 1)
      const symbol = pattern.slice(-1)
      return symbol.repeat(number)
    })
    .join('')

  print(prettyPrinted)
}
