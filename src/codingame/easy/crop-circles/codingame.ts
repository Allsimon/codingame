// CropCircles

import { miniPrintMatrix } from '@lib/matrix/matrix'

const enum Action {
  MOW,
  PLANT,
  PLANTMOW,
}

interface CirclePlant {
  x: number
  y: number
  diameter: number
  action: Action
}

function parse(line: string): CirclePlant {
  let action = Action.MOW
  if (line.startsWith('PLANTMOW')) {
    action = Action.PLANTMOW
    line = line.replace('PLANTMOW', '')
  } else if (line.startsWith('PLANT')) {
    action = Action.PLANT
    line = line.replace('PLANT', '')
  }
  const [w, h, ...rest] = line

  return { x: h.charCodeAt(0) - 97, y: w.charCodeAt(0) - 97, diameter: +rest.join(''), action }
}

function draw(circle: CirclePlant, map: string[][]) {
  for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
      if (Math.sqrt(Math.pow(circle.x - i, 2) + Math.pow(circle.y - j, 2)) <= circle.diameter / 2) {
        switch (circle.action) {
          case Action.MOW:
            map[i][j] = '  '
            break
          case Action.PLANT:
            map[i][j] = '{}'
            break
          case Action.PLANTMOW:
            map[i][j] = map[i][j] === '  ' ? '{}' : '  '
            break
        }
      }
    }
  }
}

export const init = () => {
  const circles = readline().split(' ').map(parse)
  const map = [...Array(25)].map(() => [...Array(19)].map(() => '{}'))
  circles.forEach((circle) => draw(circle, map))

  const output = miniPrintMatrix(map)
  print(output)
}
