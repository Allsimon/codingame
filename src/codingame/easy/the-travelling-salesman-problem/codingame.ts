namespace TheTravellingSalesmanProblem {
  interface City {
    X: number
    Y: number
    travelled: boolean
  }

  const N = parseInt(readline())

  const distanceBetween = (a: City, b: City) =>
    Math.sqrt(Math.pow(a.X - b.X, 2) + Math.pow(a.Y - b.Y, 2))

  const cities: City[] = []
  for (let i = 0; i < N; i++) {
    const inputs = readline().split(' ')
    cities.push({
      X: parseInt(inputs[0]),
      Y: parseInt(inputs[1]),
      travelled: i === 0,
    })
  }

  let currentIndex = 0
  let totalDistance = 0

  let nextCity: City | undefined
  while (cities.map((c) => c.travelled).includes(false)) {
    nextCity = undefined
    let distance = Number.MAX_VALUE
    let tempIndex = 0

    for (let i = 0; i < cities.length; i++) {
      if (!cities[i].travelled) {
        const currentDistance = distanceBetween(cities[i], cities[currentIndex])
        if (currentDistance < distance) {
          nextCity = cities[i]
          tempIndex = i
          distance = currentDistance
        }
      }
    }

    if (!!nextCity) {
      nextCity.travelled = true
      currentIndex = tempIndex
      totalDistance += distance
    }
  }
  if (!!nextCity) totalDistance += distanceBetween(cities[0], nextCity)

  print(Math.round(totalDistance))
}
