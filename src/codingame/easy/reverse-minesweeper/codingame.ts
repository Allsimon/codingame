// ReverseMinesweeper
import { doOnAdjacentComplete, findAllInMatrix, valueOf } from '@lib/matrix/matrix'

export const init = () => {
  const W = +readline()
  const H = +readline()
  const lines = [...Array(H)].map(() => readline().split(''))

  const mines = findAllInMatrix(lines, (p) => p === 'x')

  mines.forEach((mine) =>
    doOnAdjacentComplete(lines, mine, { considerDiagonal: true }, (p) => {
      if (valueOf(lines, p) === '.') {
        lines[p.x][p.y] = '1'
      } else if (valueOf(lines, p) === 'x') {
        // do nothing
      } else {
        lines[p.x][p.y] = +lines[p.x][p.y] + 1 + ''
      }
    }),
  )

  lines.forEach((line) => print(line.map((l) => (l === 'x' ? '.' : l)).join('')))
}
