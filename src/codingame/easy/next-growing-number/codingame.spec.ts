import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/next-growing-number/codingame'

describe('NextGrowingNumber', () => {
  it(`Test 1`, () => runTest(init, `19`, `22`))

  it(`Test 2`, () => runTest(init, `99`, `111`))

  it(`Test 3`, () => runTest(init, `2533`, `2555`))

  it(`Test 4`, () => runTest(init, `123456879`, `123456888`))

  it(`Test 5`, () => runTest(init, `11123159995399999`, `11123333333333333`))
})
