// NextGrowingNumber

function isGrowing(value: string): boolean {
  const myBalls = ('' + value).split('').sort().join('')
  return value === myBalls
}

function grow(value: string): string {
  const numbers = ('' + value).split('').map(Number)
  let min = 0
  let hasGrown = false
  for (let i = 0; i < numbers.length; i++) {
    if (hasGrown) {
      numbers[i] = min
    } else if (numbers[i] < min) {
      numbers[i] = min
      hasGrown = true
    } else if (!hasGrown) {
      min = numbers[i]
    }
  }
  return numbers.join('')
}

export const init = () => {
  let N = '' + (+readline() + 1)

  while (!isGrowing(N)) {
    // empty on purpose
    N = grow(N)
  }
  print(N)
}
