// PersonalBest
export const init = () => {
  const gymnasts = readline().split(',')
  const categories = readline().split(',')

  const N = +readline()

  const result: any = {}
  for (let i = 0; i < N; i++) {
    const [name, barsS, beamS, floorS] = readline().split(',')
    const bars = Math.max(result[name]?.bars || +barsS, +barsS)
    const beam = Math.max(result[name]?.beam || +beamS, +beamS)
    const floor = Math.max(result[name]?.floor || +floorS, +floorS)

    result[name] = { bars, beam, floor }
  }

  gymnasts
    .map((gymnast) => categories.map((cat) => result[gymnast][cat]).join(','))
    .forEach((result) => print(result))
}
