// Abcdefghijklmnopqrstuvwxyz
import {
  cloneMatrix,
  doOnAdjacentComplete,
  findAllInMatrix,
  MatrixNavigationParameter,
  Position,
  replaceInMatrix,
  valueOf,
} from '@lib/matrix/matrix'

const navParam: MatrixNavigationParameter = { considerDiagonal: false }

function parse(
  matrix: string[][],
  p: Position,
  currentChain: Position[],
  callback: (complete: Position[]) => void,
) {
  const newChain = [...currentChain, p]

  if (currentChain.length === 0) {
    doOnAdjacentComplete(matrix, p, navParam, (newPos) => parse(matrix, newPos, newChain, callback))
  } else {
    const currentLetter = valueOf(matrix, p).charCodeAt(0)
    const previousLetter = valueOf(matrix, currentChain[currentChain.length - 1]).charCodeAt(0)
    if (previousLetter === currentLetter - 1) {
      if (newChain.length === 26) {
        callback(newChain)
      } else {
        doOnAdjacentComplete(matrix, p, navParam, (newPos) =>
          parse(matrix, newPos, newChain, callback),
        )
      }
    }
  }
}

export const init = () => {
  const N = +readline()
  const lines = [...Array(N)].map(() => readline().split(''))

  let output: Position[][] = []
  findAllInMatrix(lines, (char) => char === 'a').forEach((letterA) =>
    parse(lines, letterA, [], (complete) => output.push(complete)),
  )

  const cloned = cloneMatrix(lines)
  replaceInMatrix(
    cloned,
    (_) => true,
    (_) => '-',
  )
  output.flatMap((i) => i).forEach((r) => (cloned[r.x][r.y] = valueOf(lines, r)))

  cloned.forEach((line) => print(line.join('')))
}
