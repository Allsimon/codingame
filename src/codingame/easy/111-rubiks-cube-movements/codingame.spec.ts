import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/111-rubiks-cube-movements/codingame'

describe('RubiksCubeMovements', () => {
  it('One rotation', () =>
    runTest(
      init,
      `z
D
L`,
      `L
U`,
    ))

  it('Two rotations', () =>
    runTest(
      init,
      `y z'
B
D`,
      `U
R`,
    ))

  it('Give me five!', () =>
    runTest(
      init,
      `x y x' z y'
L
B`,
      `B
L`,
    ))

  it('Identity', () =>
    runTest(
      init,
      `x y x y x y
F
D`,
      `F
D`,
    ))

  it('A long route', () =>
    runTest(
      init,
      `x y z x z y y x z y z x z x y z y x
L
F`,
      `F
D`,
    ))

  it('Stuttering', () =>
    runTest(
      init,
      `x x x y y y z z z
B
U`,
      `L
U`,
    ))
})
