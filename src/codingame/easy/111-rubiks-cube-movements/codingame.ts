import { Mapp } from '@lib/type/type'

export const init = () => {
  const rotations = readline().split(' ')
  const faces = [readline(), readline()]

  const original: Mapp<string> = {
    U: 'U',
    R: 'R',
    D: 'D',
    L: 'L',
    B: 'B',
    F: 'F',
  }

  const swap = (input: [string, string, string, string]) => {
    const U = original[input[0]]
    original[input[0]] = original[input[1]]
    original[input[1]] = original[input[2]]
    original[input[2]] = original[input[3]]
    original[input[3]] = U
  }

  const moveZ = () => swap(['U', 'L', 'D', 'R'])
  const moveZZ = () => swap(['R', 'D', 'L', 'U'])

  const moveX = () => swap(['D', 'B', 'U', 'F'])
  const moveXX = () => swap(['F', 'U', 'B', 'D'])

  const moveY = () => swap(['R', 'B', 'L', 'F'])
  const moveYY = () => swap(['F', 'L', 'B', 'R'])

  rotations.forEach((r) => {
    if (r === 'z') moveZ()
    if (r === `z'`) moveZZ()
    if (r === 'x') moveX()
    if (r === `x'`) moveXX()
    if (r === 'y') moveY()
    if (r === `y'`) moveYY()
  })

  const lookUp = (char: string) => {
    for (let key in original) {
      if (original[key] === char) {
        return key
      }
    }
  }

  faces.forEach((f) => print(lookUp(f)))
}
