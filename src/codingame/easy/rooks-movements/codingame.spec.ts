import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/rooks-movements/codingame'

describe('RooksMovements', () => {
  it('MOVING FREELY', () =>
    runTest(
      init,
      `d5
2
0 c1
1 e8`,
      `Rd5-a5
Rd5-b5
Rd5-c5
Rd5-d1
Rd5-d2
Rd5-d3
Rd5-d4
Rd5-d6
Rd5-d7
Rd5-d8
Rd5-e5
Rd5-f5
Rd5-g5
Rd5-h5`,
    ))

  it('CLOSE TO THE EDGE', () =>
    runTest(
      init,
      `a8
5
0 e8
1 d7
0 c6
1 b5
0 a4`,
      `Ra8-a5
Ra8-a6
Ra8-a7
Ra8-b8
Ra8-c8
Ra8-d8`,
    ))

  it('ONLY ALLIES', () =>
    runTest(
      init,
      `d5
2
0 g5
0 d2`,
      `Rd5-a5
Rd5-b5
Rd5-c5
Rd5-d3
Rd5-d4
Rd5-d6
Rd5-d7
Rd5-d8
Rd5-e5
Rd5-f5`,
    ))

  it('FOR FRODOOO', () =>
    runTest(
      init,
      `d5
3
0 g5
0 d2
1 d7`,
      `Rd5-a5
Rd5-b5
Rd5-c5
Rd5-d3
Rd5-d4
Rd5-d6
Rd5-e5
Rd5-f5
Rd5xd7`,
    ))
})
