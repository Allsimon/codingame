import { emptyMatrix, fillMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const rookP = readline()
  if (!rookP) return
  const nbPieces = parseInt(readline())

  const notationToNumber = (not: string): [number, number] => [
    8 - +not.charAt(1),
    not.charCodeAt(0) - 97,
  ]
  const pointToNotation = (n: number[]) => String.fromCharCode(n[1] + 97) + (8 - n[0])

  const parseShouldContinue = (x: number, y: number) => {
    const point = mat[x][y]

    if (point === 8) {
      output.push('-' + pointToNotation([x, y]))
      return true
    } else if (point === 1) {
      output.push('x' + pointToNotation([x, y]))
    }
    return false
  }

  const mat: number[][] = emptyMatrix(8, 8)
  fillMatrix(mat, () => 8)
  const rookPosition = notationToNumber(rookP)

  for (let i = 0; i < nbPieces; i++) {
    const inputs = readline().split(' ')
    const colour = parseInt(inputs[0])
    const onePiece = inputs[1]
    const place = notationToNumber(onePiece)
    mat[place[0]][place[1]] = colour
  }

  mat[rookPosition[0]][rookPosition[1]] = 5

  const output = new Array<string>()

  // left
  for (let i = rookPosition[0] - 1; i >= 0; i--) {
    if (!parseShouldContinue(i, rookPosition[1])) {
      break
    }
  }

  // right
  for (let i = rookPosition[0] + 1; i < 8; i++) {
    if (!parseShouldContinue(i, rookPosition[1])) {
      break
    }
  }
  // up
  for (let i = rookPosition[1] - 1; i >= 0; i--) {
    if (!parseShouldContinue(rookPosition[0], i)) {
      break
    }
  }

  // down
  for (let i = rookPosition[1] + 1; i < 8; i++) {
    if (!parseShouldContinue(rookPosition[0], i)) {
      break
    }
  }

  output
    .map((o) => 'R' + rookP + o)
    .sort()
    .forEach((a) => print(a))
}
