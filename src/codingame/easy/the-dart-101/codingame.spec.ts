import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/the-dart-101/codingame'

describe('TheDart101', () => {
  it(`2 players`, () =>
    runTest(
      init,
      `2
Hugo
Guillaume
10 5 3*18 15 5 4 8
5 5 10 2*19 5 6 2*5 1 20 1`,
      `Hugo`,
    ))

  it(`1 winner & 1 loser`, () =>
    runTest(
      init,
      `2
Lisa
Emma
10 5 3*18 15 5 4 8
5 5 2*18 9 3 2 8 1 3`,
      `Lisa`,
    ))

  it(`One missed`, () =>
    runTest(
      init,
      `2
Candice
Elise
10 5 3*18 20 X 2*14 4
5 5 10 2*19 5 6 2*5 1 20 1`,
      `Candice`,
    ))

  it(`Two missed`, () =>
    runTest(
      init,
      `2
Fred
Charles
10 6 3*18 X 19 X 2*25 2
5 5 10 2*19 5 6 2*5 1 20 1`,
      `Fred`,
    ))

  it(`Two missed consecutively`, () =>
    runTest(
      init,
      `2
Henry
Herve
2*5 5 5 2*19 5 6 10 1 20 1
3*17 5 12 X X 15 3*16 20`,
      `Herve`,
    ))

  it(`Three missed consecutively`, () =>
    runTest(
      init,
      `2
Eric
Cecile
2*5 5 5 2*19 5 6 10 1 20 1
3*17 5 12 X X X 2*25 3*17`,
      `Cecile`,
    ))

  it(`Over the score`, () =>
    runTest(
      init,
      `2
Noemie
Nicolas
2 5 5 19 5 6 10 1 20 1 2 5
3*17 5 12 5 2 3 15 9 20 3`,
      `Nicolas`,
    ))

  it(`2 players and lot of shoots`, () =>
    runTest(
      init,
      `2
Yoan
Ludo
20 1 5 2*5 3*18 X X 19 11 10 12 16 7 2*11 X 3*17 3*7
3*20 3*19 3*17 25 20 X X X 2*25 3*14 X X X 3*20 X 5 X 3*18`,
      `Ludo`,
    ))

  it(`4 players`, () =>
    runTest(
      init,
      `4
Eric
Delphine
Patricia
Yan
6 2 7 15 2*10 8 2 3 6 15 2 1 3 11
4 3 2 1 1 1 X X 10
X X X X X X X X X 2*25 3*17
3*15 2*10 3*5 2*12 2*7 2*7 3*15`,
      `Patricia`,
    ))

  it(`8 players`, () =>
    runTest(
      init,
      `8
Eric
Delphine
Patricia
Yan
David
Hugo
Ludo
Yoan
4 3 2 1 1 1 X X 10
X X X X X X X X X 2*25 3*17
6 2 7 15 2*10 8 2 3 6 15 2 1 3 11
3*15 2*10 3*5 2*12 2*7 2*7 3*15
2*25 3*17
10 5 3*18 20 X 2*14 4
5 5 10 2*19 5 6 2*5 1 20 1
10 5 3*18 20 X 2*14 10`,
      `David`,
    ))
})
