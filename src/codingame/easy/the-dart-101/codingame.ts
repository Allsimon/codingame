export const init = () => {
  const N = +readline()
  if (!N) return
  const players = [...Array(N)].map(() => readline())
  const shoots = [...Array(N)].map(() => readline().split(' '))

  const sum = (values: number[]) => values.reduce((a, b) => a + b, 0)

  const computeScore = (shoots: string[]) => {
    let score = 0

    let round = 0
    let currentShot = []

    for (let i = 0; i < shoots.length; i++) {
      const shoot = shoots[i]
      if (shoot === 'X') {
        if (currentShot[currentShot.length - 1] === 0) {
          if (currentShot.length === 3 && currentShot.join(' ') === '000') {
            score = 0
            currentShot = [0, 0, 0]
          } else {
            score -= 30
            currentShot.push(0)
          }
        } else {
          score -= 20
          currentShot.push(0)
        }
        score = score < 0 ? 0 : score
      } else {
        currentShot.push(eval(shoot))

        if (score + sum(currentShot) > 101) {
          round++
          currentShot = []
        }
      }
      if (currentShot.length === 3) {
        round++
        score += sum(currentShot)
        currentShot = []
      }
      if (score === 101 || score + sum(currentShot) === 101) {
        return round
      }
    }
    return 100 + round
  }

  const scores = shoots.map(computeScore)

  print(players[scores.indexOf(Math.min(...scores))])
}
