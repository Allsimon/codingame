// EquivalentResistanceCircuitBuilding
import { isNumeric } from '@lib/number/numbers'
import { Mapp } from '@lib/type/type'

function group(str: string, begin: string, end: string, op: (arr: number[]) => number): string {
  const split = str.split(' ')
  for (let i = 0; i < split.length; i++) {
    let subst: number[] = []
    if (split[i] === begin) {
      for (let j = i + 1; j < split.length; j++) {
        if (split[j] === end) {
          break
        } else if (!isNumeric(split[j])) {
          subst = []
        } else {
          subst.push(+split[j])
        }
      }
    }
    if (subst.length !== 0) {
      const result = op(subst)
      const replacement = subst.map((a) => '' + a)
      replacement.unshift(begin)
      replacement.push(end)
      str = str.replace(replacement.join(' '), '' + result)
    }
  }
  return str
}

function reduce(str: string): string {
  const reducedParenthesis = group(str, '(', ')', (arr) => arr.reduce((a, b) => a + b))
  const reduced = group(
    reducedParenthesis,
    '[',
    ']',
    (arr) => 1 / arr.map((a) => 1 / a).reduce((a, b) => a + b),
  )

  if (reduced !== str) {
    return reduce(reduced)
  }
  return reduced
}

export const init = () => {
  const N: number = +readline()
  const resistances: Mapp<number> = {}
  for (let i = 0; i < N; i++) {
    const [name, R] = readline().split(' ')
    resistances[name] = +R
  }
  const circuit: string = readline()

  const circuitVal = circuit
    .split(' ')
    .map((str) => resistances[str] || str)
    .join(' ')
  print('' + (+reduce(circuitVal)).toFixed(1))
}
