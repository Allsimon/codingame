import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/equivalent-resistance-circuit-building/codingame'

describe('EquivalentResistanceCircuitBuilding', () => {
  it(`Series`, () =>
    runTest(
      init,
      `2
A 20
B 10
( A B )`,
      `30.0`,
    ))

  it(`Parallel`, () =>
    runTest(
      init,
      `2
C 20
D 25
[ C D ]`,
      `11.1`,
    ))

  it(`Combined (Example Diagram)`, () =>
    runTest(
      init,
      `3
A 24
B 8
C 48
[ ( A B ) [ C A ] ]`,
      `10.7`,
    ))

  it(`Complex`, () =>
    runTest(
      init,
      `7
Alfa 1
Bravo 1
Charlie 12
Delta 4
Echo 2
Foxtrot 10
Golf 8
( Alfa [ Charlie Delta ( Bravo [ Echo ( Foxtrot Golf ) ] ) ] )`,
      `2.4`,
    ))

  it(`More Complex`, () =>
    runTest(
      init,
      `3
30 30
20 20
10 10
( 30 [ ( 20 20 20 ) ( 10 [ ( 10 10 ) ( 10 [ 20 20 ] ) ] ) ] )`,
      `45.0`,
    ))

  it(`5-pointed Star`, () =>
    runTest(
      init,
      `1
Star 78
[ ( [ Star ( Star Star ) ] [ Star ( Star Star ) ] Star ) ( [ Star ( Star Star ) ] [ Star ( Star Star ) ] Star ) ]`,
      `91.0`,
    ))
})
