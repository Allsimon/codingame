export const init = () => {
  const sequence = readline()

  let currentDepth = 0

  const score: any = {}
  for (let i = 0; i < sequence.length; i++) {
    const char = sequence.charAt(i)
    if (char === '-') {
      currentDepth--
      i++
      continue
    } else {
      currentDepth++
    }

    score[char] = (score[char] || 0) + 1 / currentDepth
  }

  let maxScore = 0
  let max = ''
  for (let key in score) {
    if (score[key] > maxScore) {
      max = key
      maxScore = score[key]
    }
  }
  print(max)
}
