import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/xml-mdf-2016/codingame'

describe('XmlMdf2016', () => {
  it('Only one', () => runTest(init, `aa-aa-a-a`, `a`))

  it('Nested might lose', () => runTest(init, `ab-b-a`, `a`))

  it('Several nested might win', () => runTest(init, `ab-bb-bb-b-a`, `b`))

  it('Several deeper nested might win too', () => runTest(init, `abc-cc-c-bc-cc-c-a`, `c`))

  it('Only one shall win', () => runTest(init, `abc-cc-cc-cc-c-bd-dd-d-a`, `c`))

  it('Size should not matter', () =>
    runTest(
      init,
      `nu-u-nim-mo-o-irjlncx-xzd-d-z-cg-gma-a-m-n-l-j-rff-f-fo-onkbwn-nf-f-wra-a-rlbaf-ftesov-v-o-sld-d-l-e-t-a-b-lch-ha-aw-w-c-bxt-t-x-k-net-t-eoif-f-i-ofe-eka-a-kc-c-fyvv-v-v-yx-xsjuuaf-fd-d-a-u-u-j-suewunq-q-n-u-w-er-rmd-dum-mhq-quo-o-utip-p-ivgcm-m-cg-g-gt-t-v-t-h-u-m-u`,
      `f`,
    ))
})
