// CardCountingWhenEasilyDistracted

import { reduceToObject } from '@lib/array/arrays'

function isValid(line: string): boolean {
  // Abbreviations used, and values:
  // • K = King
  // • Q = Queen
  // • J = Jack
  // • T = Ten
  // (each of the above has a value of 10)
  // • A = Ace (has a value of 1)
  // • Each number card (2 through 9, inclusive) has its own face value
  return /^([2-9]|K|Q|J|T|A)+$/.test(line)
}

function score(card: string): number {
  switch (card) {
    case 'K':
    case 'Q':
    case 'J':
    case 'T':
      return 10
    case 'A':
      return 1
    default:
      return +card
  }
}

export const init = () => {
  const streamOfConsciousness = readline()
  const bustThreshold = +readline()
  const deck = reduceToObject(
    'A23456789TJQK'.split(''),
    (t) => t,
    (_) => 4,
  )

  streamOfConsciousness
    .split('.')
    .filter(isValid)
    .forEach((line) => line.split('').forEach((char) => deck[char]--))

  let result = { below: 0, over: 0 }
  Object.entries(deck).forEach(
    ([card, amount]) => (result[score(card) < bustThreshold ? 'below' : 'over'] += amount),
  )

  const output = Math.round((100 * result.below) / (result.below + result.over))
  print(`${output}%`)
}
