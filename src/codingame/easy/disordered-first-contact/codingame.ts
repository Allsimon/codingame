export const init = () => {
  const N = parseInt(readline())
  let MESSAGE = readline()

  const sum = (i: number) => (i * (i + 1)) / 2

  const encode = (message: string) => {
    let output = ''
    for (let i = 1; i < message.length; i++) {
      const amountAlreadyRead = sum(i)
      let amountToRead = sum(i - 1)

      const finished = amountToRead > message.length
      if (finished) amountToRead = message.length

      const substr = message.slice(amountToRead, amountAlreadyRead)
      output = i % 2 ? output + substr : substr + output
    }
    return output
  }

  const decode = (message: string) => {
    let i = 0

    while (sum(i) < message.length) i++
    const left = message.length - sum(i - 1)

    let output = i % 2 ? message.substr(message.length - left, left) : message.substr(0, left)
    let mutableMessage = message.replace(output, '')
    for (let j = i - 1; j >= 0; j--) {
      const amountToRead = j
      let substr =
        j % 2
          ? mutableMessage.substr(mutableMessage.length - amountToRead, mutableMessage.length)
          : mutableMessage.substr(0, amountToRead)
      mutableMessage = mutableMessage.replace(substr, '')
      output = substr + output
    }
    return output
  }

  const action = N < 0 ? encode : decode
  for (let i = 0; i < Math.abs(N); i++) {
    MESSAGE = action(MESSAGE)
  }
  print(MESSAGE)
}
