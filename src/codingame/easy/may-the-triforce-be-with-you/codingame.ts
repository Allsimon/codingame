export const init = () => {
  const spaces = (n: number) => ' '.repeat(n)
  const star = (n: number) => '*'.repeat(n)

  const N = parseInt(readline())

  print('.' + spaces(N + N - 2) + star(1))

  for (let i = 1; i < N; i++) {
    print(spaces(N + N - i - 1) + star(i * 2 + 1))
  }

  for (let i = 0; i < N; i++) {
    let stars = star(i * 2 + 1)
    let space = spaces(N - i - 1)
    print(space + stars + space + ' ' + space + stars)
  }
}
