import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/expand-the-polynomial/codingame'

describe('ExpandThePolynomial', () => {
  it(`Simple product`, () => runTest(init, `(x-1)*(x+2)`, `x^2+x-2`))

  it(`Square`, () => runTest(init, `(x-2)^2`, `x^2-4x+4`))

  it(`With ax`, () => runTest(init, `(2x+3)(x-2)`, `2x^2-x-6`))

  it(`Null coef`, () => runTest(init, `(x-2)(x+2)`, `x^2-4`))

  it(`Bigger degree`, () =>
    runTest(init, `(4x^3-5x^2+7x-1)(4x^2-x+1)`, `16x^5-24x^4+37x^3-16x^2+8x-1`))

  it(`Cyclotomic polynomial`, () => runTest(init, `(x-1)(x+1)(x^2+1)`, `x^4-1`))

  it(`Monomial with no integer`, () => runTest(init, `(2x^2+x-3)(x^2-x-2)`, `2x^4-x^3-8x^2+x+6`))
})
