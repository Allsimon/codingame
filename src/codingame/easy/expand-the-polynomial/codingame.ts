export const init = () => {
  const parenthesisParser = /\(.+?\)/g
  const expoParser = /\(.+?\)\^\d+/g
  const expo = /\^\d+$/g

  const poly = readline()

  const parseNumber = (value: string) => {
    const s = value.replace('+', '').replace('x', '')
    return s === '' ? 1 : +s
  }

  const parseExp = (value: string): [number, number] => {
    expo.exec('')
    const expString = expo.exec(value)
    let exp = 0
    let val
    if (expString) {
      exp = +expString[0].replace('^', '')
      val = parseNumber(value.replace(`^${exp}`, ''))
    } else {
      exp = value.indexOf('x') !== -1 ? 1 : 0
      if (value == '+x') {
        val = 1
      } else if (value == '-x') {
        val = -1
      } else {
        val = parseNumber(value)
      }
    }
    return [val, exp]
  }

  const rewriteExponential = (value: string): string => {
    const regExpExecArray = expoParser.exec(value)
    if (!regExpExecArray) return value
    const found = regExpExecArray[0]
    const expString = expo.exec(found)
    if (!expString) return value
    const exp = +expString[0].replace('^', '')
    const repeated = found.replace(expString[0], '').repeat(exp)
    return rewriteExponential(value.replace(found, repeated))
  }

  const normalizeMult = (value: string): string => value.replace(')*(', ')(')

  const parsePoly = (value: string) => {
    const polyRegex = /([-+]\d*x?(?:\^\d)?)/g
    // a `+` or `-` is needed for the previous regex to work
    const preprocessed = value.charAt(0) === '-' ? value : '+' + value
    let found: RegExpExecArray | null
    const output = new Array<number>()
    while ((found = polyRegex.exec(preprocessed)) !== null) {
      const foundElement = found[0]
      const [lambda, exp] = parseExp(foundElement)
      output[exp] = lambda
    }
    return fillArray(output)
  }

  const fillArray = (array: number[]): number[] => {
    for (let i = 0; i < array.length; i++) {
      if (isNaN(+array[i])) {
        array[i] = 0
      }
    }
    return array
  }

  const p = (value: any) => (isNaN(+value) ? 0 : +value)

  const multiplyPoly = (p1: number[], p2: number[]): number[] => {
    const maxExp = 2 * Math.max(p1.length, p2.length)
    const output = Array(maxExp).fill(0)
    for (let i = 0; i <= maxExp; i++) {
      for (let j = 0; j <= i; j++) {
        for (let k = 0; k <= j; k++) {
          if (j + k === i && p1[j] != undefined && p2[k] != undefined) {
            output[i] = output[i] + (p(p1[j]) * p(p2[k]) + (j === k ? 0 : p(p1[k]) * p(p2[j])))
          }
        }
      }
    }
    return fillArray(output)
  }

  const compute = (value: string): string => {
    let found
    const polynoms: number[][] = []
    while ((found = parenthesisParser.exec(value)) !== null) {
      const poly = found[0].replace('(', '').replace(')', '')
      polynoms.push(parsePoly(poly))
    }
    while (polynoms.length > 1) {
      polynoms.push(multiplyPoly(polynoms[0], polynoms[1]))
      polynoms.shift()
      polynoms.shift()
    }
    let output = ''
    const p = polynoms[0]
    if (p) {
      for (let i = p.length - 1; i >= 0; i--) {
        if (p[i] === 0) continue
        let lambda: any = p[i]
        const exp = i === 0 ? '' : i === 1 ? 'x' : `x^${i}`
        if (lambda == 1) {
          lambda = exp == '' ? '+1' : '+'
        } else if (lambda == -1) {
          lambda = exp == '' ? -1 : '-'
        } else if (lambda > 0) {
          lambda = '+' + lambda
        }
        output += lambda + exp
      }
    }
    if (output.charAt(0)) output = output.slice(1)
    return output
  }

  print(compute(rewriteExponential(normalizeMult(poly))))
}
