export const init = () => {
  const ROUNDS = parseInt(readline())
  let CASH = parseInt(readline())
  for (let i = 0; i < ROUNDS; i++) {
    const PLAY = readline().split(' ')
    let currentBet = Math.ceil(CASH / 4)
    let win = false
    if (PLAY[1] === 'EVEN' && +PLAY[0] % 2 === 0 && +PLAY[0] !== 0) win = true
    if (PLAY[1] === 'ODD' && +PLAY[0] % 2 === 1) win = true
    if (PLAY[1] === 'PLAIN' && +PLAY[0] === +PLAY[2]) {
      win = true
      currentBet *= 35
    }
    CASH += (win ? 1 : -1) * currentBet
  }
  print(CASH)
}
