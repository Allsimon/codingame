// TableOfContents

interface Level {
  lvl: number
  index: number
  title: string
  page: number
  subLevels: Level[]
}

function parseLevel(line: string): Level {
  const [title, page] = line.split(' ')
  for (let i = 0; i < line.split('').length; i++) {
    if (line[i] !== '>') {
      return { lvl: i, index: 0, title: title.replace(/>+/, ''), page: +page, subLevels: [] }
    }
  }
  throw new Error("Can't parse level: " + line)
}

function goToLvl(lvl: Level, depth: number): Level {
  if (depth === lvl.lvl) {
    return lvl
  } else {
    const lastSubLvl = lvl.subLevels[lvl.subLevels.length - 1]
    return goToLvl(lastSubLvl, depth)
  }
}

function reduceLevel(lvls: Level[]): Level {
  const base: Level = {
    lvl: -1,
    index: 0,
    title: 'root',
    page: 0,
    subLevels: [] as Level[],
  }

  lvls.forEach((lvl) => {
    const parent = goToLvl(base, lvl.lvl - 1)
    parent.subLevels.push(lvl)
    lvl.index = parent.subLevels.length
  })
  return base
}

function printLevel(l: Level, lengthOfLine: number) {
  const leftPart = '    '.repeat(l.lvl) + l.index + ' ' + l.title
  const rightPart = '' + l.page

  const padding = '.'.repeat(lengthOfLine - leftPart.length - rightPart.length)
  print(leftPart + padding + rightPart)
  l.subLevels.forEach((sub) => printLevel(sub, lengthOfLine))
}

export const init = () => {
  const lengthOfLine = +readline()
  const N = +readline()
  const level = [...Array(N)].map(() => readline()).map(parseLevel)
  const foo = reduceLevel(level)

  foo.subLevels.forEach((lvl) => printLevel(lvl, lengthOfLine))
}
