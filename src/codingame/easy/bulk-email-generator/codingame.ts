namespace BulkEmailGenerator {
  interface Replacement {
    original: string
    replacement: string
  }

  const N = parseInt(readline())
  let text = ''
  for (let i = 0; i < N; i++) {
    text += readline() + '\n'
  }
  const regex = /\([\s\S]+?\)/g

  let found: string | undefined

  const replacements: Replacement[] = []

  let index = 0
  do {
    const regexArray = regex.exec(text)
    found = regexArray == null ? undefined : regexArray[0]
    if (!!found) {
      const choices = found.substr(1, found.length - 2).split('|')
      const replacement = choices[index++ % choices.length]

      replacements.push({ original: found, replacement: replacement })
    }
  } while (found !== undefined)

  let output = text
  replacements.forEach((r) => (output = output.replace(r.original, r.replacement)))

  print(output)
}
