import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/credit-card-verifier-luhns-algorithm/codingame'

describe('CreditCardVerifierLuhnsAlgorithm', () => {
  it('Tests', () =>
    runTest(
      init,
      `2
4556 7375 8689 9855
4024 0071 0902 2143`,
      `YES
NO`,
    ))

  it('MasterCard', () =>
    runTest(
      init,
      `3
5143 5635 7879 4533
2221 0047 4685 5642
2221 0086 7467 3005`,
      `YES
NO
NO`,
    ))

  it('Discover', () =>
    runTest(
      init,
      `4
6011 4504 4601 6538
6011 3533 1952 3079
6011 6137 9062 4575
6011 3208 9899 2827`,
      `YES
YES
YES
YES`,
    ))

  it('VISA', () =>
    runTest(
      init,
      `4
4916 9040 0624 3561
4532 0616 6103 7334
4485 9117 2097 0646
4916 8075 8881 2287`,
      `NO
YES
NO
NO`,
    ))
})
