export const init = () => {
  const n = parseInt(readline())
  if (!n) return
  const luhnCheck = (card: string) => {
    const numbers = Array.from(card)
      .map((a) => +a)
      .reverse()
    const oddSum = numbers.reduce((a, c, i) => a + (i % 2 == 0 ? c : 0))

    const evenSum = numbers.reduce(
      (a, c, i) => a + (i % 2 == 0 ? 0 : 2 * c < 10 ? 2 * c : 2 * c - 9),
      0,
    )

    return (oddSum + evenSum) % 10 === 0
  }

  Array(n)
    .fill(0)
    .map(() => readline().replace(/\s+/g, ''))
    .map((c) => (luhnCheck(c) ? 'YES' : 'NO'))
    .map((r) => print(r))
}
