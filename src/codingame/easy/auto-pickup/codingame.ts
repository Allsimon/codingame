// AutoPickup

function parse(bits: number[], outputs: string[] = []): string[] {
  const drop = bits[0] === 1 && bits[1] === 0 && bits[2] === 1

  const packetLength = bits.slice(3, 7)
  const length = [...packetLength]
    .reverse()
    .reduce((prev, current, index) => prev + current * Math.pow(2, index))
  const id = bits.slice(7, 7 + length).join('')

  if (drop) {
    outputs.push('001' + packetLength.join('') + id)
  }
  if (bits.length !== 7 + length) {
    parse(bits.slice(7 + length), outputs)
  }

  return outputs
}

export const init = () => {
  const N = +readline()
  const packet = readline()
  const bits = packet.split('').map(Number)
  print(parse(bits).join(''))
}

interface Packet {
  length: string
  id: string
}
