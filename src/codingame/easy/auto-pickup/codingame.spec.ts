import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/auto-pickup/codingame'

describe('AutoPickup', () => {
  it(`just a item drop`, () =>
    runTest(
      init,
      `9
101001010`,
      `001001010`,
    ))

  it(`more than 1 drop`, () =>
    runTest(
      init,
      `17
10100010101001011`,
      `00100010001001011`,
    ))

  it(`other packets`, () =>
    runTest(
      init,
      `17
00100101110100011`,
      `00100011`,
    ))

  it(`In between`, () =>
    runTest(
      init,
      `27
111000111010010110010011101`,
      `001001011`,
    ))

  it(`that is a long one`, () =>
    runTest(
      init,
      `63
100100111100001001001011001010100111011110011110101100011100011`,
      `0010011101001100011100011`,
    ))
})
