import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/brackets-extreme-edition/codingame'

describe('BracketsExtremeEdition', () => {
  it(`Exemple`, () => runTest(init, `{([]){}()}`, `true`))

  it(`{([{S}]]6K[()]}`, () => runTest(init, `{([{S}]]6K[()]}`, `false`))

  it(`{C{}[{[a]}RqhL]{y2}}`, () => runTest(init, `{C{}[{[a]}RqhL]{y2}}`, `true`))

  it(`W12{}{}L{}`, () => runTest(init, `W12{}{}L{}`, `true`))

  it(`h{Pn{GT{h}(c))}`, () => runTest(init, `h{Pn{GT{h}(c))}`, `false`))

  it(`{[{iHTSc}]}p(R)m(){q({})`, () => runTest(init, `{[{iHTSc}]}p(R)m(){q({})`, `false`))

  it(`][`, () => runTest(init, `][`, `false`))
})
