export const init = () => {
  const expression = readline()
  if (!expression) return

  const match = { ')': '(', '}': '{', ']': '[' }

  const bracketsFound = []
  let valid = true

  for (let i = 0; i < expression.length; i++) {
    const letter = expression.charAt(i)
    switch (letter) {
      case '(':
      case '{':
      case '[':
        bracketsFound.push(letter)
        break
      case ')':
      case '}':
      case ']':
        if (match[letter] !== bracketsFound[bracketsFound.length - 1]) {
          valid = false
        } else bracketsFound.pop()
    }
  }

  print(valid && bracketsFound.length === 0)
}
