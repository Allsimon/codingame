import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/whats-so-complex-about-mandelbrot/codingame'

describe('WhatsSoComplexAboutMandelbrot', () => {
  it(`Real out`, () =>
    runTest(
      init,
      `4.5+0i
10`,
      `1`,
    ))

  it(`Imaginary out`, () =>
    runTest(
      init,
      `0+4.2i
100`,
      `1`,
    ))

  it(`Real in`, () =>
    runTest(
      init,
      `-1.1+0i
45`,
      `45`,
    ))

  it(`Imaginary in`, () =>
    runTest(
      init,
      `0+0.2i
11`,
      `11`,
    ))

  it(`Complex out`, () =>
    runTest(
      init,
      `-0.65812-0.452i
275`,
      `124`,
    ))

  it(`Complex in`, () =>
    runTest(
      init,
      `0.15658-0.5745i
822`,
      `822`,
    ))

  it(`Check your absolute value`, () =>
    runTest(
      init,
      `0.465+0.354i
50`,
      `12`,
    ))
})
