import { euclideanDistance } from '@lib/geometry/distance'

export const init = () => {
  const input = readline()
  const m = parseInt(readline())
  if (!input) return

  const regExpExecArray = /(\+?\-?\d*\.?\d+i)/.exec(input)
  const complexPart = !!regExpExecArray ? regExpExecArray[0] : '0i'
  const complex = +complexPart.replace('i', '').replace('+', '')
  const natural = +input.replace(complexPart, '')

  const mandel = (value: number[]): number[] => {
    const [a, b] = value
    return [a * a - b * b + natural, 2 * a * b + complex]
  }

  let value = [natural, complex]

  let i = 1
  while (i < m && euclideanDistance(value, [0, 0]) <= 2) {
    value = mandel(value)
    i++
  }
  print(i)
}
