export const init = () => {
  const N = parseInt(readline())

  const TRY_VALUE = 5
  const DROP = 3
  const KICK = 2

  const countIn = (total: number, value: number) => (total - (total % value)) / value

  const maxTry = countIn(N, TRY_VALUE)

  const outputs = new Array<[number, number, number]>()

  for (let i = 0; i <= maxTry; i++) {
    const currentPoint = i * TRY_VALUE

    const maxDrop = countIn(N - currentPoint, DROP)
    for (let j = 0; j <= maxDrop; j++) {
      const dropPoint = currentPoint + j * DROP

      const kick = countIn(N - dropPoint, KICK)
      const kickPoint = dropPoint + kick * KICK
      if (kickPoint === N && kick <= i) {
        outputs.push([i, kick, j])
      }
    }
  }

  outputs
    .sort((a, b) => {
      const a0 = a[0] - b[0]
      const a1 = a[1] - b[1]
      const a2 = a[2] - b[2]
      if (a0 !== 0) return a0
      if (a1 !== 0) return a1
      return a2
    })
    .map((a) => `${a[0]} ${a[1]} ${a[2]}`)
    .forEach((a) => print(a))
}
