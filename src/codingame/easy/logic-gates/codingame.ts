import { foldLeft } from '@lib/array/arrays'
import { Mapp } from '@lib/type/type'

function parse(lane: string, i: number): number {
  return lane[i] === '_' ? 0 : 1
}
function parseBool(lane: string, i: number): boolean {
  return lane[i] === '-'
}
// LogicGates
export const init = () => {
  const N = +readline()
  const M = +readline()
  const inputSignal = [...Array(N)].map(() => readline().split(' '))
  const outputSignal = [...Array(M)].map(() => readline().split(' '))

  const input = foldLeft(inputSignal, {} as Mapp<string>, (curr, [key, val]) => {
    curr[key] = val
    return curr
  })

  outputSignal.map(([lane, op, lane1, lane2]) => {
    const output = []
    const l1 = input[lane1]
    const l2 = input[lane2]
    for (let i = 0; i < l1.length; i++) {
      if (op === 'AND') {
        output.push(+(parseBool(l1, i) && parseBool(l2, i)))
      } else if (op === 'OR') {
        output.push(+(parseBool(l1, i) || parseBool(l2, i)))
      } else if (op === 'XOR') {
        output.push(+(parse(l1, i) ^ parse(l2, i)))
      } else if (op === 'NAND') {
        output.push(+!(parseBool(l1, i) && parseBool(l2, i)))
      } else if (op === 'NOR') {
        output.push(+!(parseBool(l1, i) || parseBool(l2, i)))
      } else if (op === 'NXOR') {
        output.push(+!(parse(l1, i) ^ parse(l2, i)))
      }
    }
    const s = output.map((i) => (i === 0 ? '_' : '-')).join('')
    print(lane + ' ' + s)
  })
}
