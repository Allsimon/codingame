namespace OrderOfSuccession {
  interface People {
    childrens: People[]
    name: string
    parent: string
    birth: number
    death: string
    religion: string
    gender: string
  }

  const n = parseInt(readline())

  const peoples: People[] = []
  for (let i = 0; i < n; i++) {
    const inputs = readline().split(' ')
    peoples.push({
      childrens: [],
      name: inputs[0],
      parent: inputs[1],
      birth: parseInt(inputs[2]),
      death: inputs[3],
      religion: inputs[4],
      gender: inputs[5],
    })
  }

  const compare = (a: People, b: People): number => {
    if (a.gender !== b.gender) {
      return a.gender === 'F' ? 1 : -1
    }
    return a.birth - b.birth
  }

  const tree: { [name: string]: People } = {}
  peoples.forEach((p) => (tree[p.name] = p))
  peoples.filter((p) => p.parent !== '-').forEach((p) => tree[p.parent].childrens.push(p))

  const printTree = (peoples: People[]): void => {
    peoples.sort(compare).forEach((p) => {
      if (p.death === '-' && p.religion !== 'Catholic') {
        print(p.name)
      }
      printTree(p.childrens)
    })
  }

  printTree([peoples[0]])
}
