// WordSearchForProgrammers
import {
  cloneMatrix,
  Direction,
  isInMatrix,
  nextPosition,
  Position,
  positionAreEquals,
  replaceInMatrix,
  valueOf,
} from '@lib/matrix/matrix'

function recursivelyFindWords(
  lines: string[][],
  currentPosition: Position,
  direction: Direction,
  possibleClues: string[],
  found: Position[],
): Position[] {
  // if the current position is outside the matrix, then we didn't find the word
  if (!isInMatrix(lines, currentPosition)) {
    return []
  }
  found.push(currentPosition)
  // find all clues containing
  const clues = possibleClues.filter((clue) =>
    found.every((previousPosition) => clue.includes(valueOf(lines, previousPosition))),
  )
  if (clues.length === 0) {
    return []
  }
  // if a word has been found, return it
  for (let clue of clues) {
    const wordFound = found.map((f) => valueOf(lines, f)).join('')
    if (clue === wordFound) {
      return found
    }
  }
  // otherwise keep searching
  return recursivelyFindWords(
    lines,
    nextPosition(currentPosition, direction),
    direction,
    clues,
    found,
  )
}

function findWordsInEveryDirection(
  lines: string[][],
  currentPosition: Position,
  possibleClues: string[],
  found: Position[],
): Position[] {
  return [Direction.NorthEast, Direction.East, Direction.SouthEast, Direction.South]
    .map((direction) => recursivelyFindWords(lines, currentPosition, direction, possibleClues, []))
    .flatMap((found) => found)
}

function reverseString(str: string): string {
  return str.split('').reverse().join('')
}

export const init = () => {
  const N = +readline()
  const lines = [...Array(N)].map(() => readline().split(''))
  const clues = readline()
    .split(' ')
    .map((s) => s.toUpperCase())

  clues.map(reverseString).forEach((clue) => clues.push(clue))

  const allLettersFound: Position[] = []
  for (let i = 0; i < lines.length; i++) {
    for (let j = 0; j < lines[i].length; j++) {
      const found = findWordsInEveryDirection(lines, { x: i, y: j }, clues, [])
      allLettersFound.push(...found)
    }
  }
  const result = cloneMatrix(lines)
  replaceInMatrix(
    result,
    (t) => true,
    (x: number, y: number) => {
      const position = { x, y }

      return allLettersFound.some((found) => positionAreEquals(found, position))
        ? valueOf(result, position)
        : ' '
    },
  )
  result.forEach((line) => print(line.join('')))
}
