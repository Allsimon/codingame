// MovesInMaze
import {
  cloneAndReplaceInMatrix,
  doOnAdjacentComplete,
  findInMatrix,
  MatrixNavigationParameter,
  Position,
  printMatrix,
} from '@lib/matrix/matrix'

function convertMatrixStringToNumber(matrix: string[][]): number[][] {
  return cloneAndReplaceInMatrix(matrix, (value) => {
    if (value === '#') {
      return -1 // wall
    } else if (value === 'S') {
      return 0 // starting point
    } else if (value === '.') {
      return Number.MAX_SAFE_INTEGER // the puzzle states that the max number of position is 35
    }
    throw new Error("Can't parse map")
  })
}

function convertMatrixNumberToString(matrix: number[][]): string[][] {
  return cloneAndReplaceInMatrix(matrix, (value) => {
    if (value === -1) {
      return '#' // wall
    } else if (value <= 9) {
      return '' + value
    }
    if (value > 9 && value <= 36) {
      // convert the distance to alphabetic => 10 -> A, 11 -> B
      // String.fromCharCode(65) === A
      return String.fromCharCode(55 + value)
    } else if (value === Number.MAX_SAFE_INTEGER) {
      return '.'
    }
    throw new Error("Can't write matrix: " + value)
  })
}

export const init = () => {
  const navParam: MatrixNavigationParameter = {
    considerDiagonal: false,
    periodic: true,
  }
  const [w, h] = readline().split(' ').map(Number)
  const lines = [...Array(h)].map(() => readline().split(''))

  const distances = convertMatrixStringToNumber(lines)

  const [startingI, startingJ] = findInMatrix(distances, 0)
  const checkDistance = (minDistance: number, position: Position) => {
    const currentDistance = distances[position.x][position.y]
    if (currentDistance === -1) {
      // wall
      return
    } else if (minDistance < currentDistance) {
      distances[position.x][position.y] = minDistance
      doOnAdjacentComplete(distances, position, navParam, (p) => checkDistance(minDistance + 1, p))
    }
  }

  doOnAdjacentComplete(
    distances,
    {
      x: startingI,
      y: startingJ,
    },
    navParam,
    (p) => checkDistance(1, p),
  )

  const result = convertMatrixNumberToString(distances)

  const stringResult = printMatrix(result, (t) => t).trimEnd()
  print(stringResult)
}
