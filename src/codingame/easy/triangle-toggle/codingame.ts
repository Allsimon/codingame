// TriangleToggle
import { ShimratCrossing } from '@lib/geometry/point-in-polygon/shimrat-crossing'
import { foldLeft } from '@lib/array/arrays'

function toggle(char: string) {
  return char === '*' ? ' ' : '*'
}

export const init = () => {
  const [HI, WI] = readline().split(' ').map(Number)
  const style = readline() as 'condensed' | 'expanded'
  const howManyTriangles = +readline()
  const triangles = [...Array(howManyTriangles)].map(() => readline().split(' ').map(Number))

  const matrix = [...Array(HI)].map(() => [...Array(WI)].map(() => '*'))

  const conditions = triangles.map(([x1, y1, x2, y2, x3, y3]) => {
    return (i: number, j: number) =>
      new ShimratCrossing().contains(
        [i, j],
        [
          [y1, x1],
          [y2, x2],
          [y3, x3],
        ],
      )
  })

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      matrix[i][j] = foldLeft(
        conditions.filter((condition) => condition(i, j)),
        matrix[i][j],
        toggle,
      )
    }
  }
  const result = matrix
    .map((line) => line.map((char) => char).join(style == 'condensed' ? '' : ' '))
    .join('\n')
  print(result)
}
