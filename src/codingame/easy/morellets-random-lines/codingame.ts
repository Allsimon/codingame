export const init = () => {
  const [xA, yA, xB, yB] = readline().split(' ').map(Number)
  const n = +readline()

  const minify = (row: number[]) => row.map((l) => l / Math.min(...row))
  const sameSign = (row: number[]) => row.map((l) => (row[0] < 0 ? -1 : 1) * l)

  const unique = (table: number[][]) =>
    [...new Set(table.map((row) => row.toString()))].map((r) => r.split(',').map(Number))

  const lines = unique(
    Array(n)
      .fill(0)
      .map(() => readline().split(' ').map(Number))
      .map(minify)
      .map(sameSign),
  )

  const checkIsOnALine = (row: number[]) => {
    const [a, b, c] = row
    return xA * a + yA * b + c === 0 || xB * a + yB * b + c === 0
  }

  const isOnALine = lines.filter(checkIsOnALine).length !== 0

  const changeColor = (row: number[]) => {
    const [a, b, c] = row
    const l1 = xA * a + yA * b + c
    const l2 = xB * a + yB * b + c

    return (l1 < 0 && l2 > 0) || (l1 > 0 && l2 < 0)
  }

  if (isOnALine) {
    print('ON A LINE')
  } else {
    let colorChange = 0
    for (let i = 0; i < lines.length; i++) {
      colorChange += changeColor(lines[i]) ? 1 : 0
    }
    print(colorChange % 2 === 0 ? 'YES' : 'NO')
  }
}
