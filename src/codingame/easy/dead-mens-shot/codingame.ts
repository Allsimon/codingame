import { Point } from '@lib/geometry/point'
import { ShimratCrossing } from '@lib/geometry/point-in-polygon/shimrat-crossing'

namespace DeadMensShot {
  // @see https://www.codingame.com/ide/puzzle/dead-mens-shot

  const N = parseInt(readline())

  const polygon: Point[] = []
  for (let i = 0; i < N; i++) {
    var inputs = readline().split(' ')
    const x = parseInt(inputs[0])
    const y = parseInt(inputs[1])
    polygon.push([x, y])
  }
  const pointInPolyAlgo = new ShimratCrossing()

  const M = parseInt(readline())
  for (let i = 0; i < M; i++) {
    var inputs = readline().split(' ')
    const x = parseInt(inputs[0])
    const y = parseInt(inputs[1])
    print(pointInPolyAlgo.contains([x, y], polygon) ? 'hit' : 'miss')
  }
}
