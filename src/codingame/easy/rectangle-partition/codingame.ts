interface NumMap {
  [index: number]: number
}

function add(val: number, obj: NumMap) {
  if (obj[val] === undefined) {
    obj[val] = 1
  } else {
    obj[val] = obj[val] + 1
  }
}

function findLengths(countXs: number[]): NumMap {
  const result = {}

  countXs.forEach((value, i) => {
    add(value, result)
    for (let j = i + 1; j < countXs.length; j++) {
      add(countXs[j] - value, result)
    }
  })

  return result
}

// RectanglePartition
export const init = () => {
  const [w, h, countX, countY] = readline().split(' ').map(Number)
  const countXs = readline().split(' ').map(Number)
  const countYs = readline().split(' ').map(Number)

  countXs.push(w)
  countYs.push(h)
  const xs = findLengths(countXs)
  const ys = findLengths(countYs)

  let squares = 0
  for (const [delta, instances] of Object.entries(xs)) {
    if (ys[+delta]) {
      squares += instances * ys[+delta]
    }
  }
  print(squares)
}
