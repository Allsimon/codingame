import { runTest } from '@codingame/test'
import { init } from '@codingame/easy/tictactoe/codingame'

describe('Tictactoe', () => {
  it(`No opponent - line`, () =>
    runTest(
      init,
      `OO.
...
...`,
      `OOO
...
...`,
    ))

  it(`No opponent - column`, () =>
    runTest(
      init,
      `O..
...
O..`,
      `O..
O..
O..`,
    ))

  it(`No opponent - diagonal`, () =>
    runTest(
      init,
      `O..
.O.
...`,
      `O..
.O.
..O`,
    ))

  it(`Real condition`, () =>
    runTest(
      init,
      `OXX
XO.
O..`,
      `OXX
XO.
O.O`,
    ))

  it(`No win condition`, () =>
    runTest(
      init,
      `...
...
...`,
      `false`,
    ))
})
