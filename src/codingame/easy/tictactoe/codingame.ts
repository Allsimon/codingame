// Tictactoe

import { sameArray } from '@lib/array/arrays'
import {
  cloneMatrix,
  diagonals,
  isEqual,
  rotateClockwise,
  rotateCounterClockwise,
  row,
} from '@lib/matrix/matrix'

function rewrite(board: string[][]): string[][] {
  const clonedBoard = cloneMatrix(board)
  const diags = diagonals(clonedBoard)
  if (isWinnable(diags[0])) {
    clonedBoard[0][0] = 'O'
    clonedBoard[1][1] = 'O'
    clonedBoard[2][2] = 'O'
    return clonedBoard
  } else if (isWinnable(diags[1])) {
    clonedBoard[2][0] = 'O'
    clonedBoard[1][1] = 'O'
    clonedBoard[0][2] = 'O'
    return clonedBoard
  }

  const rotated = rotateClockwise(clonedBoard)

  for (let i = 0; i < 3; i++) {
    if (isWinnable(row(clonedBoard, i))) {
      writeMove(row(clonedBoard, i))
      return clonedBoard
    } else if (isWinnable(row(rotated, i))) {
      writeMove(row(rotated, i))
      return rotateCounterClockwise(rotated)
    }
  }
  return board
}

function writeMove(val: string[]) {
  for (let i = 0; i < val.length; i++) {
    val[i] = 'O'
  }
}

function isWinnable(val: string[]): boolean {
  return sameArray(val.sort(), ['.', 'O', 'O'])
}

export const init = () => {
  const lines = [...Array(3)].map(() => readline().split(''))

  const rewritten = rewrite(lines)
  if (isEqual(lines, rewritten)) {
    print('false')
  } else {
    rewritten.map((line) => line.join('')).forEach((line) => print(line))
  }
}
