import { CodingameAPI } from 'codingame/offline/api/model'
import got from 'got'
import fs from 'fs'
import path from 'path'
import input from './startTestSession.json'

// @ts-ignore
const currentFile = path.dirname(require.main.filename)
// poor man's directory finder: we search for the name of the git repository 'codingame' then 'src/codingame'
const codingGameFolder = '/codingame/src/codingame'
const appDir = currentFile.substring(
  0,
  currentFile.indexOf(codingGameFolder) + codingGameFolder.length,
)

// @ts-ignore
if (Object.keys(input).length === 0) {
  throw new Error(
    `Copy paste in the 'startTestSession.json' file the JSON response from the POST to startTestSession`,
  )
}

interface Game {
  tests: Test[]
  difficulty: string
  name: string
}

interface Test {
  index: number
  inputBinaryId: number
  input: string | undefined
  outputBinaryId: number
  output: string | undefined
  label: string
}

const fetchData = (output: CodingameAPI.SessionOutput) => {
  const testCases = output.currentQuestion.question.testCases

  // looks like `/training/easy/balanced-ternary-computer-encode`
  const details = output.puzzle.detailsPageUrl.split('/')

  testCases.map((l) => l.inputBinaryId).map((l) => dataUrl(l))

  const game: Game = {
    difficulty: details[2],
    name: details[3],
    tests: [],
  }

  for (let i = 0; i < testCases.length; i++) {
    game.tests.push(Object.assign({}, testCases[i]) as Test)

    got(dataUrl(testCases[i].inputBinaryId))
      .then((res: any) => (game.tests[i].input = res.body))
      .then(() => callbackFinished(game))

    got(dataUrl(testCases[i].outputBinaryId))
      .then((res: any) => (game.tests[i].output = res.body))
      .then(() => callbackFinished(game))
  }
  // callbackFinished(game);
}

const callbackFinished = (game: Game) => {
  if (game.tests.filter((l) => l.input == null || l.output == null).length === 0) {
    const nameCamel = convertCamelCase(game.name)
    const path = `${appDir}/${game.difficulty}/${game.name}`
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path)
    }

    // Game file
    fs.writeFileSync(
      path + '/codingame.ts',
      `// ${nameCamel}
export const init = () => {
  const N = +readline();
  const lines = [...Array(N)].map(() => readline().split(' ').map(Number));

};
`,
    )

    // Unit test file

    const sanitize = (input?: string) =>
      !input
        ? ''
        : input
            .replace(/\\/g, String.fromCharCode(92, 92))
            .replace(/`/g, String.fromCharCode(92, 96))

    const testsToString = game.tests
      .map(
        (t) =>
          `  it(\`${t.label}\`, () => runTest(init, \`${sanitize(t.input)}\`, \`${sanitize(t.output)}\`));`,
      )
      .join('\n\n')

    fs.writeFileSync(
      path + '/codingame.spec.ts',
      `import { runTest } from '@codingame/test';
import { init } from '@codingame/${game.difficulty}/${game.name}/codingame';

describe("${nameCamel}", () => {
${testsToString}
});
`,
    )

    console.log(game.difficulty)
    console.log(game.name)
    console.log(game.tests)
  }
}

const upperCaseFirstLetter = (input: string) => input.charAt(0).toUpperCase() + input.slice(1)
const convertCamelCase = (input: string) =>
  upperCaseFirstLetter(input.replace(/-([a-z])/g, (g) => g[1].toUpperCase()))
    .replace('-', '')
    .replace(`'`, '')
    .replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, '')

const dataUrl = (id: number) => `https://static.codingame.com/servlet/fileservlet?id=${id}`

fetchData(input)
