export namespace CodingameAPI {
  export interface SessionOutput {
    puzzle: Puzzle
    testType: string
    currentQuestion: CurrentQuestion
    direct: boolean
    questions: QuestionElement[]
    testSessionId: number
    testSessionHandle: string
    needAccount: boolean
    shareable: boolean
    userId: number
    showReplayPrompt: boolean
  }

  export interface CurrentQuestion {
    question: CurrentQuestionQuestion
    answer: Answer
  }

  export interface Answer {}

  export interface CurrentQuestionQuestion {
    testCases: TestCase[]
    languages: string[]
    availableLanguages: AvailableLanguage[]
    stubGenerator: string
    id: number
    initialId: number
    type: string
    statement: string
    duration: number
    userId: number
    contribution: Contribution
    contributor: Tor
    index: number
    title: string
  }

  export interface AvailableLanguage {
    id: string
    name: string
  }

  export interface Contribution {
    moderators: Tor[]
    id: number
    publicHandle: string
    type: string
    status: string
  }

  export interface Tor {
    userId: number
    pseudo: string
    publicHandle: string
    enable: boolean
    avatar: number
    cover?: number
  }

  export interface TestCase {
    index: number
    inputBinaryId: number
    outputBinaryId: number
    label: string
  }

  export interface Puzzle {
    forumPostId: string
    title: string
    id: number
    hints: any[]
    level: string
    handle: string
    prettyId: string
    detailsPageUrl: string
  }

  export interface QuestionElement {
    questionId: number
    title: string
    hasResult: boolean
  }
}
