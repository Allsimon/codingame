import { fstat, readdirSync, readFileSync, writeFileSync } from 'fs'
import { join } from 'path'

function walkDirectory(dir: string): string[] {
  return (
    readdirSync(dir, { withFileTypes: true })
      // @ts-ignore
      .flatMap((file) =>
        file.isDirectory() ? walkDirectory(join(dir, file.name)) : join(dir, file.name),
      )
  )
}

const distDirectory = process.env.PWD + '/dist'

const jsFiles = walkDirectory(distDirectory).filter((file) => file.endsWith('.js'))

jsFiles.forEach((file) => {
  const value = readFileSync(file, { encoding: 'utf8' })

  const newVal = value
    .replace('var init = () => {', '')
    .replace('"use strict";\n(() => {', '')
    .replace('  };\n})();\n', '')
    .split('\n')
    .map((lines) => (lines.startsWith('  ') ? lines.substring(2) : lines))
    // remove eslint comment
    .filter((lines) => !(lines.startsWith('//') && lines.endsWith('.ts')))
    .join('\n')

  writeFileSync(file, newVal)
})
// console.log(jsFiles)
