export let inputs: string[] = []
export let outputs: string[] = []
export let testNotLaunched: boolean = true

function _readline() {
  return testNotLaunched ? '' : inputs.shift()
}

function _print(object: any) {
  if (!testNotLaunched) {
    if (!('' + object).includes('\n')) {
      if (outputs.length) {
        if (outputs[0] == '' + object) {
          outputs.shift()
        } else throw new Error(`Was expecting '${outputs[0]}', got '${object}'`)
      } else throw new Error('No output left')
    } else {
      ;('' + object).split('\n').forEach(print)
    }
  }
}

function _printErr(object: any) {
  if (!testNotLaunched) console.error(object)
}

export function input(input: string) {
  inputs = input.split('\n')
}

export function output(output: string) {
  outputs = output.split('\n')
  if (inputs.length === 0) {
    throw new Error('No Inputs detected')
  }
  testNotLaunched = false
}

export function initializeTest(inputString: string, outputString: string) {
  input(inputString)
  output(outputString)
}

export function runTest(runner: () => void, inputString: string, outputString: string) {
  initializeTest(inputString, outputString)

  runner()

  expect(testIsDone()).toBeTruthy()
}

export function testIsDone() {
  return isNotEmpty(inputs) && isNotEmpty(outputs)
}

function isNotEmpty(arr: string[]) {
  return !arr.length || (arr.length === 1 && arr[0] === '')
}

// @ts-ignore
var window = window || null
const _global = (window /* browser */ || global) /* node */ as any
_global.readline = _readline
_global.print = _print
_global.printErr = _printErr
