import { Dijkstra } from '@lib/graph/dijkstra/dijkstra'

// SkynetRevolutionEpisode1
export const init = () => {
  const [N, L, E] = readline().split(' ').map(Number)

  let graph = new Dijkstra()

  ;[...Array(L)]
    .map(() => readline().split(' '))
    .forEach((e) => {
      const [a, b] = e
      graph.addVertexDistanceOneBiDir(a, b)
    })

  const gateways = [...Array(E)].map(() => readline())

  while (true) {
    const SI = readline() // The index of the node on which the Skynet agent is positioned this turn
    const minDist = gateways
      .map((e) => graph.shortestPath(SI, e))
      .filter((path) => path.length > 1)
      .reduce((prev, current) => (current.length < prev.length ? current : prev))

    graph.removeVertex(minDist[0], minDist[1])
    graph.removeVertex(minDist[1], minDist[0])

    print(`${minDist[0]} ${minDist[1]}`)
  }
}
