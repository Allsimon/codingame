import { Dijkstra } from '@lib/graph/dijkstra/dijkstra'

export const init = () => {
  const [W, H] = readline().split(' ').map(Number)
  if (!W) return
  const [X, Y] = readline().split(' ').map(Number)

  const matrix = [...Array(H)].map(() => readline().split(''))

  const graph = new Dijkstra()
  const name = (i: number, j: number) => j + ' ' + i

  const possibleExits = new Set<string>()
  for (let i = 0; i < W; i++) {
    possibleExits.add(name(i, 0))
    possibleExits.add(name(i, H - 1))
  }
  for (let i = 0; i < H; i++) {
    possibleExits.add(name(0, i))
    possibleExits.add(name(W - 1, i))
  }

  for (let i = 0; i < matrix.length - 1; i++)
    for (let j = 0; j < matrix[i].length; j++)
      if (matrix[i][j] === '.') {
        if (matrix[i][j + 1] === '.') graph.addVertexDistanceOneBiDir(name(i, j), name(i, j + 1))
        if (matrix[i + 1][j] === '.') graph.addVertexDistanceOneBiDir(name(i, j), name(i + 1, j))
      }

  const entrance = name(Y, X)

  const exits = [...possibleExits]
    .filter((exit) => graph.shortestPath(entrance, exit).length >= 2)
    .sort((a, b) => {
      const [xa, ya] = a.split(' ').map(Number)
      const [xb, yb] = b.split(' ').map(Number)

      const diffX = xa - xb
      if (diffX === 0) return ya - yb
      return diffX
    })

  print(exits.length)
  exits.forEach((exit) => print(exit))
}
