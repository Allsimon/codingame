import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/text-alignment/codingame'

describe('TextAlignment', () => {
  it(`Test 1`, () =>
    runTest(
      init,
      `LEFT
4
Never gonna give you up, never gonna let you down
Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
Never gonna tell a lie and hurt you`,
      `Never gonna give you up, never gonna let you down
Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
Never gonna tell a lie and hurt you`,
    ))

  it(`Test 2`, () =>
    runTest(
      init,
      `RIGHT
4
Never gonna give you up, never gonna let you down
Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
Never gonna tell a lie and hurt you`,
      `Never gonna give you up, never gonna let you down
            Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
              Never gonna tell a lie and hurt you`,
    ))

  it(`Test 3`, () =>
    runTest(
      init,
      `CENTER
4
Never gonna give you up, never gonna let you down
Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
Never gonna tell a lie and hurt you`,
      `Never gonna give you up, never gonna let you down
      Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
       Never gonna tell a lie and hurt you`,
    ))

  it(`Test 4`, () =>
    runTest(
      init,
      `JUSTIFY
4
Never gonna give you up, never gonna let you down
Never gonna run around and desert you
Never gonna make you cry, never gonna say goodbye
Never gonna tell a lie and hurt you`,
      `Never gonna give you up, never gonna let you down
Never   gonna   run   around   and   desert   you
Never gonna make you cry, never gonna say goodbye
Never   gonna   tell   a   lie   and   hurt   you`,
    ))
})
