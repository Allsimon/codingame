export const init = () => {
  const alignment = readline()
  const N = +readline()
  if (N === undefined) return
  const lines = [...Array(N)].map(() => readline().trim())

  const maxLength = Math.max(...lines.map((l) => l.length))
  const align = (line: string) => {
    if (alignment === 'LEFT') {
      return line
    } else if (alignment === 'RIGHT') {
      return line.padStart(maxLength, ' ')
    } else if (alignment === 'CENTER') {
      const lengthDelta = maxLength - line.length
      const left = Math.floor(lengthDelta / 2)
      return line.padStart(line.length + left, ' ')
    } else if (alignment === 'JUSTIFY') {
      let lengthDelta = maxLength - line.length
      if (lengthDelta === 0) return line
      const strings = line.split(' ')
      let output = ''
      for (let i = 0; i < strings.length; i++) {
        const space = Math.floor((lengthDelta + strings.length - 1) / (strings.length - 1 - i))
        lengthDelta -= space
        output += strings[i] + ' '.repeat(space)
      }

      return output
    }
  }

  lines.map(align).forEach((l) => print(l))
}
