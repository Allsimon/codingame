import { decimalToBase } from '@lib/number/numbers'

export const init = () => {
  let [start, N] = readline().split(' ').map(Number)
  for (let i = 0; i < N; i++) {
    const hmmm = decimalToBase(start, 2).reduce((prev, curr) => prev + (curr === 0 ? 4 : 3), 0)
    if (hmmm === start) {
      break
    }
    start = hmmm
  }

  print(start)
}
