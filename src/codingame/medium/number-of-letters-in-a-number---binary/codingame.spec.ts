import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/number-of-letters-in-a-number---binary/codingame'

describe('NumberOfLettersInANumberBinary', () => {
  it(`One past the Example`, () =>
    runTest(
      init,
      `5 2`,
      `14
`,
    ))

  it(`Bigger Start`, () => runTest(init, `891 3`, `16`))

  it(`Bigger n`, () => runTest(init, `263 6`, `18`))

  it(`A pattern emerges`, () => runTest(init, `480 16`, `18`))

  it(`Another pattern?`, () => runTest(init, `481 32`, `13`))

  it(`Big Numbers`, () => runTest(init, `2466262232 131212325`, `18`))

  it(`Bigger Numbers`, () => runTest(init, `99999999999999999 99999999999999999`, `18`))
})
