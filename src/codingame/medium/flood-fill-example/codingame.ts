import { Mapp } from '@lib/type/type'
import { adjacentTo, flatten, printMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const W = +readline()
  const H = +readline()

  if (W === undefined) return
  const map: Tile[][] = [...Array(H)].map(() =>
    readline()
      .split('')
      .map((c) => {
        const tile = { type: c, visited: {} as Mapp<number> }
        if (c !== '.' && c !== '#') tile.visited[c] = 0
        return tile
      }),
  )

  for (let i = 0; i < H; i++) {
    for (let j = 0; j < W; j++) {
      const type = map[i][j].type
      if (type !== '.' && type !== '#') {
        map[i][j].type = type + ',' + i + ',' + j
      }
    }
  }

  const isFound = (tile: Tile, type: string) => {
    return tile.type !== type && tile.visited[type] !== undefined
  }

  const toString = (t: Tile) => {
    if (t.type === '#') return '#'
    if (t.type === '.') {
      const keys = Object.keys(t.visited)
      if (keys.length === 0) {
        return '.'
      }
      if (keys.length == 1) {
        return keys[0]
      }
      return '+'
    }
    return t.type.split(',')[0]
  }

  const assignType = () => {
    for (let i = 0; i < H; i++) {
      for (let j = 0; j < W; j++) {
        map[i][j].type = toString(map[i][j])
      }
    }
  }

  let foundSome = true
  while (flatten(map).some((t) => toString(t) === '.') && foundSome) {
    foundSome = false
    for (let i = 0; i < H; i++) {
      for (let j = 0; j < W; j++) {
        const tile = map[i][j]
        if (tile.type !== '.' && tile.type !== '#') {
          const found = adjacentTo(map, i, j)
            .filter((t) => t.type === '.')
            .filter((t) => !isFound(t, tile.type))
          if (found.length !== 0) {
            foundSome = true
            found.forEach((t) => (t.visited[tile.type] = tile.visited[tile.type] + 1))
          }
        }
      }
    }
    assignType()
  }

  print(printMatrix(map, toString).trimRight())
}

interface Tile {
  type: string
  visited: Mapp<number>
}
