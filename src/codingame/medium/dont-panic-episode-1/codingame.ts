import { Mapp } from '@lib/type/type'

export class DontPanicEpisode1 {
  static init() {
    const [
      nbFloors,
      width,
      nbRounds,
      exitFloor,
      exitPos,
      nbTotalClones,
      nbAdditionalElevators,
      nbElevators,
    ] = readline().split(' ').map(Number)

    // [floor, pos]
    const elevators: Mapp<number> = {}
    ;[...Array(nbElevators)]
      .map(() => readline().split(' ').map(Number))
      .forEach((e) => (elevators[e[0]] = e[1]))

    // game loop
    while (true) {
      var inputs = readline().split(' ')
      const cloneFloor = parseInt(inputs[0]) // floor of the leading clone
      const clonePos = parseInt(inputs[1]) // position of the leading clone on its floor
      const direction = inputs[2] // direction of the leading clone: LEFT or RIGHT

      const elevatorPos = elevators[cloneFloor] || exitPos
      if (direction === 'LEFT' && elevatorPos > clonePos) {
        print('BLOCK')
      } else if (direction === 'RIGHT' && elevatorPos < clonePos) {
        print('BLOCK')
      } else {
        print('WAIT')
      }
    }
  }
}

DontPanicEpisode1.init()
