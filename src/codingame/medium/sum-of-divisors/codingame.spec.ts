import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/sum-of-divisors/codingame'

describe('SumOfDivisors', () => {
  it(`Test 1`, () => runTest(init, `2`, `4`))

  it(`Test 2`, () => runTest(init, `10`, `87`))

  it(`Test 3`, () => runTest(init, `1000`, `823081`))

  it(`No brute-force here`, () => runTest(init, `90000`, `6662106690`))
})
