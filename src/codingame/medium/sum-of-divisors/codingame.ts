export const init = () => {
  const N = +readline()

  const sum = [...Array(N)].map((_, k) => k + 1).reduce((res, e) => res - (N % e), N * N)

  print(sum)
}
