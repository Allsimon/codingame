import { Mapp } from '@lib/type/type'

export const init = () => {
  const N = +readline()
  if (!N) return
  // fuck-it
  const adjacency = [
    [2, 4, 5],
    [1, 3, 4, 5, 6],
    [2, 5, 6],
    [1, 2, 5, 7, 8],
    [1, 2, 3, 4, 6, 7, 8, 9],
    [2, 3, 5, 8, 9],
    [4, 5, 8],
    [4, 5, 6, 7, 9],
    [5, 6, 8],
  ]

  const cache: Mapp<boolean> = {}

  const isWinningMove = (N: number, d: number, opponentTurn = false): boolean => {
    const key = `${d},${N},${opponentTurn}`
    if (cache[key] !== undefined) {
      return cache[key]
    }

    const rest = N - d
    if (rest < 0) return opponentTurn
    const adjacents = adjacency[d - 1]

    const predicate = (adjacent: number) => isWinningMove(rest, adjacent, !opponentTurn)

    const output = !opponentTurn ? adjacents.every(predicate) : adjacents.some(predicate)
    cache[key] = output
    return output
  }

  const output = []
  for (let i = 1; i <= 9; i++) {
    if (isWinningMove(N, i)) {
      output.push(i)
    }
  }
  print(output.join(' '))
}
