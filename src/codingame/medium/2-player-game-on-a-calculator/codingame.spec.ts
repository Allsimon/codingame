import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/2-player-game-on-a-calculator/codingame'

describe('2PlayerGameOnACalculator', () => {
  it(`Test 1`, () => runTest(init, `2`, `1 2`))

  it(`Test 2`, () => runTest(init, `9`, `1 7 8 9`))

  it(`Test 3`, () => runTest(init, `35`, `1 3 7`))

  it(`Test 4`, () => runTest(init, `256`, `1 3 7 9`))
})
