import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/simple-fraction-to-mixed-number/codingame'

describe('SimpleFractionToMixedNumber', () => {
  it(`Both integer and fractional parts`, () =>
    runTest(
      init,
      `2
42/9
71/23`,
      `4 2/3
3 2/23`,
    ))

  it(`Integer part only`, () =>
    runTest(
      init,
      `3
6/3
28/4
105/35`,
      `2
7
3`,
    ))

  it(`Fractional part only`, () =>
    runTest(
      init,
      `2
4/6
10/78`,
      `2/3
5/39`,
    ))

  it(`Zero`, () =>
    runTest(
      init,
      `2
0/91
-0/287`,
      `0
0`,
    ))

  it(`Division by zero`, () =>
    runTest(
      init,
      `1
3/0`,
      `DIVISION BY ZERO`,
    ))

  it(`Sign handling`, () =>
    runTest(
      init,
      `2
-5/20
20/-5`,
      `-1/4
-4`,
    ))

  it(`Some more sign handling`, () =>
    runTest(
      init,
      `2
-10/-7
21/-6`,
      `1 3/7
-3 1/2`,
    ))

  it(`Several random cases`, () =>
    runTest(
      init,
      `13
0/0
0/-0
-0/0
2/2
-2/2
0/-287
-0/-287
-6414254/-7344817
7338165/8696070
9784128/9994708
9999999/-9999999
-9999999/-9999999
-9999999/9999999`,
      `DIVISION BY ZERO
DIVISION BY ZERO
DIVISION BY ZERO
1
-1
0
0
6414254/7344817
489211/579738
2446032/2498677
-1
1
-1`,
    ))
})
