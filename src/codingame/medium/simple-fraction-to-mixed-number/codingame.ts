import { commonFactors } from '@lib/number/numbers'

export const init = () => {
  const N = +readline()
  if (N === undefined) return

  const simplify = (x: number, y: number) => {
    const gcf = Math.max(...commonFactors(x, y))
    return x / gcf + '/' + y / gcf
  }

  ;[...Array(N)]
    .map(() => readline().split('/').map(Number))
    .map((value) => {
      let [x, y] = value
      if (y === 0) return 'DIVISION BY ZERO'
      if (x === 0) return 0
      if (Number.isInteger(x / y)) return x / y

      const isNegative = x / y < 0
      x = Math.abs(x)
      y = Math.abs(y)
      const integerPart = Math.floor(x / y)
      if (Math.abs(integerPart) >= 1) {
        return (isNegative ? '-' : '') + integerPart + ' ' + simplify(x - integerPart * y, y)
      } else {
        return (isNegative ? '-' : '') + simplify(x, y)
      }
    })
    .forEach((l) => print(l))
}
