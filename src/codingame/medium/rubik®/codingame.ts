import { sum } from '@lib/array/arrays'

export const init = () => {
  const N = +readline()
  if (!N) return
  if (N === 1) {
    print(1)
  } else {
    const f1 = Math.pow(N, 2)
    const f2 = N * (N - 1)
    const f3 = (N - 1) * (N - 1)
    const f4 = f3
    const f5 = (N - 1) * (N - 2)
    const f6 = (N - 2) * (N - 2)

    print(sum([f1, f2, f3, f4, f5, f6]))
  }
}
