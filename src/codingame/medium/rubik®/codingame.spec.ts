import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/rubik®/codingame'

describe('Rubik', () => {
  it(`2`, () => runTest(init, `2`, `8`))

  it(`3`, () => runTest(init, `3`, `26`))

  it(`4`, () => runTest(init, `4`, `56`))

  it(`5`, () => runTest(init, `5`, `98`))

  it(`6`, () => runTest(init, `6`, `152`))

  it(`1`, () => runTest(init, `1`, `1`))
})
