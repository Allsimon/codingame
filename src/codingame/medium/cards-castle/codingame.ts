export const init = () => {
  const N = +readline()
  if (!N) return
  const lines = [...Array(N)].map(() => readline())

  let unstable = lines.some(
    (l) => l.includes('.\\') || l.includes('/.') || l.includes('//') || l.includes('\\\\'),
  )

  if (!unstable) {
    const matrix = lines.map((l) => l.split(''))
    looooop: for (let i = 0; i < matrix.length - 1; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j] === '/' || matrix[i][j] === '\\') {
          if (matrix[i + 1][j] === '.' || matrix[i + 1][j] === matrix[i][j]) {
            unstable = true
            break looooop
          }
        }
      }
    }
  }

  print((unstable ? 'UN' : '') + 'STABLE')
}
