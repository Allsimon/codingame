import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/these-romans-are-crazy!/codingame'

describe('TheseRomansAreCrazy', () => {
  it(`6 + 7 = 13`, () =>
    runTest(
      init,
      `VI
VII`,
      `XIII`,
    ))

  it(`12 + 27 = 39`, () =>
    runTest(
      init,
      `XII
XXVII`,
      `XXXIX`,
    ))

  it(`123 + 321 = 444`, () =>
    runTest(
      init,
      `CXXIII
CCCXXI`,
      `CDXLIV`,
    ))

  it(`2016 + 999 = 3015`, () =>
    runTest(
      init,
      `MMXVI
CMXCIX`,
      `MMMXV`,
    ))

  it(`2016 + 999 = 3015`, () =>
    runTest(
      init,
      `MMXVI
CMXCIX`,
      `MMMXV`,
    ))
})
