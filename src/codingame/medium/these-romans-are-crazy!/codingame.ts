export const init = () => {
  const rom1 = readline()
  const rom2 = readline()
  if (!rom1) return

  const decode = (value: string): number =>
    value.split('').reduce((prev, l, i) => {
      const next = value[i + 1]
      let mod = 1
      if (l === 'I') {
        mod = next === 'V' || next === 'X' ? -1 : 1
      } else if (l === 'X') {
        mod = next === 'L' || next === 'C' ? -1 : 1
      } else if (l === 'C') {
        mod = next === 'D' || next === 'M' ? -1 : 1
      }
      return prev + mod * score[l]
    }, 0)

  const score: any = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000, MMM: 5000 }

  const findLetter = (value: number, power: number) =>
    Object.keys(score).filter((key) => score[key] === Math.pow(10, power) * value)[0]

  const encode = (value: number) => {
    let traced = 0
    let output = ''
    const numbers = []
    for (let i = 1; Math.pow(10, i - 1) < value; i++) {
      const current = ((value - traced) % Math.pow(10, i)) / Math.pow(10, i - 1)
      traced += current * Math.pow(10, i - 1)
      numbers.unshift(current)
    }

    for (let i = 0; i < numbers.length; i++) {
      const current = numbers[i]
      const currentPower = numbers.length - i
      const letter1 = findLetter(1, currentPower - 1)
      const letter5 = findLetter(5, currentPower - 1)
      if (current < 4) {
        output += [...Array(current)].map(() => letter1).join('')
      } else if (current === 4) {
        output += letter1 + letter5
      } else if (current < 9) {
        output += letter5 + [...Array(current - 5)].map(() => letter1).join('')
      } else {
        output += letter1 + findLetter(1, currentPower)
      }
    }
    return output
  }

  print(encode(decode(rom1) + decode(rom2)))
}
