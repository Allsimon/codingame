import { Mapp } from '@lib/type/type'

export const init = () => {
  const R = (+readline()).toString(2).padStart(8, '0').split('')
  const N = +readline()
  const startPattern = readline()
  if (N === undefined) return

  const foo: Mapp<string> = {
    '@@@': R[0],
    '@@.': R[1],
    '@.@': R[2],
    '@..': R[3],
    '.@@': R[4],
    '.@.': R[5],
    '..@': R[6],
    '...': R[7],
  }

  const neighborhood = (value: number, pattern: string) => {
    return (
      pattern[value === 0 ? pattern.length - 1 : value - 1] +
      pattern[value] +
      pattern[value === pattern.length - 1 ? 0 : value + 1]
    )
  }

  const charr = (value: number, pattern: string) =>
    foo[neighborhood(value, pattern)] === '1' ? '@' : '.'

  const evolve = (pattern: string) => {
    return pattern.split('').reduce((prev, current, i) => prev + charr(i, pattern), '')
  }

  let pattern = startPattern
  for (let i = 0; i < N; i++) {
    print(pattern)
    pattern = evolve(pattern)
  }
}
