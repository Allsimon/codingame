export const init = () => {
  const N = readline()
  if (!N) return

  const foo: any = { D: 1, R: 1, U: 1, L: 1 }
  const opposite: any = { D: 'U', R: 'L', U: 'D', L: 'R' }

  const fold = (dir: string) => {
    const opp = opposite[dir]
    Object.keys(foo)
      .filter((d) => d !== dir && d !== opp)
      .map((d) => (foo[d] = foo[d] * 2))
    foo[opp] = foo[opp] + foo[dir]
    foo[dir] = 1
  }
  N.split('').forEach(fold)

  print(foo[readline()])
  //   D  R  U  L
  //   1  1  1  1
  // D 1  2  2  2
  // R 2  1  4  4
  // U 6  2  1  8
  // L 12 10 2  1
}

type Dir = 'D' | 'R' | 'U' | 'L'
