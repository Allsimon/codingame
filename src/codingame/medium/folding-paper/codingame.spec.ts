import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/folding-paper/codingame'

describe('FoldingPaper', () => {
  it(`2 ply`, () =>
    runTest(
      init,
      `UL
D`,
      `4`,
    ))

  it(`4 ply`, () =>
    runTest(
      init,
      `DRUL
D`,
      `12`,
    ))

  it(`6 ply`, () =>
    runTest(
      init,
      `DRRULD
U`,
      `26`,
    ))

  it(`8 ply easy`, () =>
    runTest(
      init,
      `DRULUDDR
R`,
      `1`,
    ))

  it(`8 ply`, () =>
    runTest(
      init,
      `URDDLRLU
R`,
      `44`,
    ))
})
