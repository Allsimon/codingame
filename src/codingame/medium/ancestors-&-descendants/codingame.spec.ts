import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/ancestors-&-descendants/codingame'

describe('AncestorsDescendants', () => {
  it(`Test 1`, () =>
    runTest(
      init,
      `8
a
.b
..c
d
.e
..f
.g
h`,
      `a > b > c
d > e > f
d > g
h`,
    ))

  it(`Test 2`, () =>
    runTest(
      init,
      `19
a
.a1
..a11
...a111
...a112
..a12
.a2
b
.b1
..b11
.b2
..b21
...b211
c
.c1
..c11
...c111
....c1111
.c2`,
      `a > a1 > a11 > a111
a > a1 > a11 > a112
a > a1 > a12
a > a2
b > b1 > b11
b > b2 > b21 > b211
c > c1 > c11 > c111 > c1111
c > c2`,
    ))

  it(`Test 3`, () =>
    runTest(
      init,
      `28
a
.a1
..a11
..a12
...a121
..a13
...a131
....a1311
.....a13111
......a131111
...a132
..a14
..a15
.a2
.a3
.a4
b
.b1
.b2
c
.c1
..c11
...c111
....c1111
..c12
...c121
....c1211
...c122`,
      `a > a1 > a11
a > a1 > a12 > a121
a > a1 > a13 > a131 > a1311 > a13111 > a131111
a > a1 > a13 > a132
a > a1 > a14
a > a1 > a15
a > a2
a > a3
a > a4
b > b1
b > b2
c > c1 > c11 > c111 > c1111
c > c1 > c12 > c121 > c1211
c > c1 > c12 > c122`,
    ))

  it(`Test 4`, () =>
    runTest(
      init,
      `22
John
.Sam
..Randy
..George
...Alfred
.Justin
.Kelly
..Jack
...Lily
....Susan
.Jade
Robert
.Luke
Jonathan
Paul
Ken
.Flare
..Kelly
...Jessica
....Martin
.....Penelope
..David`,
      `John > Sam > Randy
John > Sam > George > Alfred
John > Justin
John > Kelly > Jack > Lily > Susan
John > Jade
Robert > Luke
Jonathan
Paul
Ken > Flare > Kelly > Jessica > Martin > Penelope
Ken > Flare > David`,
    ))

  it(`Test 5`, () =>
    runTest(
      init,
      `26
a
.b
..c
...d
....e
.....f
......g
.......h
........i
.........j
..........k
...........l
............m
............n
...........o
..........p
.........q
........r
.......s
......t
.....u
....v
...w
..x
.y
z`,
      `a > b > c > d > e > f > g > h > i > j > k > l > m
a > b > c > d > e > f > g > h > i > j > k > l > n
a > b > c > d > e > f > g > h > i > j > k > o
a > b > c > d > e > f > g > h > i > j > p
a > b > c > d > e > f > g > h > i > q
a > b > c > d > e > f > g > h > r
a > b > c > d > e > f > g > s
a > b > c > d > e > f > t
a > b > c > d > e > u
a > b > c > d > v
a > b > c > w
a > b > x
a > y
z`,
    ))

  it(`Test 6`, () =>
    runTest(
      init,
      `5
Jade
.Andrew
..Rose
.Mark
Heidi`,
      `Jade > Andrew > Rose
Jade > Mark
Heidi`,
    ))
})
