export const init = () => {
  const N = +readline()
  if (!N) return
  const lines = [...Array(N)].map(() => readline())
  const gotPoint = (value: number) => lines[value].includes('.')

  while (lines.some((l, i) => gotPoint(i))) {
    for (let i = 0; i < lines.length; i++) {
      if (!gotPoint(i)) {
        const value = lines[i]
        let j = i + 1
        for (; j < lines.length; j++) {
          if (!gotPoint(j)) break
          lines[j] = lines[j].replace(/.*?\./, value + ' > ')
        }
      }
    }
  }

  let output = []

  for (let i = 0; i < lines.length; i++)
    if (!(lines[i + 1] || '').startsWith(lines[i])) output.push(lines[i])

  output.forEach((l) => print(l))
}
