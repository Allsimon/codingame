import { sum } from '@lib/array/arrays'

export const init = () => {
  const P = +readline()
  if (P === undefined) return
  const players: Player[] = [...Array(P)]
    .map(() => readline())
    .map((l) => {
      const [name, position] = l.split(' ')
      return { name: name, position: +position }
    })

  const D = +readline()
  const dices = [...Array(D)].map(() => readline().split(' ').map(Number))
  const board = [...Array(40)].map(() => readline())

  const isDouble = (dice: number[]) => dice[0] === dice[1]

  const move = (player: Player, dice: number[], newTurn = false) => {
    player.position = (player.position + sum(dice)) % 40
    if (player.position === 30) {
      goToJail(player)
    } else {
      player.inJail = false
    }
    if (newTurn) player.turn = 0
  }

  const goToJail = (player: Player) => {
    player.position = 10
    player.inJail = true
    player.turn = 0
  }

  const play = (player: Player) => {
    const dice = dices.shift()
    if (dice === undefined) return
    player.turn = (player.turn || 0) + 1
    if (player.inJail) {
      if (isDouble(dice)) {
        move(player, dice, true)
      } else if (player.turn === 3) {
        move(player, dice, true)
      }
    } else {
      if (isDouble(dice)) {
        if (player.turn === 3) {
          goToJail(player)
        } else {
          move(player, dice)
          if (!player.inJail) play(player)
        }
      } else {
        move(player, dice, true)
      }
    }
  }

  while (dices.length !== 0) {
    players.forEach(play)
  }

  players.forEach((p) => print(p.name + ' ' + p.position))
}

interface Player {
  name: string
  position: number
  inJail?: boolean
  turn?: number
}
