import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/simplified-monopoly-turns-prediction/codingame'

describe('SimplifiedMonopolyTurnsPrediction', () => {
  it(`Start from Go`, () =>
    runTest(
      init,
      `2
Horse 0
TopHat 0
6
1 6
2 1
6 4
2 1
4 5
6 5
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Horse 26
TopHat 17`,
    ))

  it(`Not start from Go`, () =>
    runTest(
      init,
      `2
Boot 13
Car 8
4
1 2
5 3
3 2
6 4
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Boot 21
Car 26`,
    ))

  it(`Around the board`, () =>
    runTest(
      init,
      `2
Thimble 25
Wheelbarrow 21
6
5 4
6 5
2 4
5 2
6 3
1 6
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Thimble 9
Wheelbarrow 6`,
    ))

  it(`Lucky doubles`, () =>
    runTest(
      init,
      `2
Cat 5
CoinPurse 7
8
2 2
6 6
5 3
5 5
2 3
3 3
1 1
6 5
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Cat 8
CoinPurse 22`,
    ))

  it(`Go to jail`, () =>
    runTest(
      init,
      `2
Lantern 25
ClothesIron 18
9
3 2
5 3
3 5
6 6
5 4
4 2
4 3
5 4
2 2
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Lantern 19
ClothesIron 18`,
    ))

  it(`Early release`, () =>
    runTest(
      init,
      `2
Cat 18
ScottieDog 12
5
6 6
2 3
2 2
4 3
2 1
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Cat 17
ScottieDog 24`,
    ))

  it(`Friends forever`, () =>
    runTest(
      init,
      `4
Lantern 23
ClothesIron 19
Thimble 28
CoinPurse 18
10
6 1
6 5
1 1
6 6
2 3
5 4
3 2
1 6
5 1
2 6
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Lantern 10
ClothesIron 10
Thimble 10
CoinPurse 10`,
    ))

  it(`Big run`, () =>
    runTest(
      init,
      `6
Thimble 0
Wheelbarrow 0
Lantern 0
CoinPurse 0
ClothesIron 0
Cat 0
57
5 5
3 2
4 2
5 1
3 6
2 2
1 1
3 3
4 6
5 5
1 2
4 5
3 3
6 5
4 1
1 1
2 2
3 3
5 6
1 1
4 4
3 3
3 4
2 1
6 4
1 3
1 4
3 1
6 6
5 5
2 2
2 5
1 5
3 3
4 4
1 1
4 4
1 3
3 1
4 2
5 4
3 6
1 2
2 2
6 6
6 6
3 3
3 3
5 5
1 1
3 3
2 2
4 4
1 1
3 3
5 5
6 2
Go
Mediterranean Avenue
Community Chest
Baltic Avenue
Income Tax
Reading Railroad
Oriental Avenue
Chance
Vermont Avenue
Connecticut Avenue
Visit Only / In Jail
St. Charles Place
Electric Company
States Avenue
Virginia Avenue
Pennsylvania Railroad
St. James Place
Community Chest
Tennessee Avenue
New York Avenue
Free Parking
Kentucky Avenue
Chance
Indiana Avenue
Illinois Avenue
B. & O. Railroad
Atlantic Avenue
Ventnor Avenue
Water Works
Marvin Gardens
Go To Jail
Pacific Avenue
North Carolina Avenue
Community Chest
Pennsylvania Avenue
Short Line
Chance
Park Place
Luxury Tax
Boardwalk`,
      `Thimble 0
Wheelbarrow 16
Lantern 10
CoinPurse 14
ClothesIron 18
Cat 12`,
    ))
})
