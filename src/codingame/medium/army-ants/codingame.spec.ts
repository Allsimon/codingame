import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/army-ants/codingame'

describe('ArmyAnts', () => {
  it(`Test 1`, () =>
    runTest(
      init,
      `3 3
ABC
DEF
2
`,
      `CDBEAF
`,
    ))

  it(`Test 2`, () =>
    runTest(
      init,
      `5 4
eandC
oiGm
4`,
      `CodinGame
`,
    ))

  it(`Test 3`, () =>
    runTest(
      init,
      `1 5
A
BCDEF
3
`,
      `BCDAEF
`,
    ))

  it(`Test 4`, () =>
    runTest(
      init,
      `7 7
ABCDEFM
GHIJKLN
4
`,
      `MFEGDHCIBJAKLN
`,
    ))

  it(`Test 5`, () =>
    runTest(
      init,
      `14 12
ABCDEFGHIJKLMN
OPQRSTUVWXYZ
24
`,
      `OPQRSTUVWXYNZMLKJIHGFEDCBA
`,
    ))
})
