export const init = () => {
  const max = Math.max(...readline().split(' ').map(Number))
  const S1 = readline()
    .split('')
    .reverse()
    .map((l) => ({ name: l, left: true }))
  const S2 = readline()
    .split('')
    .map((l) => ({ name: l, left: false }))
  const T = +readline()

  const output = S1.concat(S2)
  for (let i = 0; i < T; i++) {
    for (let j = 0; j < output.length - 1; j++) {
      if (output[j].left) {
        if (!output[j + 1].left) {
          const old = output[j]
          output[j] = output[j + 1]
          output[j + 1] = old
          j++
        }
      }
    }
  }

  print(output.map((l) => l.name).join(''))
}

interface Ant {
  name: string
  left: boolean
}
