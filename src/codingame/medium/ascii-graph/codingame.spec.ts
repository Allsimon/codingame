import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/ascii-graph/codingame'

describe('AsciiGraph', () => {
  it(`Exemple`, () =>
    runTest(
      init,
      `1
1 1`,
      `.|..
.|*.
-+--
.|..`,
    ))

  it(`Pas de points`, () =>
    runTest(
      init,
      `0`,
      `.|.
-+-
.|.`,
    ))

  it(`Valeurs négatives`, () =>
    runTest(
      init,
      `1
-1 -1`,
      `..|.
--+-
.*|.
..|.`,
    ))

  it(`Deux points`, () =>
    runTest(
      init,
      `2
-4 3
3 -2`,
      `.....|....
.*...|....
.....|....
.....|....
-----+----
.....|....
.....|..*.
.....|....`,
    ))

  it(`Plusieurs points`, () =>
    runTest(
      init,
      `10
5 7
-8 4
6 10
2 2
3 4
4 1
3 6
8 9
7 4
1 0`,
      `.........|.........
.........|.....*...
.........|.......*.
.........|.........
.........|....*....
.........|..*......
.........|.........
.*.......|..*...*..
.........|.........
.........|.*.......
.........|...*.....
---------+*--------
.........|.........`,
    ))

  it(`Point à l'origine`, () =>
    runTest(
      init,
      `1
0 0`,
      `.|.
-*-
.|.`,
    ))
})
