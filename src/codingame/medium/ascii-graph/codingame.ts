import { emptyMatrix, printMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const N = +readline()
  if (N === undefined) return
  const lines = [...Array(N)].map(() => readline().split(' ').map(Number))

  const [xMin, yMin] = lines.reduce(
    (p, c) => [Math.min(p[0], c[0] - 1), Math.min(p[1], c[1] - 1)],
    [-1, -1],
  )
  const [xMax, yMax] = lines.reduce(
    (p, c) => [Math.max(p[0], c[0] + 1), Math.max(p[1], c[1] + 1)],
    [1, 1],
  )

  const [width, height] = [xMax - xMin + 1, yMax - yMin + 1]
  // origin
  const [xOrg, yOrg] = [yMax, -xMin]

  const matrix = emptyMatrix(height, width)

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      let symbol = '.'
      if (yMax - i === 0) {
        symbol = '-'
        if (j + xMin === 0) symbol = '+'
      } else if (j + xMin === 0) symbol = '|'
      matrix[i][j] = symbol
    }
  }

  const drawPoint = (coords: number[]) => {
    const [x, y] = coords
    const [p1, p2] = [xOrg - y, yOrg + x]
    matrix[p1][p2] = '*'
  }

  lines.forEach(drawPoint)
  print(printMatrix(matrix, (t) => '' + t).trimRight())
}
