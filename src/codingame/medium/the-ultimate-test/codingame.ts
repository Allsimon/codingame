// TheUltimateTest
import { sum } from '@lib/array/arrays'
import { flatten } from '@lib/matrix/matrix'

export const init = () => {
  const N = readline()
  const K = +readline()

  const possibilities = new Array<string[]>()

  const fetchPossibility = (value: string, letters: number, slices: string[]) => {
    const current = value.slice(0, letters)
    const rest = value.slice(letters)
    if (letters !== 0) slices.push(current)
    if (rest.length === 0) possibilities.push(slices)
    for (let i = 1; i <= rest.length; i++) {
      fetchPossibility(rest, i, [...slices])
    }
  }

  const checkPossibility = (values: string[]): string[][] => {
    const output = new Array<string[]>()
    let group = values.shift()
    if (!group) throw new Error('Empty ')
    output.push([group], ['-' + group])
    while ((group = values.shift()) !== undefined) {
      const length = output.length
      for (let i = 0; i < length; i++) {
        const out = output[i]
        output.push([...out, '-' + group])
        output[i].push(group)
      }
    }
    return output.filter((o) => sum(o.map(Number)) === K)
  }

  fetchPossibility(N, 0, [])

  const customSort = (a1: string, a2: string) =>
    a1
      .replace(/\+/g, 'a')
      .replace(/-/g, 'b')
      .localeCompare(a2.replace(/\+/g, 'a').replace(/-/g, 'b'))

  const output = flatten(possibilities.map(checkPossibility))
    .map((l) => {
      let out = ''
      for (let i = 0; i < l.length; i++) {
        if (l[i].startsWith('-')) {
          out += l[i]
        } else {
          out += (i !== 0 ? '+' : '') + l[i]
        }
      }
      return out
    })
    .filter((l) => !l.startsWith('-'))
    .sort(customSort)
    .join('\n')

  print(output)
}
