import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/the-ultimate-test/codingame'

describe('TheUltimateTest', () => {
  it(`Test 1`, () =>
    runTest(
      init,
      `123
6
`,
      `1+2+3
`,
    ))

  it(`Test 2`, () =>
    runTest(
      init,
      `123987654
200
`,
      `123+9-8+7+65+4
`,
    ))

  it(`Test 3`, () =>
    runTest(
      init,
      `1234789
45
`,
      `123+4+7-89
12+3+47-8-9
1+2+34+7-8+9
`,
    ))

  it(`Test 4`, () =>
    runTest(
      init,
      `89765421
65
`,
      `8+97+6-5-42+1
8+97-65+4+21
8-9+76-5-4-2+1
8-9+7+6+54-2+1
`,
    ))
})
