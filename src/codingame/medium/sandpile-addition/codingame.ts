import { doOnAdjacent, flatten, printMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const N = +readline()
  if (N === undefined) return
  const p1 = [...Array(N)].map(() => readline().split('').map(Number))
  const p2 = [...Array(N)].map(() => readline().split('').map(Number))

  for (let i = 0; i < p1.length; i++)
    for (let j = 0; j < p1[i].length; j++) p1[i][j] = p1[i][j] + p2[i][j]

  const trickle = (matrix: number[][]) => {
    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j] >= 4) {
          matrix[i][j] = matrix[i][j] - 4
          doOnAdjacent(matrix, i, j, false, (x, y) => (matrix[x][y] = matrix[x][y] + 1))
        }
      }
    }
  }
  while (!flatten(p1).every((t) => t < 4)) trickle(p1)

  print(printMatrix(p1, (t) => '' + t).trimRight())
}
