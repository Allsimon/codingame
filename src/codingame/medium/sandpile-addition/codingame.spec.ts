import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/sandpile-addition/codingame'

describe('SandpileAddition', () => {
  it(`One simple addition`, () =>
    runTest(
      init,
      `3
121
202
121
020
202
020`,
      `313
101
313`,
    ))

  it(`One more simple case`, () =>
    runTest(
      init,
      `3
301
030
103
100
010
001`,
      `021
202
120`,
    ))

  it(`A lot of redistribution`, () =>
    runTest(
      init,
      `3
121
232
121
303
000
303`,
      `212
131
212`,
    ))

  it(`Pretty packed`, () =>
    runTest(
      init,
      `3
333
333
333
111
111
111`,
      `030
303
030`,
    ))

  it(`Two full sandpiles`, () =>
    runTest(
      init,
      `3
333
333
333
333
333
333`,
      `212
121
212`,
    ))

  it(`Bigger grid`, () =>
    runTest(
      init,
      `4
3113
1221
1221
3113
1002
0300
0030
2001`,
      `0331
3303
3033
1330`,
    ))

  it(`Even bigger`, () =>
    runTest(
      init,
      `5
31003
13120
01310
02131
30013
11111
12221
12321
12221
11111`,
      `22112
23321
13231
12332
21122`,
    ))

  it(`Asymmetric`, () =>
    runTest(
      init,
      `3
012
302
130
120
312
312`,
      `020
123
330`,
    ))
})
