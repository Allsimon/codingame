import { printMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const N = readline()
  if (N === undefined) return
  let [W, H] = N.split(' ').map(Number)
  const lines = [...Array(H)].map(() => readline().split(''))

  const fall = (matrix: string[][]) => {
    for (let i = 0; i < W; i++) {
      let sum = 0
      for (let j = 0; j < H; j++) {
        if (matrix[j][i] === '#') sum++
      }
      for (let j = H - 1; j >= 0; j--) {
        matrix[j][i] = j < H - sum ? '.' : '#'
      }
    }
  }
  fall(lines)
  print(printMatrix(lines, (t) => t).trimRight())
}
