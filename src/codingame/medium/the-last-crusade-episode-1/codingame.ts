import { Mapp } from '@lib/type/type'

export const init = () => {
  const [W, H] = readline().split(' ').map(Number)
  const LINES = [...Array(H)].map(() => readline().split(' ').map(Number))
  const EX = +readline() // the coordinate along the X axis of the exit (not useful for this first mission, but must be read).

  const map: Mapp<TypeGiver> = {
    '1': { TOP: 'DOWN', DOWN: undefined, LEFT: 'DOWN', RIGHT: 'DOWN' },
    '2': { TOP: undefined, DOWN: undefined, LEFT: 'RIGHT', RIGHT: 'LEFT' },
    '3': { TOP: 'DOWN', DOWN: undefined, LEFT: undefined, RIGHT: undefined },
    '4': { TOP: 'LEFT', DOWN: undefined, LEFT: undefined, RIGHT: 'DOWN' },
    '5': { TOP: 'RIGHT', DOWN: undefined, LEFT: 'DOWN', RIGHT: undefined },
    '6': { TOP: undefined, DOWN: undefined, LEFT: 'RIGHT', RIGHT: 'LEFT' },
    '7': { TOP: 'DOWN', DOWN: undefined, LEFT: undefined, RIGHT: 'DOWN' },
    '8': { TOP: undefined, DOWN: undefined, LEFT: 'DOWN', RIGHT: 'DOWN' },
    '9': { TOP: 'DOWN', DOWN: undefined, LEFT: 'DOWN', RIGHT: undefined },
    '10': { TOP: 'LEFT', DOWN: undefined, LEFT: undefined, RIGHT: undefined },
    '11': { TOP: 'RIGHT', DOWN: undefined, LEFT: undefined, RIGHT: undefined },
    '12': { TOP: undefined, DOWN: undefined, LEFT: undefined, RIGHT: 'DOWN' },
    '13': { TOP: undefined, DOWN: undefined, LEFT: 'DOWN', RIGHT: undefined },
  }

  const modify = (dir: Dir, x: number, y: number) => {
    switch (dir) {
      case 'TOP':
        return [x - 1, y]
      case 'DOWN':
        return [x + 1, y]
      case 'LEFT':
        return [x, y - 1]
      case 'RIGHT':
        return [x, y + 1]
    }
  }

  // game loop
  while (true) {
    const [XI, YI, POS] = readline().split(' ')
    const goingTo = map[LINES[+YI][+XI]][POS as Dir]
    if (goingTo !== undefined) {
      const output = modify(goingTo, +YI, +XI)
      print(output.reverse().join(' '))
    }
  }
}

interface TypeGiver {
  TOP?: Dir
  DOWN?: Dir
  LEFT?: Dir
  RIGHT?: Dir
}

type Dir = 'TOP' | 'DOWN' | 'LEFT' | 'RIGHT'
