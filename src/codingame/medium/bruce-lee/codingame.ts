import { chunkSubstr } from '@lib/string/strings'

export const init = () => {
  const N = readline()
  if (N === undefined) return
  const encoded = N.split(' ')

  const read = (value: number): string => {
    switch (encoded[value]) {
      case '0':
        return '1'
      case '00':
        return '0'
    }
    throw new Error('')
  }

  try {
    if (N.replace(/\s/g, '').match(/\D/g)) {
      print('INVALID')
    } else {
      let output = ''
      for (let i = 0; i < encoded.length - 1; i = i + 2) {
        const bit = read(i)
        const length = encoded[i + 1].length
        output += bit.repeat(length)
      }

      const parse = (value: string) => String.fromCharCode(parseInt(value, 2))
      const chunks = chunkSubstr(output, 7)
      if (chunks.every((chunk) => chunk.length === 7)) {
        const s = chunks.reduce((prev, current) => prev + parse(current), '')
        print(s)
      } else {
        print('INVALID')
      }
    }
  } catch (e) {
    print('INVALID')
  }
}
