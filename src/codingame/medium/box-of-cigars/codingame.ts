export const init = () => {
  const N = +readline()
  if (N === undefined) return
  const cigars = [...Array(N)].map(() => readline()).map(Number)

  const gotNext = (base: number, diff: number) => {
    let i = 0
    while (diff !== 0 && cigars.includes(base + diff * i++)) {}
    return i
  }

  const max = cigars.reduce((previous, current, i) => {
    for (let j = i + 1; j < cigars.length; j++) {
      const diff = cigars[j] - current
      previous = Math.max(previous, gotNext(cigars[j], diff))
    }
    return previous
  }, 0)

  print(max)
}
