import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/cgfunge-interpreter/codingame'

describe('CgfungeInterpreter', () => {
  it(`Just math`, () =>
    runTest(
      init,
      `1
2419+*+IE
`,
      `42
`,
    ))

  it(`Simple text`, () =>
    runTest(
      init,
      `1
"txet elpmiS"CCCCCCCCCCCE
`,
      `Simple text
`,
    ))

  it(`Go all ways`, () =>
    runTest(
      init,
      `8
v > v
  C "
 vC GS<
> "GC"^
  C "
  G C
 C" C
 >^ E
`,
      `CGCGCG
`,
    ))

  it(`Very hello world`, () =>
    runTest(
      init,
      `8
>0111>>>>>>>>v
v<<<<<<<<<<<<<
>>>>>>"olleH"v
^CC<<<CGFunge 
>" ,"^Rulz    
|<<<<<<CCCCC<<
>>>>"!dlroW "v
E<<<<CCCCCCC<<
`,
      `Hello, Hello, Hello, Hello World!
`,
    ))

  it(`Sing me a song`, () =>
    runTest(
      init,
      `10
39DD1+*+DI  >11091+   v>v
 v  " on the wall"    < D 
>>     "reeb fo"      v S
0v<" bottles "        < C
X>DSC_SvPD      _25*+Cv |
       *v   IS        < P
^IDX-1 <>  SC        0v X
v   "pass it around"  < 1
>    " ,nwod eno ekat" ^-
 Sing it!   ^+55D-1X_ESD<
`,
      `99 bottles of beer on the wall
99 bottles of beer
take one down, pass it around
98 bottles of beer on the wall
98 bottles of beer
take one down, pass it around
97 bottles of beer on the wall
97 bottles of beer
take one down, pass it around
`,
    ))

  it(`Skip & short lines`, () =>
    runTest(
      init,
      `4
                v Go down!
Skip next line  S
Short line
Finish here --> > "!yaY" CCCCE`,
      `Yay!`,
    ))
})
