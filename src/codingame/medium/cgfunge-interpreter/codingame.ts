// CgfungeInterpreter

import { isNumeric } from '@lib/number/numbers'

export const init = () => {
  const N = +readline()
  const lines = [...Array(N)].map(() => readline().split(''))

  const stack: number[] = []
  let isStringMode = false
  let currentDirection = Dir.RIGHT
  let [x, y] = [0, 0]
  let currentText = ''

  const push = (value: string | number) => {
    stack.push(Math.abs(!isNumeric('' + value) ? ('' + value).charCodeAt(0) : +value))
  }
  const pop = () => {
    const value = stack.pop()
    if (value === undefined) throw new Error('wat')
    return value
  }

  const move = () => {
    if (currentDirection === Dir.DOWN) {
      ;[x, y] = [x, y + 1]
    } else if (currentDirection === Dir.TOP) {
      ;[x, y] = [x, y - 1]
    } else if (currentDirection === Dir.LEFT) {
      ;[x, y] = [x - 1, y]
    } else if (currentDirection === Dir.RIGHT) {
      ;[x, y] = [x + 1, y]
    }
  }

  const interpret = (value: string) => {
    if (value === '"') {
      isStringMode = !isStringMode
    } else if (isNumeric(value) || isStringMode) {
      push(value)
    } else if (value === 'P') {
      pop()
    } else if (value === 'X') {
      const [x, y] = [pop(), pop()]
      push(x)
      push(y)
    } else if (value === 'D') {
      const x = pop()
      push(x)
      push(x)
    } else if (value === '_') {
      currentDirection = pop() === 0 ? Dir.RIGHT : Dir.LEFT
    } else if (value === '|') {
      currentDirection = pop() === 0 ? Dir.DOWN : Dir.TOP
    } else if (value === 'I') {
      currentText += pop()
    } else if (value === 'C') {
      currentText += String.fromCharCode(pop())
    } else if (value === 'S') {
      move()
    } else if (value === '+') {
      push(pop() + pop())
    } else if (value === '-') {
      push(pop() - pop())
    } else if (value === '*') {
      push(pop() * pop())
    } else if (value === '' || value === ' ') {
      // do nothing
    } else if (value === '>') {
      currentDirection = Dir.RIGHT
    } else if (value === '<') {
      currentDirection = Dir.LEFT
    } else if (value === '^') {
      currentDirection = Dir.TOP
    } else if (value === 'v') {
      currentDirection = Dir.DOWN
    } else {
      push(value)
    }
  }

  while (lines[y][x] !== 'E') {
    interpret(lines[y][x])
    move()
  }
  print(currentText)
}

const enum Dir {
  DOWN,
  LEFT,
  TOP,
  RIGHT,
}
