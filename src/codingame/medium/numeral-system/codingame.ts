// NumeralSystem
export const init = () => {
  const N = readline()
  const [foo, Z] = N.split('=')
  const [X, Y] = foo.split('+')

  for (let i = 2; i <= 36; i++) {
    if (parseInt(X, i) + parseInt(Y, i) === parseInt(Z, i)) {
      print(i)
      break
    }
  }
}
