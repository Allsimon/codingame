import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/numeral-system/codingame'

describe('NumeralSystem', () => {
  it(`Easy`, () => runTest(init, `52+38=101`, `9`))

  it(`Normal`, () => runTest(init, `2B+15A=184`, `17`))

  it(`Corner case min`, () => runTest(init, `1+1=10`, `2`))

  it(`Corner case max`, () => runTest(init, `HYYYYYYYYY+DYYYYYYYYY=VXXXXXXXXW`, `36`))

  it(`Minimum valid system`, () => runTest(init, `2+2=4`, `5`))
})
