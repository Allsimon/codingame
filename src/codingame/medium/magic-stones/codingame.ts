export const init = () => {
  const N = +readline()
  if (!N) return
  let map = readline().split(' ').map(Number)

  let found = true

  while (found) {
    found = false
    map = map.sort((a, b) => b - a)
    for (let i = 0; i < map.length; i++) {
      if (map[i] === map[i + 1]) {
        found = true
        map.push(map[i] + 1)
        map.splice(i, 2)
      }
    }
  }

  print(map.length)
}
