// IpMaskCalculating
import { decimalToBase } from '@lib/number/numbers'
import { chunk, fillUpTo } from '@lib/array/arrays'

export const init = () => {
  const [ip, mask] = readline().split('/')
  const ips = ip.split('.').map((v) => fillUpTo(decimalToBase(+v, 2), 0, 8).map((t) => t === 1))
  const bMask = chunk(
    [...Array(32)].map((_, i) => i < +mask),
    8,
  ).map((l) => l.reverse())

  const network = ips.map((subV, i) => subV.map((v, j) => bMask[i][j] && v))
  const broadcast = ips.map((subV, i) => subV.map((v, j) => v || !bMask[i][j]))

  const value = (arr: boolean[][]) =>
    arr
      .map((sub) =>
        parseInt(
          sub
            .map((v) => (v ? 1 : 0))
            .reverse()
            .join(''),
          2,
        ),
      )
      .join('.')

  print(value(network))
  print(value(broadcast))
}
