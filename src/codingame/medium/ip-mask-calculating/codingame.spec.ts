import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/ip-mask-calculating/codingame'

describe('IpMaskCalculating', () => {
  it(`Your ip at home`, () =>
    runTest(
      init,
      `192.168.0.5/24`,
      `192.168.0.0
192.168.0.255`,
    ))

  it(`Your ip at school`, () =>
    runTest(
      init,
      `10.0.1.34/24`,
      `10.0.1.0
10.0.1.255`,
    ))

  it(`Not a real network mask`, () =>
    runTest(
      init,
      `77.45.23.98/32`,
      `77.45.23.98
77.45.23.98`,
    ))

  it(`Little mask (not a real network mask too)`, () =>
    runTest(
      init,
      `78.23.88.1/0`,
      `0.0.0.0
255.255.255.255
`,
    ))

  it(`255.255.240.0`, () =>
    runTest(
      init,
      `234.67.0.5/20`,
      `234.67.0.0
234.67.15.255`,
    ))

  it(`254.0.0.0`, () =>
    runTest(
      init,
      `77.65.155.7/7`,
      `76.0.0.0
77.255.255.255`,
    ))

  it(`A strange IP`, () =>
    runTest(
      init,
      `09.45.000.2/18`,
      `9.45.0.0
9.45.63.255`,
    ))

  it(`42`, () =>
    runTest(
      init,
      `42.42.42.42/10`,
      `42.0.0.0
42.63.255.255
`,
    ))
})
