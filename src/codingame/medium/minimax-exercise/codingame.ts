import { computeMMRootNode, MMNode } from '@lib/game/alpha-beta'

export const init = () => {
  const [D, B] = readline().split(' ').map(Number)
  if (B === undefined) return
  const leaves = readline().split(' ').map(Number)

  const root = computeMMRootNode(D, B, leaves)

  let visited = 0
  const alphaBeta = (
    node: MMNode,
    alpha = -Infinity,
    beta = Infinity,
    maximizingPlayer = true,
  ): number => {
    visited++
    if (node.children.length === 0) {
      if (node.value == undefined) throw new Error('No value found on leaf')
      return node.value
    }
    if (maximizingPlayer) {
      let value = -Infinity
      for (let child of node.children) {
        value = Math.max(value, alphaBeta(child, alpha, beta, false))
        alpha = Math.max(alpha, value)
        if (alpha >= beta) break
      }
      return value
    } else {
      let value = Infinity
      for (let child of node.children) {
        value = Math.min(value, alphaBeta(child, alpha, beta, true))
        beta = Math.min(beta, value)
        if (alpha >= beta) break
      }
      return value
    }
  }
  const result = alphaBeta(root)

  print([result, visited].join(' '))
}
