import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/10101/codingame'

describe('10101', () => {
  it(`Tiny`, () =>
    runTest(
      init,
      `3
3
..#
..#
##.`,
      `4`,
    ))

  it(`Stretch`, () =>
    runTest(
      init,
      `5
3
###..
###..
.....`,
      `2`,
    ))

  it(`You lose`, () =>
    runTest(
      init,
      `6
6
.#.#.#
#.#.#.
.#.#.#
#.#.#.
.#.#.#
#.#.#.`,
      `0`,
    ))

  it(`Big`, () =>
    runTest(
      init,
      `10
10
#########.
########..
#######..#
.#........
#######.##
########..
########..
#..#######
#..#######
.#..#..#..`,
      `3`,
    ))
})
