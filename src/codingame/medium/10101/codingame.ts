// 10101
import { cloneMatrix, printMatrix, transpose } from '@lib/matrix/matrix'

export const init = () => {
  const W = +readline()
  const H = +readline()
  const matrix = [...Array(H)].map(() => readline().split(''))

  const filledRow = (mat: string[][]) =>
    mat.map((row) => row.filter((v) => v === '.').length).filter((l) => l === 0).length

  const placeBlock = (x: number, y: number) => {
    const next = [
      [x, y],
      [x + 1, y],
      [x + 1, y + 1],
      [x, y + 1],
    ]
    const possible = next.every((coord) => matrix[coord[0]][coord[1]] === '.')
    if (!possible) return 0

    const clone = cloneMatrix(matrix)
    next.forEach((coord) => (clone[coord[0]][coord[1]] = '#'))
    return filledRow(clone) + filledRow(transpose(clone))
  }

  const filled = new Array<number>()
  for (let i = 0; i < H - 1; i++) for (let j = 0; j < W - 1; j++) filled.push(placeBlock(i, j))

  print(Math.max(...filled))
}
