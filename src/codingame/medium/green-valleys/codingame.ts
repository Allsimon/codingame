import { doOnAdjacent } from '@lib/matrix/matrix'

export const init = () => {
  const H = +readline()
  const N = +readline()
  if (N === undefined) return
  const lines = [...Array(N)].map(() => readline().split(' ').map(Number))

  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N; j++) {
      if (lines[i][j] > H) lines[i][j] = -1
    }
  }

  const check = (x: number, y: number, obj: Holder) => {
    doOnAdjacent(lines, x, y, false, (i, j) => {
      if (lines[i][j] >= 0) {
        obj.min = Math.min(lines[i][j], obj.min)
        obj.size++
        lines[i][j] = -1
        check(i, j, obj)
      }
    })
  }

  const output = []
  for (let i = 0; i < N; i++) {
    for (let j = 0; j < N; j++) {
      let obj = { size: 1, min: Infinity }
      if (lines[i][j] >= 0) check(i, j, obj)

      output.push(obj)
    }
  }
  const min = (output.sort((a, b) => b.size - a.size)[0] || { min: 0 }).min
  print(min == Infinity ? 0 : min)
}

interface Holder {
  size: number
  min: number
}
