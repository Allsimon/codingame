export const init = () => {
  const N = +readline()

  if (!N) return
  const times = [...Array(N)].map(() => readline())
  const timeSecond = times
    .map((v) => v.split(':').map(Number))
    .map((value) => {
      const [hours, minutes, seconds] = value
      return seconds + minutes * 60 + hours * 3600
    })

  print(times[timeSecond.indexOf(Math.min(...timeSecond))])
}
