export const init = () => {
  const N = +readline()
  if (N === undefined) return
  const lines = readline().split(' ').map(Number)

  const nbrOf1 = lines.reduce((prev, curr) => prev + (curr === 1 ? 1 : 0), 0)
  let output = 0
  for (let i = 0; i < nbrOf1; i++) if (lines[i] === 0) output++

  print(output)
}
