import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/bender-episode-1/codingame'

describe('BenderEpisode1', () => {
  it(`Simple mouvements`, () =>
    runTest(
      init,
      `5 5
#####
#@  #
#   #
#  $#
#####
`,
      `SOUTH
SOUTH
EAST
EAST
`,
    ))

  it(`Obstacles`, () =>
    runTest(
      init,
      `8 8
########
# @    #
#     X#
# XXX  #
#   XX #
#   XX #
#     $#
########
`,
      `SOUTH
EAST
EAST
EAST
SOUTH
EAST
SOUTH
SOUTH
SOUTH
`,
    ))

  it(`Priorités`, () =>
    runTest(
      init,
      `8 8
########
#     $#
#      #
#      #
#  @   #
#      #
#      #
########
`,
      `SOUTH
SOUTH
EAST
EAST
EAST
NORTH
NORTH
NORTH
NORTH
NORTH
`,
    ))

  it(`Ligne droite`, () =>
    runTest(
      init,
      `8 8
########
#      #
# @    #
# XX   #
#  XX  #
#   XX #
#     $#
########
`,
      `EAST
EAST
EAST
EAST
SOUTH
SOUTH
SOUTH
SOUTH
`,
    ))

  it(`Modificateurs de trajectoire`, () =>
    runTest(
      init,
      `10 10
##########
#        #
#  S   W #
#        #
#  $     #
#        #
#@       #
#        #
#E     N #
##########
`,
      `SOUTH
SOUTH
EAST
EAST
EAST
EAST
EAST
EAST
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
WEST
WEST
WEST
WEST
SOUTH
SOUTH
`,
    ))

  it(`Mode casseur`, () =>
    runTest(
      init,
      `10 10
##########
# @      #
# B      #
#XXX     #
# B      #
#    BXX$#
#XXXXXXXX#
#        #
#        #
##########
`,
      `SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
EAST
EAST
EAST
`,
    ))

  it(`Inverseur`, () =>
    runTest(
      init,
      `10 10
##########
#    I   #
#        #
#       $#
#       @#
#        #
#       I#
#        #
#        #
##########
`,
      `SOUTH
SOUTH
SOUTH
SOUTH
WEST
WEST
WEST
WEST
WEST
WEST
WEST
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
EAST
EAST
EAST
EAST
EAST
EAST
EAST
SOUTH
SOUTH
`,
    ))

  it(`Téléportation`, () =>
    runTest(
      init,
      `10 10
##########
#    T   #
#        #
#        #
#        #
#@       #
#        #
#        #
#    T  $#
##########
`,
      `SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
EAST
EAST
EAST
EAST
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
`,
    ))

  it(`Mur cassé ?`, () =>
    runTest(
      init,
      `10 10
##########
#        #
#  @     #
#  B     #
#  S   W #
# XXX    #
#  B   N #
# XXXXXXX#
#       $#
##########
`,
      `SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
EAST
NORTH
NORTH
WEST
WEST
WEST
WEST
SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
EAST
EAST
`,
    ))

  it(`All together`, () =>
    runTest(
      init,
      `15 15
###############
#      IXXXXX #
#  @          #
#             #
#             #
#  I          #
#  B          #
#  B   S     W#
#  B   T      #
#             #
#         T   #
#         B   #
#            $#
#        XXXX #
###############
`,
      `SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
WEST
WEST
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
EAST
EAST
EAST
EAST
EAST
EAST
EAST
EAST
EAST
EAST
EAST
EAST
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
WEST
WEST
WEST
WEST
WEST
WEST
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
`,
    ))

  it(`LOOP`, () =>
    runTest(
      init,
      `15 15
###############
#      IXXXXX #
#  @          #
#E S          #
#             #
#  I          #
#  B          #
#  B   S     W#
#  B   T      #
#             #
#         T   #
#         B   #
#N          W$#
#        XXXX #
###############
`,
      `LOOP
`,
    ))

  it(`Boucles multiples ?`, () =>
    runTest(
      init,
      `30 15
###############
#  #@#I  T$#  #
#  #    IB #  #
#  #     W #  #
#  #      ##  #
#  #B XBN# #  #
#  ##      #  #
#  #       #  #
#  #     W #  #
#  #      ##  #
#  #B XBN# #  #
#  ##      #  #
#  #       #  #
#  #     W #  #
#  #      ##  #
#  #B XBN# #  #
#  ##      #  #
#  #       #  #
#  #       #  #
#  #      ##  #
#  #  XBIT #  #
#  #########  #
#             #
# ##### ##### #
# #     #     #
# #     #  ## #
# #     #   # #
# ##### ##### #
#             #
###############
`,
      `SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
EAST
NORTH
NORTH
NORTH
NORTH
WEST
WEST
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
NORTH
WEST
WEST
WEST
WEST
WEST
SOUTH
SOUTH
EAST
EAST
EAST
EAST
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
WEST
WEST
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
NORTH
WEST
WEST
WEST
WEST
WEST
SOUTH
SOUTH
EAST
EAST
EAST
EAST
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
NORTH
WEST
WEST
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
SOUTH
SOUTH
SOUTH
WEST
WEST
WEST
WEST
WEST
SOUTH
SOUTH
EAST
EAST
EAST
EAST
NORTH
NORTH
NORTH
NORTH
WEST
WEST
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
SOUTH
EAST
EAST
EAST
EAST
`,
    ))
})
