import { emptyMatrix } from '@lib/matrix/matrix'
import { Point } from '@lib/geometry/point'
import { isSame } from '@lib/geometry/distance'

export const init = () => {
  const [L, C] = readline().split(' ').map(Number)
  if (!C) return

  const mat = emptyMatrix(L, C)

  for (let i = 0; i < L; i++) {
    const row = readline()
    for (let j = 0; j < row.length; j++) {
      mat[i][j] = row.charAt(j)
    }
  }

  let inversedDir = false
  let currentDir = Dir.SOUTH
  let breakerMode = false
  let gotBlocked = false
  const output = new Array<string>()

  const move = (point: Point): void => {
    let p = point
    const suicide = find('$')
    let i = 0
    while (!isSame(p, suicide) && i++ < 1000) {
      p = goToNextPoint(p)
    }
  }

  const nextPoint = (point: Point): Point => {
    let x, y
    switch (currentDir) {
      case Dir.SOUTH:
        ;[x, y] = [1, 0]
        break
      case Dir.EAST:
        ;[x, y] = [0, 1]
        break
      case Dir.NORTH:
        ;[x, y] = [-1, 0]
        break
      case Dir.WEST:
        ;[x, y] = [0, -1]
        break
      default:
        throw new Error('FFs ' + currentDir)
    }
    const [x1, y1] = point
    return [x + x1, y + y1]
  }

  const goToNextPoint = (point: Point): Point => {
    const nextP = nextPoint(point)
    const [x, y] = nextP
    const value = mat[x][y]
    if (value != '#' && value != 'X') {
      gotBlocked = false
    }
    if (value === ' ' || value === '$' || value === '@') return goTo(nextP)
    if (value === 'B') {
      breakerMode = !breakerMode
      return goTo(nextP)
    } else if (value === 'T') {
      return goTo(findOtherTeleporter(nextP))
    } else if (value === 'I') {
      inversedDir = !inversedDir
      return goTo(nextP)
    } else if (value === 'S') {
      return goTo(nextP, Dir.SOUTH)
    } else if (value === 'E') {
      return goTo(nextP, Dir.EAST)
    } else if (value === 'N') {
      return goTo(nextP, Dir.NORTH)
    } else if (value === 'W') {
      return goTo(nextP, Dir.WEST)
    } else if (value === '#' || value === 'X') {
      if (breakerMode && value === 'X') {
        mat[x][y] = ' '
        return goTo(nextP)
      }
      if (!gotBlocked) {
        currentDir = inversedDir ? 3 : 0
      } else {
        currentDir += inversedDir ? -1 : 1
        // @ts-ignore: hack
        if (currentDir == -1) currentDir = 3
        // @ts-ignore: hack
        if (currentDir == 4) currentDir = 0
      }
      gotBlocked = true
      return goToNextPoint(point)
    } else {
      throw new Error('WTF: ' + value)
    }
  }

  const goTo = (point: Point, dir = currentDir): Point => {
    output.push(Dir[currentDir])
    currentDir = dir
    return point
  }

  const find = (val: string, predicate: (p: Point) => boolean = () => true): Point => {
    for (let i = 0; i < mat.length; i++) {
      for (let j = 0; j < mat[i].length; j++) {
        if (predicate([i, j]) && mat[i][j] === val) return [i, j]
      }
    }
    throw new Error('Cant find ' + val)
  }

  const findOtherTeleporter = (point: Point): Point => find('T', (p) => !isSame(p, point))

  move(find('@'))
  if (output.length > 999) {
    print('LOOP')
  } else {
    output.forEach((o) => print(o))
  }
}

enum Dir {
  SOUTH,
  EAST,
  NORTH,
  WEST,
}
