import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/bust-speeding-vehicles/codingame'

describe('BustSpeedingVehicles', () => {
  it(`Test de l'exemple`, () =>
    runTest(
      init,
      `50
3
RSLJ97 134 1447409503
RSLJ97 185 1447413099
RSLJ97 201 1447420298`,
      `RSLJ97 185`,
    ))

  it(`Pas d'excès de vitessse`, () =>
    runTest(
      init,
      `60
4
RSWJ98 152 1447416000
RSWJ98 199 1447419600
RSWJ98 247 1447423200
RSWJ98 295 1447426800`,
      `OK`,
    ))

  it(`Plusieurs voitures en excès de vitesse`, () =>
    runTest(
      init,
      `90
9
PAZD54 50 1447413071
PAZD54 150 1447416671
PAZD54 250 1447420211
DJSS87 50 1447408801
DJSS87 150 1447417501
DJSS87 250 1447421101
LSKD97 50 1447417436
LSKD97 150 1447424636
LSKD97 250 1447431836`,
      `PAZD54 150
PAZD54 250
DJSS87 250`,
    ))

  it(`Vitesse limite`, () =>
    runTest(
      init,
      `100
6
SKRD94 75 1447407932
SKRD94 175 1447411532
SKRD94 275 1447415132
ZBZJ42 75 1447418732
ZBZJ42 175 1447422333
ZBZJ42 275 1447425932`,
      `ZBZJ42 275`,
    ))
})
