import { Mapp } from '@lib/type/type'

export const init = () => {
  const L = +readline() / 3600
  const N = +readline()
  if (!N) return

  const speeds = [...Array(N)]
    .map(() => readline().split(' '))
    .reduce(
      (prev, current) => {
        const [plaque, dist, timestamp] = current
        if (prev[plaque] == undefined) prev[plaque] = []
        prev[plaque].push([+dist, +timestamp])
        return prev
      },
      {} as Mapp<number[][]>,
    )

  const output = new Array<string>()
  Object.keys(speeds).forEach((plaque) => {
    speeds[plaque].reduce((prev, current) => {
      if ((current[0] - prev[0]) / (current[1] - prev[1]) > L) {
        output.push(`${plaque} ${current[0]}`)
      }
      return current
    })
  })

  if (output.length === 0) {
    print('OK')
  } else {
    output.forEach((o) => print(o))
  }
}
