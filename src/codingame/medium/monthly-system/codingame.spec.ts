import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/monthly-system/codingame'

describe('MonthlySystem', () => {
  it(`Example`, () =>
    runTest(
      init,
      `2
FebSepDec
JunMarJan`,
      `JulNovDec`,
    ))

  it(`Multi Months`, () =>
    runTest(
      init,
      `4
Jan
Feb
Mar
Apr`,
      `Jul`,
    ))

  it(`Longer Months`, () =>
    runTest(
      init,
      `2
JanFebMarApr
MarFebAprJun`,
      `MarMarJunSep`,
    ))

  it(`Beware the leading zeros`, () =>
    runTest(
      init,
      `4
JanJanSepOctDecNovFeb
JulJunAugOctSep
JunJunMarJan
DecOctAugMay`,
      `FebMaySepNovJunFeb`,
    ))
})
