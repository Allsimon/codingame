// MonthlySystem
import { chunkSubstr } from '@lib/string/strings'
import { sum } from '@lib/array/arrays'
import { decimalToBase } from '@lib/number/numbers'

export const init = () => {
  const N = +readline()
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ]

  const compute = (value: string) =>
    sum(
      chunkSubstr(value, 3)
        .map((m) => months.indexOf(m))
        .reverse()
        .map((m, i) => m * 12 ** i),
    )

  const outputBase10 = sum([...Array(N)].map(() => readline()).map(compute))

  const outputBase12 = decimalToBase(outputBase10, 12)
    .map((v) => months[v])
    .reverse()
    .join('')

  print(outputBase12)
}
