export const init = () => {
  const lines: number[][] = [...Array(9)].map(() => readline().split(' ').map(Number))

  const isSame = (trips: number[]) => trips[0] === trips[1] && trips[1] === trips[2]

  const output = new Set<string>()

  const values = (x: number, y: number) => {
    if (!inRange(x, y)) return -1
    return lines[x][y]
  }

  const inRange = (...x: number[]) => x.every((n) => n >= 0 && n < 9)

  const check = (x: number, y: number): boolean => {
    const ffs: Array<number[]> = [[], [], [], [], [], []]
    for (let i = 0; i < 3; i++) {
      ffs[0].push(values(x + i, y))
      ffs[1].push(values(x - i, y))
      ffs[2].push(values(x, y + i))
      ffs[3].push(values(x, y - i))
      ffs[4].push(values(x, y - i + 1))
      ffs[5].push(values(x - i + 1, y))
    }
    return ffs.some(isSame)
  }

  const swap = (x1: number, y1: number, x2: number, y2: number) => {
    if (inRange(x1, y1, x2, y2)) {
      const old = lines[x1][y1]
      lines[x1][y1] = lines[x2][y2]
      lines[x2][y2] = old
    }
  }

  const moveAndCheck = (x: number, y: number) => {
    swap(x - 1, y, x, y)
    if (check(x - 1, y) && inRange(x - 1, y)) output.add([x - 1, y, x, y].join(' '))
    if (check(x, y) && inRange(x, y)) output.add([x - 1, y, x, y].join(' '))
    swap(x - 1, y, x, y)

    swap(x, y - 1, x, y)
    if (check(x, y) && inRange(x, y)) output.add([x, y - 1, x, y].join(' '))
    if (check(x, y - 1) && inRange(x, y - 1)) output.add([x, y - 1, x, y].join(' '))
    swap(x, y - 1, x, y)
  }

  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 9; j++) {
      moveAndCheck(i, j)
    }
  }

  print(output.size)
  ;[...output].sort().forEach((l) => print(l))
}
