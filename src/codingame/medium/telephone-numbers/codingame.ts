export const init = () => {
  const N = +readline()
  if (!N) return

  let output = 0
  const assign = (obj: any, values: string[]): any => {
    const key = values[0]
    const currentObj = obj[key] || {}
    if (Object.keys(currentObj).length === 0 && currentObj.constructor === Object) {
      output++
    }
    obj[key] = Object.assign(
      values.length === 1 ? { foo: 'bar' } : assign(currentObj, values.splice(1)),
      currentObj,
    )
    return obj
  }

  ;[...Array(N)]
    .map(() => readline())
    .sort()
    .map((e) => e.split(''))
    .reduce(assign, {})

  print(output)
}
