export const init = () => {
  const [R, L] = [+readline(), +readline()]

  const nextSequence = (numbers: number[]) => {
    let current = numbers[0]
    let count = 1
    let output = new Array<number>()
    for (let i = 1; i < numbers.length; i++) {
      if (numbers[i] === current) {
        count++
      } else {
        output.push(count, current)
        current = numbers[i]
        count = 1
      }
    }
    output.push(count, current)
    return output
  }

  let output = [R]
  for (let i = 1; i < L; i++) {
    output = nextSequence(output)
  }
  print(output.join(' '))
}
