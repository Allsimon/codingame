import { decimalToBase } from '@lib/number/numbers'
import { chunkSubstr } from '@lib/string/strings'

export const init = () => {
  const [L, H] = readline().split(' ').map(Number)
  if (!L) return

  const mayaParser = (value: number): string[] =>
    [...Array(value)]
      .map(() => chunkSubstr(readline(), L))
      .reduce((prev, cur) => {
        for (let i = 0; i < cur.length; i++) {
          prev[i] += '\n' + cur[i]
        }
        return prev
      })

  const toNumber = (linesToRead: number): number => {
    return mayaParser(linesToRead)
      .map((l) =>
        chunkSubstr(l, (L + 1) * H).map((l) => (l.endsWith('\n') ? l.slice(0, -1) : l)),
      )[0]
      .map((maya) => mayas.indexOf(maya))
      .reverse()
      .reduce((prev, current, i) => prev + current * Math.pow(20, i), 0)
  }

  const mayas = mayaParser(H)
  const S1 = toNumber(+readline())
  const S2 = toNumber(+readline())

  const result = eval(S1 + readline() + S2)
  decimalToBase(result, 20)
    .reverse()
    .forEach((m) => print(mayas[m]))
}
