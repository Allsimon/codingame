export const init = () => {
  const [a, b, c, d] = readline().split(' ').map(Number)
  const registers: any = { a: a, b: b, c: c, d: d }

  const n = +readline()
  if (!n) return

  const getValue = (srcImm: string, reg: any = registers) =>
    isNaN(+reg[srcImm]) ? +srcImm : +reg[srcImm]

  const instructions = [...Array(n)].map(() => readline())

  for (let i = 0; i < instructions.length; i++) {
    const [inst, dest, srcImm, srcImm2] = instructions[i].split(' ')
    if (inst === 'MOV') {
      registers[dest] = getValue(srcImm, registers)
    } else if (inst === 'ADD') {
      registers[dest] = getValue(srcImm) + getValue(srcImm2)
    } else if (inst === 'SUB') {
      registers[dest] = getValue(srcImm) - getValue(srcImm2)
    } else if (inst === 'JNE') {
      if (getValue(srcImm) !== getValue(srcImm2)) {
        i = +dest - 1
      }
    }
  }
  print(Object.values(registers).join(' '))
}
