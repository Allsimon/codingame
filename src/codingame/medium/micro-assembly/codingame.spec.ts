import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/micro-assembly/codingame'

describe('MicroAssembly', () => {
  it(`MOV test`, () =>
    runTest(
      init,
      `1 2 3 -4
2
MOV b 3
MOV c a`,
      `1 3 1 -4`,
    ))

  it(`ADD test`, () =>
    runTest(
      init,
      `2 3 4 5
3
ADD a b 1
ADD b 2 7
ADD c a b`,
      `4 9 13 5`,
    ))

  it(`SUB test`, () =>
    runTest(
      init,
      `14 2 21 9
3
SUB a a a
SUB d 12 a
SUB b 15 3`,
      `0 12 21 12`,
    ))

  it(`JNE test`, () =>
    runTest(
      init,
      `3 5 7 9
2
SUB b b 1
JNE 0 b 0`,
      `3 0 7 9`,
    ))

  it(`Handling negative values`, () =>
    runTest(
      init,
      `0 -2 -3 -4
4
MOV a -1
SUB b c -3
ADD d d -1
JNE 2 d -10`,
      `-1 0 -3 -10`,
    ))

  it(`Calculating sum of 1..N`, () =>
    runTest(
      init,
      `0 10 0 0
3
ADD a a b
SUB b b 1
JNE 0 b 0`,
      `55 0 0 0`,
    ))

  it(`Nested loops`, () =>
    runTest(
      init,
      `1 3 3 7
9
MOV a 10
MOV b 5
MOV c b
SUB c c 1
ADD a a c
JNE 3 c 0
SUB b b 1
JNE 2 b c
SUB d 0 d`,
      `30 0 0 -7`,
    ))

  it(`Multiplication and jump over instruction`, () =>
    runTest(
      init,
      `0 7 5 3
13
ADD a a b
SUB c c 1
JNE 0 c 0
MOV b a
SUB c d 1
ADD a a b
SUB c c 1
JNE 5 c 0
SUB b 0 d
JNE 11 a -105
MOV a 0
ADD d d b
SUB b b b`,
      `105 0 0 0`,
    ))
})
