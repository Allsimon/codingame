// VoteCounting
import { Mapp } from '@lib/type/type'

export const init = () => {
  const N = +readline()
  const M = +readline()

  const voters: Mapp<number> = [...Array(N)]
    .map(() => readline().split(' '))
    .reduce((prev, current) => {
      const [name, vote] = current
      return Object.assign(prev, { [name]: +vote })
    }, {})

  const votes = [...Array(M)]
    .map(() => readline().split(' '))
    .reduce((prev, current) => {
      let [name, value] = current
      if (!prev[name]) {
        prev[name] = { Yes: 0, No: 0, Invalid: 0 }
      }
      if (value !== 'Yes' && value !== 'No') value = 'Invalid'
      // @ts-ignore
      prev[name][value] = prev[name][value] + 1

      return prev
    }, {} as Mapp<Vote>)

  let yes = 0
  let no = 0
  Object.keys(voters).forEach((key) => {
    const currentVote: Vote = votes[key]
    if (currentVote) {
      if (currentVote.Yes + currentVote.No + currentVote.Invalid <= voters[key]) {
        yes += currentVote.Yes
        no += currentVote.No
      }
    }
  })

  print(`${yes} ${no}`)
}

interface Vote {
  Yes: number
  No: number
  Invalid: number
}
