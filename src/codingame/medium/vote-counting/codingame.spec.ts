import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/vote-counting/codingame'

describe('VoteCounting', () => {
  it(`Exemple`, () =>
    runTest(
      init,
      `2
3
Albert 2
Roger 1
Albert Yes
Roger No
Albert Yes`,
      `2 1`,
    ))

  it(`Votant invalide`, () =>
    runTest(
      init,
      `1
4
Albert 3
CodinGame Yes
Albert Yes
Albert No
Albert No`,
      `1 2`,
    ))

  it(`Trop de votes`, () =>
    runTest(
      init,
      `3
6
Yohann 1
Sandra 3
Capucine 1
Capucine Yes
Sandra No
Yohann Yes
Sandra No
Yohann Yes
Sandra No`,
      `1 3`,
    ))

  it(`Vote maybe`, () =>
    runTest(
      init,
      `2
2
Kevin 1
Charlotte 1
Kevin Maybe
Charlotte Yes`,
      `1 0`,
    ))

  it(`Complexe`, () =>
    runTest(
      init,
      `4
9
Albert 2
Roger 1
Thierry 1
Valérie 3
Albert Yes
Valérie Yes
Erwan No
Albert No
Thierry Yes
Valérie Yes
Valérie No
Valérie Non
Roger Maybe`,
      `2 1`,
    ))
})
