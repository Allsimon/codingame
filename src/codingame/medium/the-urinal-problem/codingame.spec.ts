import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/the-urinal-problem/codingame'

describe('TheUrinalProblem', () => {
  it(`Small Bathroom`, () =>
    runTest(
      init,
      `3
UU!`,
      `0`,
    ))

  it(`School Bathroom`, () =>
    runTest(
      init,
      `5
!UUU!`,
      `2`,
    ))

  it(`Sporting Event`, () =>
    runTest(
      init,
      `32
!!U!!U!U!!UUUU!UU!!UUUUUUUUU!!!!`,
      `23`,
    ))

  it(`Mostly Empty`, () =>
    runTest(
      init,
      `20
UUUUUUUUU!UUUUUUUUUU`,
      `19`,
    ))
})
