// TheUrinalProblem
export const init = () => {
  const N = +readline()
  const urinals = readline()
    .split('')
    .map((urinal) => urinal === 'U')

  const ur = []
  for (let i = 0; i < urinals.length; i++) {
    if (urinals[i]) {
      let j = i + 1
      for (; j < urinals.length; j++) {
        if (!urinals[j]) break
      }
      if (i === 0) i--
      if (j === urinals.length) j++
      ur.push({ start: i, size: j - i, border: i === -1 || j === urinals.length + 1 })
      i = j
    }
  }
  const urinal = ur.sort((a, b) => b.size - a.size)[0]

  if (urinal.border) {
    print(urinal.start === -1 ? 0 : urinals.length - 1)
  } else {
    print(urinal.start + Math.floor(urinal.size / 2))
  }
}
