// FindTheReplacement
import { Mapp } from '@lib/type/type'

export const init = () => {
  const X = readline()
  const Y = readline()

  const checkReplacement = () => {
    const found: Mapp<string> = {}
    for (let i = 0; i < X.length; i++) {
      const x = X[i]
      const y = Y[i]
      if (found[x] !== undefined) {
        if (found[x] !== y) throw new Error('')
      }
      found[x] = y
    }
    return found
  }

  if (X === Y) {
    print('NONE')
  } else {
    try {
      const replace = checkReplacement()
      Object.keys(replace)
        .filter((key) => replace[key] !== key)
        .forEach((key) => print(`${key}->${replace[key]}`))
    } catch (e) {
      print("CAN'T")
    }
  }
}
