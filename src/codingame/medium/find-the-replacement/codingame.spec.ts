import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/find-the-replacement/codingame'

describe('FindTheReplacement', () => {
  it(`Easy replacement`, () =>
    runTest(
      init,
      `BARTENDER
CARBENDER`,
      `B->C
T->B`,
    ))

  it(`No replacement`, () =>
    runTest(
      init,
      `Statement
Statement`,
      `NONE`,
    ))

  it(`CAN'T (hard)`, () =>
    runTest(
      init,
      `TARGET
THRONE`,
      `CAN'T`,
    ))

  it(`Symbols`, () =>
    runTest(
      init,
      `!_@...@@..@@@!$$#@@$$@._####!##...!!!@@@!!!@@$$....@@!!!$$$$!!##@!$$#!@@.###
!_$###$$##$$$!$$#$$$$$#_####!#####!!!$$$!!!$$$$####$$!!!$$$$!!##$!$$#!$$####`,
      `@->$
.->#`,
    ))

  it(`Random, all different`, () =>
    runTest(
      init,
      `i pdnwspak aeeomaegant ganwua ediap ons
yzzdhfkzfdzfjjfkfjkfhzzkfhfzfzjdyfzzfhk`,
      `i->y
 ->z
p->z
n->h
w->f
s->k
a->f
k->d
e->j
o->f
m->k
g->k
t->z
u->z`,
    ))

  it(`Symbols reversed`, () =>
    runTest(
      init,
      `!_$###$$##$$$!$$#$$$$$#_####!#####!!!$$$!!!$$$$####$$!!!$$$$!!##$!$$#!$$####
!_@...@@..@@@!$$#@@$$@._####!##...!!!@@@!!!@@$$....@@!!!$$$$!!##@!$$#!@@.###`,
      `CAN'T`,
    ))
})
