// MagicSquare
import { sum } from '@lib/array/arrays'
import { diagonals, flatten, transpose } from '@lib/matrix/matrix'

export const init = () => {
  const matrix = [...Array(+readline())].map(() => readline().split(' ').map(Number))

  const magicNumber = sum(matrix[0])

  const checkSumEquals = (value: number, ...matrices: number[][][]) =>
    matrices.every((matrix) => matrix.every((row) => sum(row) === value))

  const flat = flatten(matrix)
  const isMagic =
    flat.every((l) => l >= 0) &&
    flat.length === new Set(flat).size &&
    checkSumEquals(magicNumber, matrix, transpose(matrix), diagonals(matrix))
  print(isMagic ? 'MAGIC' : 'MUGGLE')
}
