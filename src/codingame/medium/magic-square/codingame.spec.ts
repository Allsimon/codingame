import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/magic-square/codingame'

describe('MagicSquare', () => {
  it(`Test 1`, () =>
    runTest(
      init,
      `3
2 7 6
9 5 1
4 3 8`,
      `MAGIC`,
    ))

  it(`Test 2`, () =>
    runTest(
      init,
      `1
1`,
      `MAGIC`,
    ))

  it(`Test 3`, () =>
    runTest(
      init,
      `4
1 15 14 4
12 6 7 9
8 10 11 5
13 3 2 16`,
      `MAGIC`,
    ))

  it(`Test 4`, () =>
    runTest(
      init,
      `3
4 9 2
3 5 -7
8 1 6`,
      `MUGGLE`,
    ))

  it(`Test 5`, () =>
    runTest(
      init,
      `3
1 9 5
6 2 7
8 4 3`,
      `MUGGLE`,
    ))

  it(`Test 6`, () =>
    runTest(
      init,
      `3
5 9 1
3 4 8
7 2 6`,
      `MUGGLE`,
    ))

  it(`Test 7`, () =>
    runTest(
      init,
      `7
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 1 1 1`,
      `MUGGLE`,
    ))
})
