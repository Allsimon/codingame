import { init } from '@codingame/medium/dwarfs-standing-on-the-shoulders-of-giants/codingame'
import { runTest } from '@codingame/test'

describe('DwarfsStandingOnTheShouldersOfGiants', () => {
  it(`Exemple simple`, () =>
    runTest(
      init,
      `3
1 2
1 3
3 4
`,
      `3
`,
    ))

  it(`Exemple complet`, () =>
    runTest(
      init,
      `8
1 2
1 3
3 4
2 4
2 5
10 11
10 1
10 3
`,
      `4
`,
    ))

  it(`Plusieurs courants`, () =>
    runTest(
      init,
      `4
2 3
8 9
1 2
6 3
`,
      `3
`,
    ))
})
