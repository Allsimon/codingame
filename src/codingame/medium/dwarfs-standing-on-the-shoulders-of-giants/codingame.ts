import { Mapp } from '@lib/type/type'

export const init = () => {
  const ffs: Mapp<number[]> = {}
  const length: Mapp<number> = {}
  ;[...Array(+readline())]
    .map(readline)
    .map((l) => l.split(' ').map(Number))
    .map((l) => {
      const [x, y] = l
      if (ffs[x] == undefined) ffs[x] = []
      if (ffs[y] == undefined) ffs[y] = []
      ffs[y].push(x)
    })

  const assignLength = (key: string | number, total = 1) => {
    if ((length[key] || 0) < total) length[key] = total
    ffs[key].forEach((newKey) => assignLength(newKey, total + 1))
  }

  Object.keys(ffs).forEach((key) => assignLength(key))
  print(Math.max(...Object.values(length)))
}
