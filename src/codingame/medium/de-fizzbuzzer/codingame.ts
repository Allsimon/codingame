import { Mapp } from '@lib/type/type'

export const init = () => {
  const N = +readline()
  if (N === undefined) return
  const lines = [...Array(N)].map(() => readline())

  const fizzBuzz: Mapp<number> = {}

  const nbrOfDiv = (value: number, div: number) => {
    let i = 0
    while (Number.isInteger((value = value / div))) i++

    return i
  }

  for (let i = 1; i <= 1000; i++) {
    let output = ''
    const nbrOf3 = (('' + i).match(/3/g) || []).length
    const nbrOfDivBy3 = nbrOfDiv(i, 3)
    output += 'Fizz'.repeat(nbrOf3 + nbrOfDivBy3)
    const nbrOf5 = (('' + i).match(/5/g) || []).length
    const nbrOfDivBy5 = nbrOfDiv(i, 5)
    output += 'Buzz'.repeat(nbrOf5 + nbrOfDivBy5)
    if (output.length === 0) output = '' + i

    fizzBuzz[output] = fizzBuzz[output] || i
  }
  lines.forEach((line) => print(fizzBuzz[line] || 'ERROR'))
}
