// KaprekarsRoutine
export const init = () => {
  const N = readline()

  const D = (value: string) =>
    +('' + value)
      .split('')
      .sort((a, b) => +b - +a)
      .join('')
  const A = (value: string) =>
    +('' + value)
      .split('')
      .sort((a, b) => +a - +b)
      .join('')
  const kaprekar = (value: string) => ('' + (D(value) - A(value))).padStart(value.length, '0')

  const findKaprekarLoop = () => {
    const alrdyFound = new Array<string>()
    let kap: string = N
    while (!alrdyFound.includes(kap)) {
      alrdyFound.push(kap)
      kap = kaprekar(kap)
    }
    return alrdyFound.slice(alrdyFound.indexOf(kap))
  }

  print(findKaprekarLoop().join(' '))
}
