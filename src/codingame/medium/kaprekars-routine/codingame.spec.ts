import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/kaprekars-routine/codingame'

describe('KaprekarsRoutine', () => {
  it(`length 1 cycle`, () => runTest(init, `2178`, `6174`))

  it(`dont forget padding`, () => runTest(init, `09`, `09 81 63 27 45`))

  it(`number of the beast`, () => runTest(init, `666`, `000`))

  it(`big`, () =>
    runTest(init, `43208766`, `43208766 85317642 75308643 84308652 86308632 86326632 64326654`))
})
