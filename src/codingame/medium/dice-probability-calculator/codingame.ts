export const init = () => {
  const N = readline()
  if (!N) return

  const dFound = N.match(/d\d+/g) || []
  const dValues = dFound.map((d) => +d.slice(1))
  const dFactor = [...Array(dValues.length)].map((v, i) =>
    dValues.slice(0, i).reduce((prev, current) => current * prev, 1),
  )

  const maxLaunch = dValues.reduce((prev, current) => prev * current, 1)
  const exp = [...Array(maxLaunch)].map(() => N)

  for (let i = 0; i < maxLaunch; i++) {
    for (let j = 0; j < dValues.length; j++) {
      const value = '' + ((Math.floor(i / dFactor[j]) % dValues[j]) + 1)
      exp[i] = exp[i].replace(dFound[j], value)
    }
  }

  const foo = exp
    .map(eval)
    .map(Number)
    .reduce(
      (prev, current) => Object.assign(prev, { [current]: (prev[current] || 0) + 1 }),
      {} as any,
    )

  Object.keys(foo)
    .sort((a, b) => +a - +b)
    .map((key) => +key + ' ' + ((foo[key] * 100) / maxLaunch).toFixed(2))
    .forEach((o) => print(o))
}
