import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/dice-probability-calculator/codingame'

describe('DiceProbabilityCalculator', () => {
  it(`A die and two additions`, () =>
    runTest(
      init,
      `1+d4+1`,
      `3 25.00
4 25.00
5 25.00
6 25.00`,
    ))

  it(`Simple Comparison`, () => runTest(init, `(2>5)+2*(5>2)+4*(10>5)`, `6 100.00`))

  it(`Addition of two dice`, () =>
    runTest(
      init,
      `d6+d6`,
      `2 2.78
3 5.56
4 8.33
5 11.11
6 13.89
7 16.67
8 13.89
9 11.11
10 8.33
11 5.56
12 2.78`,
    ))

  it(`Comparaison with sum of dice`, () =>
    runTest(
      init,
      `8>d6+d6`,
      `0 41.67
1 58.33`,
    ))

  it(`Subtraction of dice`, () =>
    runTest(
      init,
      `d9-2*d4`,
      `-7 2.78
-6 2.78
-5 5.56
-4 5.56
-3 8.33
-2 8.33
-1 11.11
0 11.11
1 11.11
2 8.33
3 8.33
4 5.56
5 5.56
6 2.78
7 2.78`,
    ))

  it(`Multiplication of dice`, () =>
    runTest(
      init,
      `12-d2*d6`,
      `0 8.33
2 8.33
4 8.33
6 16.67
7 8.33
8 16.67
9 8.33
10 16.67
11 8.33`,
    ))

  it(`FIX IT !`, () =>
    runTest(
      init,
      `2*d6*d3-(3+d3)*d5>0`,
      `0 59.63
1 40.37`,
    ))
})
