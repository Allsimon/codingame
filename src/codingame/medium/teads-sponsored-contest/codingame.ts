import { Mapp } from '@lib/type/type'

export const init = () => {
  const n = +readline() // the number of adjacency relations
  if (!n) return
  const inverted: Mapp<string[]> = {}
  const degree: Mapp<number> = {}

  const origin = [...Array(n)]
    .map(() => readline().split(' '))
    .reduce(
      (prev, current) => {
        const [a, b] = current
        inverted[b] = inverted[b] || []
        prev[a] = prev[a] || []
        prev[a].push(b)
        inverted[b].push(a)
        degree[b] = (degree[b] || 0) + 1
        degree[a] = (degree[a] || 0) + 1

        return prev
      },
      {} as Mapp<string[]>,
    )

  const burnLeaf = (
    g1: Mapp<string[]>,
    g2: Mapp<string[]>,
    degree: Mapp<number>,
    length = 0,
  ): number => {
    const keys = Object.keys(degree)
    if (keys.length <= 1) return length

    const leaves = keys.filter((key) => degree[key] === 1)
    leaves.forEach((l) => {
      // remove leaves that were holding a link to our leave
      removeVertex(g1, l)
      removeVertex(g2, l)
      delete degree[l]
    })
    return burnLeaf(g1, g2, degree, length + 1)
  }

  const removeVertex = (graph: Mapp<string[]>, vertex: string) => {
    if (graph[vertex] !== undefined) {
      graph[vertex].forEach((upperLeave) => {
        if (degree[upperLeave] !== undefined) {
          degree[upperLeave] = degree[upperLeave] - 1
        }
      })
    }
  }

  print(burnLeaf(origin, inverted, degree))
}
