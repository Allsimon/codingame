// CgsMinifier

import { escapeRegExp } from '@lib/string/strings'

export const init = () => {
  const N = +readline()
  let lines = [...Array(N)].map(() => readline()).join('\n')

  const strings = lines.match(/'.*'/g)

  lines = lines.replace(/\s/g, '')

  const matches = [...new Set(lines.match(/\$\w+\$/g))]
  const letters = 'abcdefghijklmnopqrstuvwxyz'.split('')
  matches
    .map(escapeRegExp)
    .map((regex) => new RegExp(regex, 'g'))
    .forEach((match) => (lines = lines.replace(match, `$${letters.shift()}$` || 'ffs')))

  if (strings) strings.forEach((str) => (lines = lines.replace(str.replace(/\s/g, ''), str)))

  print(lines)
}
