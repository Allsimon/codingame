import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/cgs-minifier/codingame'

describe('CgsMinifier', () => {
  it(`Exemple`, () =>
    runTest(
      init,
      `4
(
$hello$ = 'hello';
print $hello$;
)
`,
      `($a$='hello';print$a$;)
`,
    ))

  it(`1`, () =>
    runTest(
      init,
      `3
(     
          'hello'
 )
`,
      `('hello')
`,
    ))

  it(`2`, () =>
    runTest(
      init,
      `1
print 'SomeSpaces  and    tabs';
`,
      `print'SomeSpaces  and    tabs';
`,
    ))

  it(`3`, () =>
    runTest(
      init,
      `5
$some_text$;
(
$some_text$ = 'I like trains';
)
print $some_text$;
`,
      `$a$;($a$='I like trains';)print$a$;
`,
    ))

  it(`4`, () =>
    runTest(
      init,
      `6
$int_1$=42;
$int_2$=1337;
$tmp$=$int_1$;
$int_1$=$int_2$;
$int_2$=$tmp$;
print 'I just switched values yippee';
`,
      `$a$=42;$b$=1337;$c$=$a$;$a$=$b$;$b$=$c$;print'I just switched values yippee';
`,
    ))
})
