import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/snake-encoding/codingame'

describe('SnakeEncoding', () => {
  it(`Easy Peasy`, () =>
    runTest(
      init,
      `3
1
ABC
DEF
GHI`,
      `DAF
GBI
CEH`,
    ))

  it(`More rows`, () =>
    runTest(
      init,
      `5
1
ABCDE
FGHIJ
KLMNO
PQRST
UVWXY`,
      `FAHCJ
KBMDO
PGRIT
ULWNY
EQVSX`,
    ))

  it(`More loops`, () =>
    runTest(
      init,
      `7
5
I_LOVE_
TESTING
AMAZING
ENCODES
NOTYOU?
NUM83R5
NICE:)!`,
      `NNMT3O5
NECC:D!
_AIAEI)
GTUS8IR
GIOLYVU
S_NOOEE
?EMTZNN`,
    ))

  it(`Let's try everything`, () =>
    runTest(
      init,
      `10
38
PN_OOEITH_
_THYSNLIKM
LRNIEGC'A_
_CVDR!IO'E
!E!Y!U9_0O
OT_IO_RINE
>EAGE!BHEL
NO_E__ITWS
A!T!A!Z0_0
RMWRTWRAED`,
      `I'M_TRYING
TO_ENCODE!
I_HOPE_YOU
LIKE_THIS_
CHALLENGE!
IT'S_OVER_
9000!!!!!!
RANDOM_ROW
BEER>WATER
I_WANT_A_Z`,
    ))
})
