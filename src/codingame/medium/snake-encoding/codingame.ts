import { printMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const N = +readline()
  const X = +readline()
  if (N === undefined) return
  const lines = [...Array(N)].map(() => readline().split(''))
  const H = lines.length
  const W = (lines[0] || []).length

  let flattened = new Array<string>()

  const snakeDo = (fun: (x: number, y: number) => void) => {
    for (let i = 0; i < W; i++)
      if (i % 2 === 0) for (let j = H - 1; j >= 0; j--) fun(i, j)
      else for (let j = 0; j < H; j++) fun(i, j)
  }
  snakeDo((i, j) => flattened.push(lines[j][i]))
  flattened = flattened.slice(-X).concat(flattened.slice(0, -X))
  snakeDo((i, j) => (lines[j][i] = flattened.shift() || '$'))

  print(printMatrix(lines, (t) => t).trimRight())
}
