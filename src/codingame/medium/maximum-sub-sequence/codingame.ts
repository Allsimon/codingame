export const init = () => {
  const N = +readline()
  if (N === undefined) return
  const lines = readline().split(' ').map(Number)

  let seq: number[][] = []
  for (let i = 0; i < lines.length; i++) {
    seq.push(
      lines.slice(i).reduce((prev, current) => {
        if (prev.length === 0) {
          prev.push(current)
        } else {
          if (prev[prev.length - 1] + 1 === current) {
            prev.push(current)
          }
        }
        return prev
      }, [] as number[]),
    )
  }

  const output = seq.sort((a, b) => {
    const diff = b.length - a.length
    if (a.length === b.length) {
      return a[0] - b[0]
    }
    return diff
  })[0]
  print(output.join(' '))
}
