export const init = () => {
  const value = ['2', '3', '4', '5', '6', '7', '8', '9', '1', 'J', 'Q', 'K', 'A']
  const n = +readline()
  if (!n) return
  const card1 = [...Array(n)].map(() => readline())
  const m = +readline()
  const card2 = [...Array(m)].map(() => readline())

  const match = (d1: Deck, d2: Deck, t1: Deck, t2: Deck, manche: number): string => {
    if (d1.length === 0) {
      return '2 ' + manche
    } else if (d2.length === 0) {
      return '1 ' + manche
    }
    const p1 = d1.splice(0, 1)[0]
    const p2 = d2.splice(0, 1)[0]
    t1.push(p1)
    t2.push(p2)

    if (score(p1) === score(p2)) {
      return bataille(d1, d2, t1, t2, manche)
    } else if (score(p1) < score(p2)) {
      d2.push(...t1)
      d2.push(...t2)
      return match(d1, d2, [], [], manche + 1)
    } else {
      d1.push(...t1)
      d1.push(...t2)
      return match(d1, d2, [], [], manche + 1)
    }
  }

  const score = (card: string) => value.indexOf(card.charAt(0))

  const bataille = (d1: Deck, d2: Deck, t1: Deck, t2: Deck, manche: number) => {
    const trashes = [d1, d2].map((d) => d.splice(0, 3))
    if (trashes.every((t) => t.length === 3)) {
      t1.push(...trashes[0])
      t2.push(...trashes[1])
      return match(d1, d2, t1, t2, manche)
    } else {
      throw new Error('ffs')
    }
  }

  try {
    print(match(card1, card2, [], [], 0))
  } catch (e) {
    print('PAT')
  }
}

type Deck = string[]
