export const init = () => {
  const result = +readline()
  if (!result) return
  const numbers = readline().split(' ').map(Number).sort()
  const BIG_NUMBER = 10
  let minPossibility = BIG_NUMBER
  let minDistance = Infinity

  const bruteForce = (nbrs = numbers) => {
    const currentLength = 6 - nbrs.length
    if (currentLength >= minPossibility) return
    if (nbrs.includes(result)) {
      minPossibility = currentLength
      return
    }
    for (let i = 0; i < nbrs.length; i++) {
      for (let j = 0; j < nbrs.length; j++) {
        if (i !== j) {
          bruteForce(subArray(i, j, nbrs, (a, b) => a - b, subPossible))
          bruteForce(subArray(i, j, nbrs, (a, b) => a / b, divPossible))
          bruteForce(subArray(i, j, nbrs, (a, b) => a * b))
          bruteForce(subArray(i, j, nbrs, (a, b) => a + b))
        }
      }
    }
  }

  const subArray = (
    i: number,
    j: number,
    nbrs: number[],
    computation: (a: number, b: number) => number,
    predicate: (a: number) => boolean = () => true,
  ): number[] => {
    const a = nbrs[i]
    const b = nbrs[j]

    const res = computation(a, b)
    const dist = Math.abs(result - res)

    if (dist < minDistance) minDistance = dist
    if (res === a || res === b) return []
    if (!predicate(res)) return []
    return cloneAndRemove(nbrs, i, j, res)
  }

  const cloneAndRemove = (nbrs: number[], i: number, j: number, res: number): number[] => {
    const output = []
    for (let k = 0; k < nbrs.length; k++) {
      if (k === i) {
        output.push(res)
      } else if (k === j) {
      } else {
        output.push(nbrs[k])
      }
    }
    return output
  }

  const subPossible = (a: number) => a > 0
  const divPossible = (a: number) => Math.floor(a) === a

  bruteForce(numbers)
  if (minPossibility !== BIG_NUMBER) {
    print('POSSIBLE')
    print(minPossibility)
  } else {
    print('IMPOSSIBLE')
    print(minDistance)
  }
}
