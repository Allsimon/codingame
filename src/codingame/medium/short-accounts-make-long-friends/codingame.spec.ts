import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/short-accounts-make-long-friends/codingame'

describe('ShortAccountsMakeLongFriends', () => {
  it(`Very simple`, () =>
    runTest(
      init,
      `101
100 5 2 3 4 1`,
      `POSSIBLE
1`,
    ))

  it(`Bad Bad Bad`, () =>
    runTest(
      init,
      `999
1 2 2 3 3 1`,
      `IMPOSSIBLE
918`,
    ))

  it(`425`, () =>
    runTest(
      init,
      `425
2 50 25 5 9 2`,
      `POSSIBLE
2`,
    ))

  it(`889`, () =>
    runTest(
      init,
      `889
3 7 8 10 4 6`,
      `POSSIBLE
5`,
    ))

  it(`A division ?`, () =>
    runTest(
      init,
      `869
2 3 4 2 50 6`,
      `IMPOSSIBLE
1`,
    ))

  it(`Another Division`, () =>
    runTest(
      init,
      `651
8 75 25 50 5 10`,
      `POSSIBLE
5`,
    ))
})
