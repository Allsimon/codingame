import { sum } from '@lib/array/arrays'

export const init = () => {
  const N = +readline()
  const Y = +readline()
  if (N === undefined) return
  let cages = [...Array(N)].map(() => readline().split(' ').map(Number))

  const alive = () => sum(cages.map((cage) => cage[2]))

  for (let i = 0; i < Y; i++) {
    cages = cages.map((cage) => {
      const [S, H, A] = cage
      return [2 * S, H - 2 * S, H <= 0 ? 0 : H]
    })
    const left = alive()
    print(left)
    if (left === 0) break
  }
}
