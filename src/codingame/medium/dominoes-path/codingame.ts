// DominoesPath
export const init = () => {
  const N = +readline()
  const dominoes = [...Array(N)].map(() => readline().split(' ').map(Number))

  const popDomino = (domino: number[], dominoes: number[][]): boolean => {
    if (dominoes.length === 0) return true
    const [a1, b1] = domino
    let found
    for (let i = 0; i < dominoes.length; i++) {
      const [a2, b2] = dominoes[i]
      if (b1 === a2) {
        found = [a2, b2]
        dominoes.splice(i, 1)
        break
      } else if (b1 === b2) {
        found = [b2, a2]
        dominoes.splice(i, 1)
        break
      }
    }
    if (!found) return false

    return popDomino(found, dominoes)
  }

  const first = dominoes.shift()
  if (first) print(popDomino(first, dominoes))
}
