import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/dominoes-path/codingame'

describe('DominoesPath', () => {
  it(`Exemple`, () =>
    runTest(
      init,
      `2
1 2
2 3`,
      `true`,
    ))

  it(`Pas de chemin (simple)`, () =>
    runTest(
      init,
      `2
0 0
1 1`,
      `false`,
    ))

  it(`Chemin long dans l'ordre`, () =>
    runTest(
      init,
      `5
0 1
1 2
2 3
3 4
4 5`,
      `true`,
    ))

  it(`Chemin brisé`, () =>
    runTest(
      init,
      `5
0 1
1 2
2 3
3 4
5 6`,
      `false`,
    ))

  it(`Long chemin mélangé`, () =>
    runTest(
      init,
      `20
0 1
2 3
5 6
1 2
6 4
5 0
3 4
4 2
2 1
4 5
1 3
3 5
0 5
5 2
0 3
3 3
2 6
6 1
1 4
4 0`,
      `true`,
    ))

  it(`Longue séquence invalide`, () =>
    runTest(
      init,
      `8
0 1
1 2
2 3
3 4
4 5
5 6
6 1
2 4`,
      `false`,
    ))
})
