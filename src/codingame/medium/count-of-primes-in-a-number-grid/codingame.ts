import { isPrime } from '@lib/number/numbers'

export const init = () => {
  const [R, C] = readline().split(' ').map(Number)
  if (R === undefined) return

  const matrix = [...Array(R)].map(() => readline().split(' ').map(Number))

  const output = new Set()

  const check = (
    func: (i: number, j: number) => number,
    maxX: number,
    maxY: number,
    start: number,
  ) => {
    for (let i = 0; i < maxX; i++) {
      let foo = ''
      for (let j = start; j < maxY; j++) {
        foo += '' + func(i, j)
        if (isPrime(+foo)) output.add(+foo)
      }
    }
  }

  for (let k = 0; k < C; k++) {
    check((i, j) => matrix[i][j], R, C, k)
  }
  for (let k = 0; k < R; k++) {
    check((i, j) => matrix[j][i], C, R, k)
  }
  print(output.size)
}
