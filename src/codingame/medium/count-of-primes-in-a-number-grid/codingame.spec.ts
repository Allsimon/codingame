import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/count-of-primes-in-a-number-grid/codingame'

describe('CountOfPrimesInANumberGrid', () => {
  it(`Test 1`, () =>
    runTest(
      init,
      `2 2
2 3
1 7`,
      `6`,
    ))

  it(`Much ado about nothing`, () =>
    runTest(
      init,
      `4 4
0 0 0 0
0 0 0 0
0 2 0 0
0 0 0 0`,
      `1`,
    ))

  it(`Gordon Lee puzzle`, () =>
    runTest(
      init,
      `5 5
3 8 2 1 9
3 3 7 9 7
1 9 4 6 9
9 1 5 7 1
9 1 7 3 9`,
      `50`,
    ))

  it(`Large`, () =>
    runTest(
      init,
      `6 6
3 1 7 3 3 3
9 9 5 6 3 9
1 1 8 1 4 2
1 3 6 3 7 3 
3 4 9 1 9 9
3 7 9 3 7 9`,
      `78`,
    ))

  it(`Asymmetric`, () =>
    runTest(
      init,
      `5 7
1 9 3 1 3 3 3
9 9 4 1 9 9 9
1 3 6 9 7 9 1
3 7 7 5 7 2 7
9 6 2 3 8 3 3`,
      `64`,
    ))
})
