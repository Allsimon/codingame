export const init = () => {
  const N = +readline()
  if (!N) return
  const net = [...Array(N)].map(() => readline().split(' ').map(Number))

  const nY = net.map((e) => e[1]).sort((a, b) => a - b)
  const median = nY[Math.floor(nY.length / 2)]
  const nX = net.map((e) => e[0])
  const lengthX = Math.max(...nX) - Math.min(...nX)

  const lengthY = nY.reduce((prev, current) => prev + Math.abs(median - current), 0)

  print(lengthX + lengthY)
}
