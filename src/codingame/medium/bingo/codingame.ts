import { replaceInMatrix } from '@lib/matrix/matrix'

export const init = () => {
  const n = +readline()
  const SIZE = 5
  if (!n) return
  const cards = [...Array(n)].map(() => [...Array(5)].map(() => readline().split(' ').map(Number)))

  const checkNumber = (value: number) =>
    cards.forEach((matrix) =>
      replaceInMatrix(
        matrix,
        (t) => t === value,
        () => 0,
      ),
    )

  const checkLines = () => {
    return cards
      .map((matrix) => {
        const diagonal1 = [...Array(SIZE)].map((v, i) => matrix[i][i]).every((v) => v === 0)
        const diagonal2 = [...Array(SIZE)].map((v, i) => matrix[4 - i][i]).every((v) => v === 0)
        let gotLine1 = false
        let gotLine2 = false
        for (let i = 0; i < SIZE; i++) {
          gotLine1 = gotLine1 || [...Array(SIZE)].map((v, j) => matrix[i][j]).every((v) => v === 0)
          gotLine2 = gotLine2 || [...Array(SIZE)].map((v, j) => matrix[j][i]).every((v) => v === 0)
        }
        return diagonal1 || diagonal2 || gotLine1 || gotLine2
      })
      .some((t) => t === true)
  }

  const sum = (a: number, b: number) => a + b
  const checkBingo = () =>
    cards.map((matrix) => matrix.map((line) => line.reduce(sum)).reduce(sum)).some((a) => a === 0)

  let foundLines = false
  let foundBingo = false

  readline()
    .split(' ')
    .map(Number)
    .forEach((v, i) => {
      checkNumber(v)
      if (!foundLines && checkLines()) {
        foundLines = true
        print(i + 1)
      }
      if (foundLines && !foundBingo) {
        if (checkBingo()) {
          foundBingo = true
          print(i + 1)
          return
        }
      }
    })
}
