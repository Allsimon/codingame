import { adjacentTo, emptyMatrix, printMatrix } from '@lib/matrix/matrix'
import { sum } from '@lib/array/arrays'

export const init = () => {
  const [W, H] = readline().split(' ').map(Number)
  const matrix = [...Array(H)].map(() => readline().split('').map(Number))

  const foo = emptyMatrix(H, W)

  for (let i = 0; i < H; i++) {
    for (let j = 0; j < W; j++) {
      const neighbours = sum(adjacentTo(matrix, i, j, true))
      if (matrix[i][j] === 1) {
        foo[i][j] = neighbours === 2 || neighbours === 3 ? 1 : 0
      } else {
        foo[i][j] = neighbours === 3 ? 1 : 0
      }
    }
  }
  print(printMatrix(foo, (t) => '' + t).trimRight())
}
