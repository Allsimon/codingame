export const init = () => {
  const x = readline()
  if (!x) return
  const n = +readline()

  const upNumber = (value: [string, number]): [string, number] => {
    const [rem, left] = upThing(+value[0], value[1], 999)
    return [('' + rem).padStart(3, '0'), left]
  }

  const upThing = (value: number, toAdd: number, wrapAround: number) => {
    const a1 = value + toAdd
    const remainder = a1 % wrapAround
    const left = (a1 - remainder) / wrapAround
    return [remainder, left]
  }

  const upS = (value: [string, number]): [string, number] => {
    const [vall, toAdd] = value
    const [a, b] = [0, 1].map((o) => vall.charCodeAt(o)).map((o) => o - 65)
    const [b1, left] = upThing(b, toAdd, 26)
    const [a1, left2] = upThing(a, left, 26)
    return [String.fromCharCode(a1 + 65, b1 + 65), left2]
  }

  const doStuff = (value: [string, number]): string => {
    const [a, b, c] = value[0].split('-')
    const [b1, l1] = upNumber([b, value[1]])
    const [c1, l2] = upS([c, l1])
    const [a1, l3] = upS([a, l2])
    const output = [a1, b1, c1].join('-')
    if (l3 != 0) {
      return doStuff([output, l3])
    }
    return output
  }

  print(doStuff([x, n]))
}
