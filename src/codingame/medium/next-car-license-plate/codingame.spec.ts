import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/next-car-license-plate/codingame'

describe('NextCarLicensePlate', () => {
  it(`+5`, () =>
    runTest(
      init,
      `AB-123-CD
5`,
      `AB-128-CD`,
    ))

  it(`+100`, () =>
    runTest(
      init,
      `AZ-566-QS
100`,
      `AZ-666-QS`,
    ))

  it(`999+1`, () =>
    runTest(
      init,
      `BN-999-GH
1`,
      `BN-001-GI`,
    ))

  it(`+10 000`, () =>
    runTest(
      init,
      `CG-007-CG
10000`,
      `CG-017-CQ`,
    ))

  it(`+100 000`, () =>
    runTest(
      init,
      `IO-010-OI
100000`,
      `IO-110-SE`,
    ))

  it(`+1 000 000`, () =>
    runTest(
      init,
      `QS-456-DF
1000000`,
      `QT-457-PS`,
    ))

  it(`Very big`, () =>
    runTest(
      init,
      `ER-963-DF
87654321`,
      `JQ-027-XY`,
    ))
})
