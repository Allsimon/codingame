import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/bulgarian-solitaire/codingame'

describe('BulgarianSolitaire', () => {
  it(`1 pile`, () =>
    runTest(
      init,
      `1
3`,
      `1`,
    ))

  it(`3 piles`, () =>
    runTest(
      init,
      `3
5 4 2`,
      `5`,
    ))

  it(`Large piles`, () =>
    runTest(
      init,
      `2
1000 950
`,
      `62`,
    ))

  it(`Empty piles`, () =>
    runTest(
      init,
      `7
0 0 7 421 0 0 7`,
      `1`,
    ))

  it(`Many piles`, () =>
    runTest(
      init,
      `30
149 173 180 166 33 100 164 78 94 17 27 151 53 93 20 188 101 197 70 159 69 15 185 76 82 171 70 180 129 155`,
      `82`,
    ))
})
