export const init = () => {
  const N = +readline()
  if (N === undefined || N == 0) return
  let lines = readline()
    .split(' ')
    .map(Number)
    .filter((l) => l !== 0)
    .sort((a, b) => a - b)

  const result = new Array<number[]>()

  const equals = (a1: number[], a2: number[]) =>
    a1.length === a2.length && a1.every((value, index) => value === a2[index])

  while (!result.some((l) => equals(l, lines))) {
    result.push(lines)
    lines = lines.map((l) => l - 1)
    lines.push(lines.length)
    lines = lines.filter((l) => l !== 0).sort((a, b) => a - b)
  }
  let i = 0
  for (; i < result.length; i++) {
    if (equals(lines, result[i])) break
  }

  print(result.length - i)
}
