import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/ddcg-mapper/codingame'

describe('DdcgMapper', () => {
  it(`Exemple`, () =>
    runTest(
      init,
      `7
2
X000 2
00XX 3`,
      `0000
X0XX
0000
X000
00XX
X000
0000`,
    ))

  it(`1`, () =>
    runTest(
      init,
      `4
1
X0X0 2`,
      `X0X0
0000
X0X0
0000`,
    ))

  it(`2`, () =>
    runTest(
      init,
      `12
3
X0X0 2
0X0X 3
0XX0 5`,
      `XXXX
0000
XXX0
0X0X
X0X0
0000
XXXX
0XX0
X0X0
0X0X
X0X0
0000`,
    ))

  it(`3`, () =>
    runTest(
      init,
      `90
9
X000 2
0X00 3
00X0 5
000X 4
0XX0 11
0X00 7
XXXX 42
00XX 16
XX00 22`,
      `XXX0
0000
XXXX
0X00
X000
00X0
XXXX
0000
X000
0X00
X0XX
0000
XX00
0XX0
X00X
0XX0
X000
0000
XX0X
0000
XXX0
0X00
X00X
0000
XXX0
00X0
X0XX
0X00
X000
0000
XXXX
0000
X000
0X00
XX0X
0XX0
XX00
0000
X00X
0X00
X0X0
0X00
XXXX
0000
X000
0XX0
XXXX
0000
XXXX
0000
X0XX
0X00
X000
0000
XX0X
0XX0
X000
0XX0
X0XX
0000
XXX0
0000
XX0X
0X00
X000
00X0
XX0X
0000
XXX0
0X00
X0XX
0000
XX00
0000
X0XX
0XX0
XX00
0000
XX0X
0XX0
X0X0
0X00
X00X
0X00
XX00
00X0
X00X
0X00
X000
0000`,
    ))
})
