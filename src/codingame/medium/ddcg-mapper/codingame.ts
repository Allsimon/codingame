export const init = () => {
  const L = +readline()
  const N = +readline()

  if (N === undefined) return
  const lines = [...Array(N)].map(() => readline().split(' ')).reverse()
  const matrix = [...Array(L)].map(() => [false, false, false, false])

  const orArray = (b1: boolean[], b2: boolean[]) => {
    for (let i = 0; i < b1.length; i++) b2[i] = b1[i] || b2[i]
  }

  lines.forEach((line) => {
    const [pattern, tempo] = line
    const bitPattern = pattern.split('').map((p) => p === 'X')

    for (let i = 0; i < matrix.length; i++) {
      if ((i % +tempo) + 1 === +tempo) {
        orArray(bitPattern, matrix[i])
      }
    }
  })
  matrix
    .reverse()
    .map((l) => l.map((f) => (f ? 'X' : '0')).join(''))
    .forEach((l) => print(l))
}
