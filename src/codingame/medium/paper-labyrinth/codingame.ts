import { Dijkstra } from '@lib/graph/dijkstra/dijkstra'

export const init = () => {
  const [xs, ys] = readline().split(' ').map(Number)
  if (xs == undefined) return
  const [xr, yr] = readline().split(' ').map(Number)
  const [w, h] = readline().split(' ').map(Number)

  const matrix = [...Array(h)].map(() =>
    readline()
      .split('')
      .map((n) => Number.parseInt(n, 16)),
  )
  const graph = new Dijkstra()
  const addVertex = (i1: number, j1: number, i2: number, j2: number) =>
    graph.addVertexDistanceOne(i1 + ' ' + j1, i2 + ' ' + j2)

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      let value = matrix[i][j].toString(2).split('').reverse().map(Number).map(Boolean)
      if (!value[Dir.DOWN]) addVertex(i, j, i + 1, j)
      if (!value[Dir.LEFT]) addVertex(i, j, i, j - 1)
      if (!value[Dir.TOP]) addVertex(i, j, i - 1, j)
      if (!value[Dir.RIGHT]) addVertex(i, j, i, j + 1)
    }
  }

  const entrance = ys + ' ' + xs
  const rabbit = yr + ' ' + xr

  const output =
    graph.shortestPath(entrance, rabbit).length -
    1 +
    ' ' +
    (graph.shortestPath(rabbit, entrance).length - 1)
  print(output)
}

const enum Dir {
  DOWN,
  LEFT,
  TOP,
  RIGHT,
}
