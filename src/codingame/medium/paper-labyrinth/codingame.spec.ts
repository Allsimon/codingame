import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/paper-labyrinth/codingame'

describe('PaperLabyrinth', () => {
  it(`Simple`, () =>
    runTest(
      init,
      `0 0
5 0
6 1
75555d`,
      `5 5`,
    ))

  it(`No one-way door, no loops`, () =>
    runTest(
      init,
      `0 0
1 1
4 4
e65c
abea
2519
355d`,
      `10 10`,
    ))

  it(`One way door`, () =>
    runTest(
      init,
      `0 0
1 3
4 4
75ce
6598
2758
b759`,
      `12 8`,
    ))

  it(`House of cards`, () =>
    runTest(
      init,
      `0 0
3 5
5 6
7554e
6459b
a3c2c
3caaa
6119a
35d39`,
      `18 8`,
    ))
})
