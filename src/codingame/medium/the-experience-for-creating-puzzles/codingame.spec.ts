import { runTest } from '@codingame/test'
import { init } from '@codingame/medium/the-experience-for-creating-puzzles/codingame'

describe('TheExperienceForCreatingPuzzles', () => {
  it(`Leveling up`, () =>
    runTest(
      init,
      `10
300
1`,
      `11
364`,
    ))

  it(`Accepted 5 times`, () =>
    runTest(
      init,
      `1
10
5`,
      `11
287`,
    ))

  it(`Prolific Codingamer.`, () =>
    runTest(
      init,
      `23
250
20`,
      `28
883`,
    ))

  it(`BlitzProg has second thoughts`, () =>
    runTest(
      init,
      `27
928
1`,
      `27
628`,
    ))

  it(`Unapproved puzzle.`, () =>
    runTest(
      init,
      `30
1
0`,
      `30
1`,
    ))
})
