export const init = () => {
  let level = +readline()
  let xp = +readline()
  let N = +readline() * 300
  if (N === undefined) return

  const xpNeeded = (lvl: number) => Math.floor(lvl * Math.sqrt(lvl) * 10)

  while ((N -= xp) >= 0) {
    level++
    xp = xpNeeded(level)
  }
  print(level)
  print(-N)
}
