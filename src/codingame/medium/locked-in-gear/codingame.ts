import { euclideanDistance } from '@lib/geometry/distance'

export const init = () => {
  const N = +readline()
  if (!N) return

  const gears = [...Array(N)].map(() => readline().split(' ').map(Number))

  const rotate = (value: number[], gears: number[][], clockwise: boolean) => {
    const [x1, y1, r1] = value
    return gears.forEach((gear, i) => {
      const [x2, y2, r2] = gear

      if (euclideanDistance([x1, y1], [x2, y2]) === r1 + r2) {
        // @ts-ignore
        if (gear[3] === undefined) gear[3] = !clockwise
        // @ts-ignore
        else if (gear[3] === clockwise) throw new Error('locked')

        rotate(gear, remove(gears, i), !clockwise)
      }
    })
  }

  const remove = (array: number[][], index: number) => array.filter((val, i) => i !== index)

  try {
    rotate(gears[0], remove(gears, 0), true)
    const result = gears[gears.length - 1][3]
    if (result == undefined) print('NOT MOVING')
    else if (result) print('CW')
    else print('CCW')
  } catch (e) {
    print('NOT MOVING')
  }
}
