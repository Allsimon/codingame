import { Mapp } from '@lib/type/type'

export const init = () => {
  const N = +readline()
  if (!N) return

  const words = [...Array(N)].map(() => readline().split(''))

  const count = (word: string[]) => {
    return word.reduce((prev, current) => {
      prev[current] = (prev[current] || 0) + 1
      return prev
    }, {} as Mapp<number>)
  }

  const LETTERS = count(readline().split(''))

  const points: Mapp<number> = {}
  const assign = (point: number, ...letters: string[]) =>
    letters.forEach((l) => (points[l] = point))
  assign(1, 'e', 'a', 'i', 'o', 'n', 'r', 't', 'l', 's', 'u')
  assign(2, 'd', 'g')
  assign(3, 'b', 'c', 'm', 'p')
  assign(4, 'f', 'h', 'v', 'w', 'y')
  assign(5, 'k')
  assign(8, 'j', 'x')
  assign(10, 'q', 'z')

  const score = (word: string[]) => {
    const wordCount = count(word)
    const gotLetters = Object.keys(wordCount).every((key) => wordCount[key] <= (LETTERS[key] || 0))

    return gotLetters ? word.reduce((prev, current) => prev + points[current], 0) : 0
  }

  const output = words
    .map((w, i) => [score(w), i, w.join('')])
    .filter((w) => w[0] !== 0)
    // @ts-ignore
    .sort((a, b) => (b[0] - a[0] === 0 ? -b[1] + a[1] : b[0] - a[0]))[0][2]

  print(output)
}
