export const init = () => {
  const N = +readline()
  if (!N) return
  const glass = [' *** ', ' * * ', ' * * ', '*****'].join('\n')
  const empty = [``, ``, ``, ``].join('\n')
  const spaceColumn = [' ', ' ', ' ', ' '].join('\n')
  const bigSpaceColumn = ['   ', '   ', '   ', '   '].join('\n')
  const maxSize = (value: number) => {
    let i = 0
    while ((i * (i + 1)) / 2 <= value) i++
    return i - 1
  }

  const join = (s1: string, s2: string) => {
    const t1 = s1.split('\n')
    const t2 = s2.split('\n')
    return t1.map((t, i) => t + t2[i]).join('\n')
  }

  const glasses = (value: number, max: number) => {
    let foo = empty
    for (let i = 0; i < value; i++) {
      foo = join(join(foo, glass), i === value - 1 ? empty : spaceColumn)
    }

    for (let j = 0; j < max - value; j++) {
      foo = join(bigSpaceColumn, foo)
    }

    for (let j = 0; j < max - value; j++) {
      foo = join(foo, bigSpaceColumn)
    }
    return foo
  }

  const max = maxSize(N)
  const output = []
  for (let i = 1; i <= max; i++) {
    output.push(glasses(i, max))
  }
  output
    .join('\n')
    .split('\n')
    .forEach((l) => print(l))
}
