// QueneauNumbers
import { sameArray } from '@lib/array/arrays'

export const init = () => {
  const N = +readline()

  const queneau = (value: number[]) => {
    let output = []
    for (let i = 0; i < value.length / 2; i++) {
      if (i !== value.length - 1 - i) output.push(value[value.length - 1 - i])
      output.push(value[i])
    }
    return output
  }

  // tslint:disable-next-line:no-inner-declarations
  function* queneauGenerator(value: number) {
    const original = [...Array(value)].map((_, i) => i + 1)

    let inc = original
    while (value-- > 0) {
      yield (inc = queneau(inc))
    }
    if (!sameArray(inc, original)) throw new Error('IMPOSSIBLE')
  }

  try {
    const results = [...queneauGenerator(N)]
    print(results.map((result) => result.join(',')).join('\n'))
  } catch (e) {
    print((e as Error).message)
  }
}
