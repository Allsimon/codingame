export const init = () => {
  const speed = +readline()
  if (!speed) return
  const distDurations = [...Array(+readline())].map(() => readline().split(' ').map(Number))

  const checkPossible = (value: number): boolean => {
    return distDurations.every((v) => {
      const [dist, duration] = v
      const po = (3.6 * dist) / value
      return ((po - (po % duration)) / duration) % 2 === 0
    })
  }

  let output = speed
  while (!checkPossible(output--)) {}
  print(output + 1)
}
