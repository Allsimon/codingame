export const init = () => {
  const n = +readline()
  if (!n) return
  const values = readline().split(' ').map(Number)

  let output = 0

  for (let i = 0; i < values.length; i++) {
    let current = values[i]
    for (let j = i + 1; j < values.length; j++) {
      const diff = values[j] - current
      if (diff < output) output = diff
      if (values[j] > current) {
        i = j - 1
        break
      }
    }
  }
  print(output)
}
