export const init = () => {
  const [W, H] = readline().split(' ').map(Number)
  if (!W) return
  let [X, Y] = readline().split(' ').map(Number)
  const direction = readline()
  const T = +readline()

  // @ts-ignore
  let currentDir = { N: 0, E: 1, S: 2, W: 3 }[direction]

  let dirModif = [
    [-1, 0],
    [0, 1],
    [1, 0],
    [0, -1],
  ]

  const matrix = [...Array(H)].map(() => readline().split(''))
  // @ts-ignore
  const rotate = (value: string) => (currentDir = (4 + currentDir + (value === '#' ? 1 : -1)) % 4)

  const move = () => {
    rotate(matrix[X][Y])
    matrix[X][Y] = matrix[X][Y] === '#' ? '.' : '#'
    // @ts-ignore
    const [x1, y1] = dirModif[currentDir]
    X += x1
    Y += y1
  }

  for (let i = 0; i < T; i++) {
    move()
  }

  print(matrix.map((l) => l.join('')).join('\n'))
}
