import { sum } from '@lib/array/arrays'

export const init = () => {
  const N = +readline()
  if (!N) return
  const C = +readline()

  const budget = [...Array(N)]
    .map(() => readline())
    .map(Number)
    .sort((a, b) => a - b)

  const checkBudget = (budg: number[], value: number, startAt = 0) => {
    const split = Math.floor(value / (budg.length - startAt))
    if (startAt < budg.length) {
      if (budg[startAt] >= split) {
        budg[startAt] = split
      }
      checkBudget(budg, value - budg[startAt], startAt + 1)
    }
  }

  checkBudget(budget, C)

  if (sum(budget) !== C) {
    print('IMPOSSIBLE')
  } else {
    budget.forEach((b) => print(b))
  }
}
