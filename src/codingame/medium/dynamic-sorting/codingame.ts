export const init = () => {
  const expression = readline()
  const types = readline()

  const objs = [...Array(+readline())].map(readline).map((e) => {
    const out: any = {}
    e.split(',').map((attr) => {
      const [key, value] = attr.split(':')
      out[key] = value
    })
    return out
  })

  const attrs: SortAttr[] = (expression.match(/[-+]\w+/g) || []).map((s) => ({
    asc: s.charAt(0) === '+',
    attr: s.slice(1),
  }))

  const compareFn = (a: any, b: any, attrs: SortAttr[]): number => {
    if (attrs.length === 0) return 0
    const attr = attrs[0].attr
    const asc = attrs[0].asc
    if (p(a[attr]) < p(b[attr])) return asc ? -1 : 1
    if (p(a[attr]) > p(b[attr])) return asc ? 1 : -1
    return compareFn(a, b, attrs.slice(1))
  }

  const p = (a: any) => (isNaN(+a) ? a : +a)

  objs.sort((a, b) => compareFn(a, b, attrs)).forEach((o) => print(o.id))
}

interface SortAttr {
  attr: string
  asc: boolean
}
