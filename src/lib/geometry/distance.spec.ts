import {
  euclideanDistance,
  isInCircle,
  isSame,
  manhattanDistance,
  Norm,
  vectorSum,
} from '@lib/geometry/distance'
import { Point } from '@lib/geometry/point'

describe('Distance', () => {
  function isANorm(norm: Norm) {
    const a = 13

    const p1: Point = [Math.random(), Math.random()]
    const p2: Point = [Math.random(), Math.random()]
    const p3: Point = [Math.random(), Math.random()]
    const zero: Point = [0, 0]

    // triangle inequality
    // @ts-ignore
    expect(norm(vectorSum(p1, p2), zero)).toBeLessThanOrEqual(norm(p1, zero) + norm(p2, zero))

    // zero vector
    expect(norm(p1, p1)).toStrictEqual(0)

    const p3scaled = p3.map((i) => a * i)

    // @ts-ignore
    expect(norm(p3scaled, zero)).toBeCloseTo(a * norm(p3, zero), 5)

    // commutativity
    expect(norm(p1, p2)).toBeCloseTo(norm(p2, p1), 5)
  }

  it('euclidean distance', () => {
    expect(euclideanDistance([2, 3], [8, 7])).toBe(Math.sqrt(52))

    isANorm(euclideanDistance)
  })

  it('vector sum doesnt fuck up', () => {
    expect(vectorSum([3, 4], [2, 8])).toEqual([5, 12])
  })

  it('manhattan distance', () => {
    expect(manhattanDistance([3, 4], [2, 8])).toBe(5)
    isANorm(manhattanDistance)
  })

  it('is in circle', () => {
    expect(isInCircle([3, 4], 5)).toBeTruthy()
    expect(isInCircle([3, 4], 2)).toBeFalsy()
    expect(isInCircle([8, 9], 3, [6, 8])).toBeTruthy()
    expect(isInCircle([8, 9], 1, [6, 8])).toBeFalsy()
  })

  it('is same', () => {
    expect(isSame([3, 4], [3, 4])).toBeTruthy()
    expect(isSame([4, 3], [3, 4])).toBeFalsy()
  })
})
