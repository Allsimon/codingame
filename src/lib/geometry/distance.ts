import { Point } from '@lib/geometry/point'

export const euclideanDistance: Norm = (p1, p2) =>
  Math.sqrt(Math.pow(p1[0] - p2[0], 2) + Math.pow(p1[1] - p2[1], 2))

export const manhattanDistance: Norm = (p1, p2) => Math.abs(p1[0] - p2[0]) + Math.abs(p1[1] - p2[1])

export const isInCircle = (p: Point, radius: number, circleCenter: Point = [0, 0]) =>
  euclideanDistance(p, circleCenter) <= radius

export const vectorSum = (v1: number[], v2: number[]) => v1.map((a, i) => a + v2[i])

export const isSame = (p1: Point, p2: Point) => manhattanDistance(p1, p2) === 0

export type Norm = (p1: Point, p2: Point) => number
