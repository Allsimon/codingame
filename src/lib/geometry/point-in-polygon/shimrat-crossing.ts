import { Point } from '@lib/geometry/point'
import { PointInPolygon } from '@lib/geometry/point-in-polygon/point-in-polygon'

const pointIn = require('pointinpoly').pointInPoly // use the array-pairs version
export class ShimratCrossing implements PointInPolygon {
  contains(point: Point, vertices: Point[]): boolean {
    const [x, y] = point

    return pointIn(x, y, vertices)
  }
}
