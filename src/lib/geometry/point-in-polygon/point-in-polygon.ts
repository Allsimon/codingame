import { Point } from '@lib/geometry/point'

export interface PointInPolygon {
  contains(point: Point, vertices: Point[]): boolean
}
