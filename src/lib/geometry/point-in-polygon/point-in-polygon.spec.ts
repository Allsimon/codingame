import { ShimratCrossing } from '@lib/geometry/point-in-polygon/shimrat-crossing'
import { PointInPolygon } from '@lib/geometry/point-in-polygon/point-in-polygon'
import { Point } from '@lib/geometry/point'

describe('Point in poly test', () => {
  const algorithms: PointInPolygon[] = [new ShimratCrossing()]

  it('at least one algorithm is implemented', () => {
    expect(algorithms.length).toBeTruthy()
  })

  it('square test', () => {
    const polygon: Point[] = [
      [-100, -100],
      [100, -100],
      [100, 100],
      [-100, 100],
    ]
    const pointsOutside: Point[] = [
      [101, 101],
      [80, -101],
    ]
    const pointsInside: Point[] = [
      [0, 0],
      [99, 99],
      [0, -100],
    ]

    check(polygon, pointsOutside, pointsInside)
  })

  it('triangle test', () => {
    // *****
    // *   *
    // *  **
    // * ***
    // *****
    const polygon: Point[] = [
      [1, 1],
      [1, 3],
      [3, 1],
    ]
    const pointsOutside: Point[] = [
      [0, 0],
      [1, 4],
      [4, 1],
      [2, 3],
    ]
    const pointsInside: Point[] = [
      [1, 2],
      [2, 1],
      [2, 2],
    ]

    check(polygon, pointsOutside, pointsInside)
  })

  // Test doesn't pass
  // it('ten sides', () => {
  //   const polygon: Point[] = [[0, 0], [100, 0], [150, 25], [200, 75], [150, 100], [100, 125], [50, 125], [-50, 100], [-60, 50], [-50, 25]]
  //   const pointsOutside: Point[] = [[50, 80], [100, 100], [175, 60]]
  //   const pointsInside: Point[] = [[-100, 80], [0, 120], [200, 100]]
  //
  //   check(polygon, pointsOutside, pointsInside)
  // })

  const check = (polygon: Point[], pointsOutside: Point[], pointsInside: Point[]): void => {
    algorithms.forEach((algo) => {
      pointsInside.forEach((p) => expect(algo.contains(p, polygon)).toBeTruthy())
      pointsOutside.forEach((p) => expect(algo.contains(p, polygon)).toBeFalsy())
      // check that the point in the polygon are inside the polygon
      polygon.forEach((p) => expect(algo.contains(p, polygon)).toBeTruthy())
    })
  }
})
