import { angleBetween, dotProduct } from '@lib/geometry/vector'

describe('Vector', () => {
  it('angle between points', () => {
    expect(angleBetween([0, 1], [0, 0], [0, 1])).toBe(0)
    expect(angleBetween([0, 0], [0, 0], [0, 1])).toBe(0)

    expect(angleBetween([1, 0], [0, 0], [0, 1])).toBeCloseTo(Math.PI / 2, 6)
    expect(angleBetween([1, 1], [0, 0], [0, 1])).toBeCloseTo(Math.PI / 4, 6)
  })

  it('scalar product', () => {
    expect(dotProduct([1, 2], [3, 4])).toEqual(11)
  })
})
