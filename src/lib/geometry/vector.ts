import { Point } from '@lib/geometry/point'
import { sum } from '@lib/array/arrays'
import { euclideanDistance } from '@lib/geometry/distance'

export const angleBetween = (a: Point, b: Point, c: Point): number => {
  const v1 = vector(b, a)
  const v2 = vector(b, c)
  return angleBetweenVectors(v1, v2)
}

export const angleBetweenVectors = (v1: Vector, v2: Vector): number => {
  const normm = norm(v1) * norm(v2)
  return normm === 0 ? 0 : Math.acos(dotProduct(v1, v2) / normm)
}

export const vector = (p1: Point, p2: Point) => [p1[0] - p2[0], p1[1] - p2[1]]

export const dotProduct = (v1: Vector, v2: Vector): number => sum(v1.map((a, i) => a * v2[i]))

export const norm = (v1: Vector) => euclideanDistance(v1, [0, 0])

export type Vector = number[]
