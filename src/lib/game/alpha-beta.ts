export const alphaBeta = (
  node: MMNode,
  alpha = -Infinity,
  beta = Infinity,
  maximizingPlayer = true,
): number => {
  if (node.children.length === 0) {
    if (node.value == undefined) throw new Error('No value found on leaf')
    return node.value
  }
  if (maximizingPlayer) {
    let value = -Infinity
    for (let child of node.children) {
      value = Math.max(value, alphaBeta(child, alpha, beta, false))
      alpha = Math.max(alpha, value)
      if (alpha >= beta) break
    }
    return value
  } else {
    let value = Infinity
    for (let child of node.children) {
      value = Math.min(value, alphaBeta(child, alpha, beta, true))
      beta = Math.min(beta, value)
      if (alpha >= beta) break
    }
    return value
  }
}

// Sample method
export const computeMMRootNode = (D: number, B: number, leaves: number[]) => {
  let root = { children: [] }
  let previousNodes: MMNode[] = [root]
  let tempNode: MMNode[] = []

  for (let i = 1; i <= D; i++) {
    previousNodes.forEach((node) => {
      for (let j = 0; j < B; j++) {
        const child = {
          value: i === D ? leaves.shift() : undefined,
          children: [],
        }
        node.children.push(child)
        tempNode.push(child)
      }
    })
    previousNodes = tempNode
    tempNode = []
  }
  return root
}

export interface MMNode {
  value?: number
  children: MMNode[]
}
