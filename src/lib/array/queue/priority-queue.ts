import { PriorityNode } from '@lib/array/queue/priority-node'

export class PriorityQueue<T> {
  nodes: PriorityNode<T>[] = []

  /**
   * Enqueue a new node
   * @param {[type]} priority
   * @param {[type]} key
   */
  enqueue(priority: number, key: T) {
    this.nodes.push(new PriorityNode(key, priority))
    this.nodes.sort((a, b) => a.priority - b.priority)
  }

  /**
   * Dequeue the highest priority key
   */
  dequeue(): T {
    const priorityNode = this.nodes.shift()
    if (!priorityNode) throw new Error('Empty queue')
    return priorityNode.key
  }

  /**
   * Checks if empty
   */
  empty(): boolean {
    return !this.nodes.length
  }
}
