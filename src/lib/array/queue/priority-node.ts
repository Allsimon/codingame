export class PriorityNode<T> {
  key: T
  priority: number

  constructor(key: T, priority: number) {
    this.key = key
    this.priority = priority
  }
}
