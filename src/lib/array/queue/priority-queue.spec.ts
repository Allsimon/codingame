import { PriorityQueue } from '@lib/array/queue/priority-queue'

describe('Priority Queue', () => {
  it(`simple`, () => {
    const queue = new PriorityQueue()
    queue.enqueue(3, 'foo')
    queue.enqueue(1, 'bar')
    queue.enqueue(6, 'baz')

    expect(queue.dequeue()).toEqual('bar')
    expect(queue.dequeue()).toEqual('foo')
    expect(queue.dequeue()).toEqual('baz')
    expect(() => queue.dequeue()).toThrow('Empty queue')
  })
})
