import { chunk, fillUpTo, foldLeft, intersect, sameArray, sum } from '@lib/array/arrays'

describe('Array', () => {
  it('vector sum doesnt fuck up', () => {
    expect(sum([1, 2, 3, 4])).toEqual(10)
  })

  it('intersects', () => {
    expect(intersect([1, 2, 3, 4], [2, 4, 5, 6])).toEqual([2, 4])
  })

  it('fillsUpTo', () => {
    expect(fillUpTo([1, 2, 3, 4], 5, 2)).toEqual([1, 2])
    expect(fillUpTo([1, 2, 3, 4], 5, 8)).toEqual([1, 2, 3, 4, 5, 5, 5, 5])
  })

  it('same array', () => {
    expect(sameArray([1, 2, 3, 4], [1, 2, 3, 4])).toBeTruthy()
    expect(sameArray([1, 2, 3, 4], [1, 2, 3, 4, 5])).toBeFalsy()
    expect(sameArray([1, 2, 3, 4, 5], [1, 2, 3, 4])).toBeFalsy()
    expect(sameArray([true, false], [false, true], (v1, v2) => v1 === !v2)).toBeTruthy()
  })

  it('chunk', () => {
    expect(chunk([1, 2, 3, 4], 1)).toEqual([[1], [2], [3], [4]])
    expect(chunk([1, 2, 3, 4], 2)).toEqual([
      [1, 2],
      [3, 4],
    ])
    expect(chunk([1, 2, 3, 4], 3)).toEqual([[1, 2, 3], [4]])
  })

  it('foldLeft', () => {
    expect(foldLeft(['0', '1', '2'], 3, (current, next) => current + +next)).toEqual(6)
  })
})
