export const sum = (array: number[]) => array.reduce((total, i) => total + i, 0)

export function intersect<T>(arr1: T[], arr2: T[]): T[] {
  return [...new Set(arr1)].filter((x) => new Set(arr2).has(x))
}

export function fillUpTo<T>(arr: T[], value: T, newLength: number) {
  return [...Array(newLength)].map((_, i) => (i < arr.length ? arr[i] : value))
}

export function sameArray<T>(a1: T[], a2: T[], equality = (t1: T, t2: T) => t1 === t2) {
  return a1.length === a2.length && a1.every((v, i) => equality(v, a2[i]))
}

export function chunk<T>(arr: T[], len: number): T[][] {
  let chunks = [],
    i = 0
  const n = arr.length
  while (i < n) chunks.push(arr.slice(i, (i += len)))
  return chunks
}

export function foldLeft<T, U>(arr: T[], identity: U, acc: (current: U, next: T) => U): U {
  if (!arr.length) {
    return identity
  } else {
    const head = arr[arr.length - 1]
    const tail = arr.slice(0, arr.length - 1)
    return foldLeft(tail, acc(identity, head), acc)
  }
}

export function sortByMultiCriteria<T>(
  ...criteria: ((t1: T, t2: T) => number)[]
): (t1: T, t2: T) => number {
  return (a: T, b: T) => {
    for (let i = 0; i < criteria.length; i++) {
      const curCriteriaComparatorValue = criteria[i](a, b)
      // if the comparison objects are not equivalent, return the value obtained
      // in this current criteria comparison
      if (curCriteriaComparatorValue !== 0) {
        return curCriteriaComparatorValue
      }
    }
    return 0
  }
}

export function reduceToObject<T, K extends string | number | symbol, V>(
  array: T[],
  keyExtractor: (elmt: T) => K,
  valueExtractor: (elmt: T) => V,
): Record<K, V> {
  return Object.fromEntries(array.map((a) => [keyExtractor(a), valueExtractor(a)])) as Record<K, V>
}
