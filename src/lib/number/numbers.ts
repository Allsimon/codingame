import { intersect } from '@lib/array/arrays'

const tau = 0.00001
export const isClose = (a1: number, a2: number) => Math.abs(a1 - a2) < tau

export const toDecimal = (value: string, base: number): number => {
  return parseInt(value, base)
}

export const getDigitCount = (number: number) =>
  Math.max(Math.floor(Math.log10(Math.abs(number))), 0) + 1

export const takeNumbersAt = (number: number, n: number, fromLeft = false) => {
  const location = fromLeft ? getDigitCount(number) + 1 - n : n
  return Math.floor((number / Math.pow(10, location - 1)) % 10)
}

export const getFirstNumbers = (number: number, n: number, fromLeft = false) => {
  const location = fromLeft ? getDigitCount(number) - n : n
  if (!fromLeft) {
    return number % 10 ** location
  }
  return Math.floor(number / 10 ** location)
}

export const decimalToBase = (value: number, base: number): number[] => {
  if (value === 0) return [0]
  const output = []

  let quotient = value
  let remainder = -1
  while (quotient !== 0) {
    remainder = quotient % base
    quotient = Math.floor(quotient / base)
    output.push(remainder)
  }

  return output
}

export const factor = (value: number): number[] => {
  const max = Math.sqrt(value)
  const output = new Set<number>()
  for (let i = 1; i <= max; i++) {
    const b = value / i
    if (Number.isInteger(b)) {
      output.add(i)
      output.add(b)
    }
  }
  return [...output].sort((a, b) => a - b)
}

export const commonFactors = (a: number, b: number): number[] => {
  return intersect(factor(a), factor(b))
}

export const isPrime = (num: number): boolean => {
  if (num <= 1) return false
  const sqrt = Math.floor(Math.sqrt(num))
  for (let i = 2; i < sqrt + 1; i++) {
    if (num % i === 0) {
      return false
    }
  }
  return true
}

export function isNumeric(str: string) {
  return !isNaN(parseFloat(str))
}

export function modulo(n: number, m: number) {
  return ((n % m) + m) % m
}
