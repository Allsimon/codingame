import {
  commonFactors,
  decimalToBase,
  factor,
  getFirstNumbers,
  isClose,
  isPrime,
  modulo,
  takeNumbersAt,
} from '@lib/number/numbers'

describe('Number', () => {
  it('is close', () => {
    expect(isClose(3, 3.00000000032)).toBeTruthy()
    expect(isClose(3.00000000032, 3)).toBeTruthy()
    expect(isClose(3, 5)).toBeFalsy()
  })

  it('takeNumbersAt', () => {
    expect(takeNumbersAt(1234567890987654321, 1, true)).toBe(1)
    expect(takeNumbersAt(1234567890987654321, 4, true)).toBe(4)
    expect(takeNumbersAt(1234567890987654321, 7, true)).toBe(7)
    expect(takeNumbersAt(1234567890987654321, 8, true)).toBe(8)
    expect(takeNumbersAt(1234567890987654321, 14, true)).toBe(6)
    expect(takeNumbersAt(123456789098765, 1, false)).toBe(5)
    expect(takeNumbersAt(123456789098765, 9, false)).toBe(7)
    expect(takeNumbersAt(123456789098765, 10, false)).toBe(6)
  })

  it('getFirstNumbers', () => {
    expect(getFirstNumbers(1234567890987654321, 1, true)).toBe(1)
    expect(getFirstNumbers(1234567890987654321, 4, true)).toBe(1234)
    expect(getFirstNumbers(1234567890987654321, 13, true)).toBe(1234567890987)
    expect(getFirstNumbers(123456789098765, 5, false)).toBe(98765)
  })

  it('decimal to base', () => {
    expect(decimalToBase(0, 2)).toEqual([0])
    expect(decimalToBase(237, 2)).toEqual([1, 0, 1, 1, 0, 1, 1, 1])
    expect(decimalToBase(983012, 5)).toEqual([2, 2, 0, 4, 2, 4, 2, 2, 2])
  })

  it('factor', () => {
    expect(factor(18)).toEqual([1, 2, 3, 6, 9, 18])
    expect(factor(48)).toEqual([1, 2, 3, 4, 6, 8, 12, 16, 24, 48])
    expect(factor(237)).toEqual([1, 3, 79, 237])
    expect(factor(6)).toEqual([1, 2, 3, 6])
    expect(factor(9)).toEqual([1, 3, 9])
  })

  it('common factor', () => {
    expect(commonFactors(84, 132)).toEqual([1, 2, 3, 4, 6, 12])
    expect(commonFactors(79, 31)).toEqual([1])
    expect(commonFactors(6, 9)).toEqual([1, 3])
  })

  it('isPrime', () => {
    const knownPrime = [
      2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89,
      97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191,
      193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293,
      307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419,
      421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499,
    ]

    expect(isPrime(0)).toBeFalsy()
    expect(isPrime(1301081)).toBeTruthy()
    for (let i = 1; i < 500; i++) expect(isPrime(i)).toEqual(knownPrime.includes(i))
  })

  it('modulo', () => {
    expect(modulo(1, 3)).toEqual(1)
    expect(modulo(3, 3)).toEqual(0)
    expect(modulo(-1, 3)).toEqual(2)
    expect(modulo(-7, 3)).toEqual(2)
  })
})
