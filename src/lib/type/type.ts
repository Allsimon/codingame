export interface Mapp<T> {
  [key: string]: T
}

export type BinaryOperator<T> = (a: T, b: T) => T

export type Predicate<T> = (t: T) => boolean
