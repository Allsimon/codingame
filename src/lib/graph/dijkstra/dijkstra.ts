import { PriorityQueue } from '@lib/array/queue/priority-queue'
import { Mapp } from '@lib/type/type'

export class Dijkstra {
  vertices: Mapp<Mapp<number>> = {}

  /**
   * Add a new vertex and related edges
   * @param {[type]} name  [description]
   * @param {[type]} edges [description]
   */
  addVertex(name: string, edges: Mapp<number>) {
    this.vertices[name] = Object.assign(this.vertices[name] || {}, edges)
  }

  addVertexDistanceOne(name: string, ...edges: string[]) {
    const edg = edges.reduce((prev, current) => {
      prev[current] = 1
      return prev
    }, {} as Mapp<number>)
    this.addVertex(name, edg)
  }

  removeVertex(name: string, ...edges: string[]) {
    edges.forEach((edge) => {
      if (this.vertices[name]) delete this.vertices[name][edge]
    })
  }

  addVertexDistanceOneBiDir(name: string, ...edges: string[]) {
    this.addVertexDistanceOne(name, ...edges)
    edges.forEach((e) => this.addVertexDistanceOne(e, name))
  }

  cleanUp() {
    for (let key in this.vertices) {
      for (let vertexKey in this.vertices[key]) {
        if (this.vertices[vertexKey] == undefined) {
          this.vertices[vertexKey] = {}
        }
      }
    }
  }

  /**
   * Computes the shortest path from start to finish
   * @param {[type]} start  [description]
   * @param {[type]} finish [description]
   */
  shortestPath(start: string, finish: string) {
    let nodes = new PriorityQueue<string>(),
      distances: Mapp<number> = {},
      previous: Mapp<string> = {},
      path = [],
      smallest,
      vertex,
      neighbor,
      alt

    //Init the distances and queues variables
    for (vertex in this.vertices) {
      if (vertex === start) {
        distances[vertex] = 0
        nodes.enqueue(0, vertex)
      } else {
        distances[vertex] = Infinity
        nodes.enqueue(Infinity, vertex)
      }

      // @ts-ignore
      previous[vertex] = null
    }

    //continue as long as the queue haven't been emptied.
    while (!nodes.empty()) {
      smallest = nodes.dequeue()

      //we are the last node
      if (smallest === finish) {
        //Compute the path
        while (previous[smallest]) {
          path.push(smallest)
          smallest = previous[smallest]
        }
        break
      }

      //No distance known. Skip.
      if (!smallest || distances[smallest] === Infinity) {
        continue
      }

      //Compute the distance for each neighbor
      for (neighbor in this.vertices[smallest]) {
        alt = distances[smallest] + this.vertices[smallest][neighbor]

        if (alt < distances[neighbor]) {
          distances[neighbor] = alt
          previous[neighbor] = smallest
          nodes.enqueue(alt, neighbor)
        }
      }
    }
    //the starting point isn't in the solution &
    //the solution is from end to start.
    return path.concat(start).reverse()
  }
}
