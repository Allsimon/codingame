import { Dijkstra } from '@lib/graph/dijkstra/dijkstra'

describe('Dijsktra', () => {
  it(`simple`, () => {
    let graph = new Dijkstra()
    graph.addVertex('A', { B: 7, C: 8 })
    graph.addVertex('C', { A: 8 })
    graph.addVertex('B', { A: 7, F: 8 })
    graph.addVertex('F', { B: 8 })

    expect(graph.shortestPath('A', 'F')).toEqual(['A', 'B', 'F'])
  })

  it(`doesn't overwrite vertex`, () => {
    let graph = new Dijkstra()

    graph.addVertex('A', { B: 7 })
    graph.addVertex('A', { C: 8 })
    graph.addVertex('C', { A: 8 })
    graph.addVertex('B', { A: 7, F: 8 })
    graph.addVertex('F', { B: 8 })

    expect(graph.shortestPath('A', 'F')).toEqual(['A', 'B', 'F'])
  })

  it(`simple 2`, () => {
    let graph = new Dijkstra()
    graph.addVertexDistanceOne('A', 'B', 'C')
    graph.addVertexDistanceOne('B', 'A', 'C')
    graph.addVertexDistanceOne('C', 'D', 'E')
    graph.addVertexDistanceOne('E', 'A', 'B', 'F')

    graph.cleanUp()
    expect(graph.shortestPath('A', 'F')).toEqual(['A', 'C', 'E', 'F'])
  })

  it(`bidir`, () => {
    let graph = new Dijkstra()
    graph.addVertexDistanceOneBiDir('A', 'B', 'C')
    graph.addVertexDistanceOneBiDir('B', 'C')
    graph.addVertexDistanceOneBiDir('C', 'D', 'E')
    graph.addVertexDistanceOneBiDir('E', 'A', 'B', 'F')
    expect(graph.shortestPath('A', 'F')).toEqual(['A', 'E', 'F'])
  })

  it(`delete`, () => {
    let graph = new Dijkstra()
    graph.addVertex('A', { B: 7, C: 8 })
    graph.addVertex('C', { A: 8, F: 15 })
    graph.addVertex('B', { A: 7, F: 8 })
    graph.addVertex('F', { B: 8 })
    expect(graph.shortestPath('A', 'F')).toEqual(['A', 'B', 'F'])
    graph.removeVertex('A', 'B')
    // shouldn't crash
    graph.removeVertex('G', 'W')
    expect(graph.shortestPath('A', 'F')).toEqual(['A', 'C', 'F'])
  })
})
