import {
  adjacentTo,
  cloneMatrix,
  column,
  diagonals,
  directions,
  doOnAdjacent,
  emptyMatrix,
  fillMatrix,
  flatten,
  nextPosition,
  oppositeDirection,
  printMatrix,
  rotateClockwise,
  rotateCounterClockwise,
  transpose,
} from '@lib/matrix/matrix'

describe('Matrix', () => {
  it('empty matrix', () => {
    const matrix = emptyMatrix(8, 4)
    expect(matrix.length).toBe(8)
    matrix.forEach((row) => expect(row.length).toBe(4))

    const mat2 = emptyMatrix(3)
    expect(mat2.length).toBe(3)
    mat2.forEach((row) => expect(row.length).toBe(3))
  })

  it('print matrix', () => {
    const matrix = [
      [1, 2],
      [3, 4],
    ]
    const output = printMatrix(matrix)
    const expected = `1 2 \n3 4 \n`
    expect(output).toBe(expected)

    const o2 = printMatrix(matrix, (i) => i * i + ' ')
    const e2 = `1 4 \n9 16 \n`
    expect(o2).toBe(e2)
  })

  it('should fill matrix correctly', () => {
    const matrix = emptyMatrix(3, 3)
    matrix[2][3] = 999
    fillMatrix(matrix, (x, y) => x + y)

    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (i === 2 && j === 3) {
          expect(matrix[i][j]).toBe(999)
        } else {
          expect(matrix[i][j]).toBe(i + j)
        }
      }
    }
  })

  it('clone', () => {
    const matrix = [
      [1, 2],
      [3, 4],
      [5, 6],
    ]
    expect(cloneMatrix(matrix) === matrix).toBeFalsy()
    expect(cloneMatrix(matrix)).toEqual(matrix)
  })

  it('columns', () => {
    const matrix = [
      [1, 2],
      [3, 4],
      [5, 6],
    ]
    expect(column(matrix, 0)).toEqual([1, 3, 5])
    expect(column(matrix, 1)).toEqual([2, 4, 6])
  })

  it('transpose', () => {
    const matrix = [
      [1, 2],
      [3, 4],
      [5, 6],
    ]
    expect(transpose(matrix)).toEqual([
      [1, 3, 5],
      [2, 4, 6],
    ])
  })

  it('flatten', () => {
    const matrix = [
      [1, 2],
      [3, 4],
      [5, 6],
    ]
    expect(flatten(matrix)).toEqual([1, 2, 3, 4, 5, 6])
  })

  it('diagonals', () => {
    const matrix = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ]
    expect(diagonals(matrix)).toEqual([
      [1, 5, 9],
      [7, 5, 3],
    ])
    expect(() => diagonals([[1, 2]])).toThrow('Not square')
  })

  it('doOnAdjacent', () => {
    const matrix = [
      [1, 2],
      [3, 4],
      [5, 6],
    ]
    let foo = 0
    doOnAdjacent(matrix, 0, 0, true, (x, y) => (foo += x + y))
    expect(foo).toEqual(4)
    doOnAdjacent(matrix, 0, 0, false, (x, y) => (foo += x + y))
    expect(foo).toEqual(6)
  })

  it('rotate', () => {
    const matrix = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ]

    const clockwise = [
      [7, 4, 1],
      [8, 5, 2],
      [9, 6, 3],
    ]

    const counterClockwise = [
      [3, 6, 9],
      [2, 5, 8],
      [1, 4, 7],
    ]

    expect(rotateClockwise(matrix)).toEqual(clockwise)
    expect(rotateCounterClockwise(matrix)).toEqual(counterClockwise)
    expect(rotateCounterClockwise(rotateClockwise(matrix))).toEqual(matrix)
  })

  it('adjacentTo', () => {
    const matrix = emptyMatrix(3, 3)
    fillMatrix(matrix, (x, y) => x + y)

    expect(adjacentTo(matrix, 0, 0)).toEqual(expect.arrayContaining([1, 1]))
    expect(adjacentTo(matrix, 0, 1)).toEqual(expect.arrayContaining([0, 2, 2]))
    expect(adjacentTo(matrix, 1, 1)).toEqual(expect.arrayContaining([1, 1, 3, 3]))
    expect(adjacentTo(matrix, 1, 2)).toEqual(expect.arrayContaining([2, 2, 4]))

    expect(adjacentTo(matrix, 1, 1, true)).toEqual(expect.arrayContaining([0, 1, 2, 1, 3, 2, 3, 4]))
    expect(adjacentTo(matrix, 1, 2, true)).toEqual(expect.arrayContaining([1, 2, 3, 2, 4]))
  })

  it('oppositeDirection', () => {
    for (let direction of directions) {
      // check that going in the opposite direction twice is identical to not changing direction
      const opposite = oppositeDirection(direction)
      expect(oppositeDirection(opposite)).toEqual(direction)
    }
  })

  for (let direction of directions) {
    it('returns to the original position after going back and forth in: ' + direction, () => {
      const originalPosition = { x: 1, y: 1 }

      const firstMove = nextPosition(originalPosition, direction)
      const secondMove = nextPosition(firstMove, oppositeDirection(direction))
      expect(originalPosition).toEqual(secondMove)
    })
  }
})
