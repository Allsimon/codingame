import { modulo } from '@lib/number/numbers'
import { Predicate } from '@lib/type/type'

export interface Position {
  x: number
  y: number
}

export function positionAreEquals(p1: Position, p2: Position): boolean {
  return p1.x === p2.x && p1.y === p2.y
}

export function valueOf<T>(matrix: T[][], p: Position): T {
  return matrix[p.x][p.y]
}

export function emptyMatrix<T>(height: number, width = height): T[][] {
  // @ts-ignore
  return Array(height)
    .fill(undefined)
    .map(() => Array(width).fill(undefined))
}

export function printMatrix<T>(matrix: T[][], display = (t: T) => t + ' ') {
  let output = ''
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      output += display(matrix[i][j])
    }
    output += '\n'
  }
  return output
}

export function miniPrintMatrix<T>(matrix: T[][], display = (t: T) => t) {
  return matrix.map((line) => line.map((char) => display(char)).join('')).join('\n')
}

export function fillMatrix<T>(matrix: T[][], filler: (x: number, y: number) => T) {
  replaceInMatrix(matrix, (t) => t == undefined, filler)
}

export function replaceInMatrix<T>(
  matrix: T[][],
  predicate: (value: T) => boolean,
  filler: (x: number, y: number) => T,
) {
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      if (predicate(matrix[i][j])) matrix[i][j] = filler(i, j)
    }
  }
}

export function cloneAndReplaceInMatrix<T, U>(
  matrix: T[][],
  filler: (value: T, x: number, y: number) => U,
): U[][] {
  const newMatrix = emptyMatrix<U>(matrix.length, matrix[0].length)
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      newMatrix[i][j] = filler(matrix[i][j], i, j)
    }
  }
  return newMatrix
}

export function row<T>(matrix: T[][], value: number): T[] {
  return matrix[value]
}

export function column<T>(matrix: T[][], value: number): T[] {
  return matrix.map((x) => x[value])
}

export function cloneMatrix<T>(matrix: T[][]): T[][] {
  return matrix.map((l) => l.slice(0))
}

export function diagonals<T>(matrix: T[][]): T[][] {
  if (matrix.length !== matrix[0].length) throw new Error('Not square')
  const diagonals = []
  let diagonal = []
  let antidiagonal = []
  for (let i = 0; i < matrix.length; i++) {
    diagonal.push(matrix[i][i])
    antidiagonal.push(matrix[matrix.length - 1 - i][i])
  }
  diagonals.push(diagonal, antidiagonal)
  return diagonals
}

export function transpose<T>(matrix: T[][]): T[][] {
  const newArray: T[][] = []
  const mLength = matrix.length
  const mWidth = matrix[0].length
  let i
  for (i = 0; i < mWidth; i++) newArray.push([])

  for (i = 0; i < mLength; i++) {
    for (let j = 0; j < mWidth; j++) {
      newArray[j].push(matrix[i][j])
    }
  }
  return newArray
}

export function findAllInMatrix<T>(input: T[][], predicate: Predicate<T>): Position[] {
  const found: Position[] = []
  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input[i].length; j++) {
      if (predicate(input[i][j])) {
        found.push({ x: i, y: j })
      }
    }
  }
  return found
}

export function findInMatrix<T>(input: T[][], value: T): [number, number] {
  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input[i].length; j++) {
      if (input[i][j] === value) {
        return [i, j]
      }
    }
  }
  return [-1, -1]
}

export function rotateClockwise<T>(input: T[][]): T[][] {
  // reverse the rows
  const matrix = cloneMatrix(input).reverse()
  // swap the symmetric elements
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < i; j++) {
      const temp = matrix[i][j]
      matrix[i][j] = matrix[j][i]
      matrix[j][i] = temp
    }
  }
  return matrix
}

export function rotateCounterClockwise<T>(input: T[][]): T[][] {
  // reverse the individual rows
  const matrix = cloneMatrix(input).map((row) => row.reverse())
  // swap the symmetric elements
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < i; j++) {
      const temp = matrix[i][j]
      matrix[i][j] = matrix[j][i]
      matrix[j][i] = temp
    }
  }
  return matrix
}

export function flatten<T>(mat: T[][]): T[] {
  return mat.reduce((acc, val) => acc.concat(val), [])
}

export function doOnAdjacent<T>(
  matrix: T[][],
  x: number,
  y: number,
  considerDiagonal: boolean,
  func: (x: number, y: number) => void,
): void {
  doOnAdjacentComplete(
    matrix,
    { x, y },
    {
      considerDiagonal,
      periodic: false,
    },
    (p: Position) => func(p.x, p.y),
  )
}

export interface MatrixNavigationParameter {
  considerDiagonal?: boolean
  periodic?: boolean // if you go left, you appear on the right side, and vice versa, similarly with up/down
}

function handlePeriodicity<T>(matrix: T[][], position: Position): Position {
  let x = position.x
  let y = position.y
  if (position.x >= matrix.length || position.x < 0) {
    x = modulo(x, matrix.length)
  }
  if (position.y >= matrix[x].length || position.y < 0) {
    y = modulo(y, matrix[x].length)
  }
  return { x, y }
}

export function doOnAdjacentComplete<T>(
  matrix: T[][],
  position: Position,
  param: MatrixNavigationParameter,
  func: (p: Position) => void,
): void {
  const check = (x: number, y: number, consider = true) => {
    if (!!param.periodic) {
      const patchedPosition = handlePeriodicity(matrix, { x, y })
      x = patchedPosition.x
      y = patchedPosition.y
    }
    if (consider && x >= 0 && x < matrix.length && y >= 0 && y < matrix[x].length) {
      func({ x, y })
    }
  }
  const x = position.x
  const y = position.y

  check(x - 1, y - 1, param.considerDiagonal)
  check(x - 1, y)
  check(x - 1, y + 1, param.considerDiagonal)
  check(x, y - 1)
  check(x, y + 1)
  check(x + 1, y - 1, param.considerDiagonal)
  check(x + 1, y)
  check(x + 1, y + 1, param.considerDiagonal)
}

export function adjacentTo<T>(matrix: T[][], x: number, y: number, considerDiagonal = false): T[] {
  const output = new Array<T>()
  doOnAdjacent(matrix, x, y, considerDiagonal, (x, y) => output.push(matrix[x][y]))
  return output
}

export function isEqual<T>(m1: T[][], m2: T[][]): boolean {
  return JSON.stringify(m1) === JSON.stringify(m2)
}

export function isInMatrix<T>(matrix: T[][], p: Position): boolean {
  return p.x >= 0 && p.x < matrix.length && p.y >= 0 && p.y < matrix[p.x].length
}

export enum Direction {
  North = 'North',
  South = 'South',
  East = 'East',
  West = 'West',
  NorthEast = 'NorthEast',
  NorthWest = 'NorthWest',
  SouthEast = 'SouthEast',
  SouthWest = 'SouthWest',
}

export const directions = [
  Direction.North,
  Direction.South,
  Direction.East,
  Direction.West,
  Direction.NorthEast,
  Direction.NorthWest,
  Direction.SouthEast,
  Direction.SouthWest,
]

export function oppositeDirection(d: Direction): Direction {
  switch (d) {
    case Direction.North:
      return Direction.South
    case Direction.South:
      return Direction.North
    case Direction.East:
      return Direction.West
    case Direction.West:
      return Direction.East
    case Direction.NorthEast:
      return Direction.SouthWest
    case Direction.NorthWest:
      return Direction.SouthEast
    case Direction.SouthEast:
      return Direction.NorthWest
    case Direction.SouthWest:
      return Direction.NorthEast
  }
}

export function nextPosition(p: Position, direction: Direction): Position {
  switch (direction) {
    case Direction.North:
      return { x: p.x - 1, y: p.y }
    case Direction.South:
      return { x: p.x + 1, y: p.y }
    case Direction.East:
      return { x: p.x, y: p.y + 1 }
    case Direction.West:
      return { x: p.x, y: p.y - 1 }
    case Direction.NorthEast:
      return { x: p.x - 1, y: p.y + 1 }
    case Direction.NorthWest:
      return { x: p.x - 1, y: p.y - 1 }
    case Direction.SouthEast:
      return { x: p.x + 1, y: p.y + 1 }
    case Direction.SouthWest:
      return { x: p.x + 1, y: p.y - 1 }
  }
}
