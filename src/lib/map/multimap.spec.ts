import { Multimap } from '@lib/map/multimap'

describe('Multimap', () => {
  it('basic stuff', () => {
    const multimap = new Multimap()
    multimap.push('foo', 'foo')
    multimap.push('foo', 'bar')

    expect(multimap.get('foo')).toEqual(['foo', 'bar'])
    expect(multimap.get('bar')).toEqual([])
  })
})
