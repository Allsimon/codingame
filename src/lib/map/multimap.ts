import { Mapp } from '@lib/type/type'

export class Multimap<T> {
  _map: Mapp<T[]> = {}

  push(key: string | number, thing: T): Multimap<T> {
    if (this._map[key] === undefined) {
      this._map[key] = []
    }
    this._map[key].push(thing)
    return this
  }

  get(key: string | number): T[] {
    return this._map[key] || []
  }
}
