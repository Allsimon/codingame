import { chunkSubstr, escapeRegExp } from '@lib/string/strings'

describe('Strings', () => {
  it(`chunk`, () => {
    expect(chunkSubstr('foobar', 2)).toEqual(['fo', 'ob', 'ar'])
    expect(chunkSubstr('foobar', 3)).toEqual(['foo', 'bar'])
    expect(chunkSubstr('foobar', 4)).toEqual(['foob', 'ar'])
  })

  it(`escape regex`, () => {
    expect(escapeRegExp('/')).toEqual('/')
    expect(escapeRegExp('foobar')).toEqual('foobar')
    expect(escapeRegExp('$foobar')).toEqual('\\$foobar')
  })
})
