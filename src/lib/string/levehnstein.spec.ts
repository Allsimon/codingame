import { levenshteinDistance } from '@lib/string/levehnstein'

describe('Levehnstein', () => {
  it(`same`, () => {
    expect(levenshteinDistance('foo', 'foo')).toBe(0)
  })

  it(`difference`, () => {
    expect(levenshteinDistance('foo', 'fol')).toBe(1)
    expect(levenshteinDistance('foobar', 'fooobar')).toBe(1)
    expect(levenshteinDistance('foo', 'f')).toBe(2)
  })
})
