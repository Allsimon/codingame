const esbuild = require('esbuild')
const fs = require('fs')

// @ts-ignore
function getCodingameFiles(dir, f) {
  f = f || []
  const files = fs.readdirSync(dir)
  for (let i in files) {
    const name = dir + '/' + files[i]
    if (fs.statSync(name).isDirectory()) {
      getCodingameFiles(name, f)
    } else if (name.endsWith('codingame.ts')) {
      f.push(name)
    }
  }
  return f
}

getCodingameFiles('src/codingame', []).forEach((file) =>
  esbuild
    .build({
      entryPoints: [file],
      outfile: file.replace('src/codingame', 'dist/').replace('.ts', '.js'),
      bundle: true,
    })
    .catch(() => process.exit(1)),
)
