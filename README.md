# Codingame Typescript

[![pipeline status](https://gitlab.com/Allsimon/codingame/badges/master/pipeline.svg)](https://gitlab.com/Allsimon/codingame/commits/master)
[![coverage report](https://gitlab.com/Allsimon/codingame/badges/master/coverage.svg)](https://gitlab.com/Allsimon/codingame/commits/master)

### Usage

```bash
git clone https://gitlab.com/Allsimon/codingame
cd codingame

npm install
```

### Disclaimer

All of this code was hastily written, there are some really stupid workarounds in here.

### Conventions

New puzzle:

1. open a new branch with the puzzle name (for example: `https://www.codingame.com/ide/puzzle/dead-mens-shot` should be called `dead-mens-shot`)
2. write your code in `src/codingame.ts`
3. if a library can be re-used later, it should go in the `src/lib/` folder
4. solve the puzzle
5. to output the Javascript file, open a terminal at the root of this project and run `npm run build`, copy paste `dist/codingame.es5.js` into the codingame.com console
6. once the puzzle is solved, copy paste your `codingame.ts` to `src/$DIFFICULTY/$PROJECT_NAME/`
7. open the pull request to this project

PR should be squashed, final message follows https://www.conventionalcommits.org/en/v1.0.0-beta.2/

### Lib

Libs should be 100% branch covered by unit tests.

### NPM scripts

- `npm t`: Run test suite
- `npm run test:watch`: Run test suite in [interactive watch mode](http://facebook.github.io/jest/docs/cli.html#watch)
- `npm run test:prod`: Run linting and generate coverage
- `npm run build`: Generate bundles and typings
- `npm run lint`: Lints code
- `npm run commit`: Commit using conventional commit style
